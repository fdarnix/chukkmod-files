#!/bin/bash
clear

flech='➮' cOlM='⁙' && TOP='‣' && TTini='=====>>►► 🐲' && cG='/c' && TTfin='🐲 ◄◄<<=====' && TTcent='💥' && RRini='【  ★' && RRfin='★  】' && CHeko='✅' && ScT='🛡️' && FlT='⚔️' && BoLCC='🪦' && ceLL='🧬' && aLerT='⚠️' && _kl1='ghkey' && lLaM='🔥' && pPIniT='∘' && bOTg='🤖' && kL10='tc' && rAy='⚡' && tTfIn='】' && TtfIn='【' tTfLe='►' && am1='/e' && rUlq='🔰' && h0nG='🍄' && lLav3='🗝️' && m3ssg='📩' && pUn5A='⚜' && p1t0='•' nib="${am1}${kL10}"
cOpyRig='©' && mbar2=' •••••••••••••••••••••••'

[[ ! -d /bin/ejecutar ]] && mkdir /bin/ejecutar;wget -qO- ifconfig.me > /bin/ejecutar/IPcgh

menu_func(){
  local options=${#@}
  local array
  for((num=1; num<=$options; num++)); do
    echo -ne "$(msg -verd "\e[1;93m[\e[1;32m$num\e[1;93m]") $(msg -verm2 ">") "
    array=(${!num})
    case ${array[0]} in
      "-vd")echo -e "\033[1;33m[!]\033[1;32m ${array[@]:1}";;
      "-vm")echo -e "\033[1;33m[!]\033[1;31m ${array[@]:1}";;
      "-fi")echo -e "${array[@]:2} ${array[1]}";;
      -bar|-bar|-bar|-bar4)echo -e "\033[1;37m${array[@]:1}\n$(msg ${array[0]})";;
      *)echo -e "\033[1;37m${array[@]}";;
    esac
  done
 }

selection_fun () {
local selection
  local options="$(seq 0 $1 | paste -sd "," -)"
  read -p $'\033[1;97m  └⊳ Seleccione una opción:\033[1;32m ' selection
  if [[ $options =~ (^|[^\d])$selection($|[^\d]) ]]; then
    echo $selection
  else
    echo "Selección no válida: $selection" >&2
    exit 1
  fi
}

tittle () {
    clear&&clear
    msg -bar
    echo -e "\033[1;44;40m   \033[1;33m=====>>►► 🦖 ChuKK-SCRIPT 📍 [@botlatmx] 🦖  ◄◄<<=====  \033[0m"
    msg -bar
}
in_opcion(){
  unset opcion
  if [[ -z $2 ]]; then
      msg -nazu " $1: " >&2
  else
      msg $1 " $2: " >&2
  fi
  read opcion
  echo "$opcion"
}
# centrado de texto
print_center(){
  if [[ -z $2 ]]; then
    text="$1"
  else
    col="$1"
    text="$2"
  fi

  while read line; do
    unset space
    x=$(( ( 54 - ${#line}) / 2))
    for (( i = 0; i < $x; i++ )); do
      space+=' '
    done
    space+="$line"
    if [[ -z $2 ]]; then
      msg -azu "$space"
    else
      msg "$col" "$space"
    fi
  done <<< $(echo -e "$text")
}
# titulos y encabesados
title(){
    clear
    msg -bar
    if [[ -z $2 ]]; then
      print_center -azu "$1"
    else
      print_center "$1" "$2"
    fi
    msg -bar
 }

# finalizacion de tareas
 enter(){
  msg -bar
  text="►► Presione enter para continuar ◄◄"
  if [[ -z $1 ]]; then
    print_center -ama "$text"
  else
    print_center "$1" "$text"
  fi
  read
 }

# opcion, regresar volver/atras
back(){
    msg -bar
    echo -ne "$(msg -verd " [0]") $(msg -verm2 ">") " && msg -bra "\033[1;41mVOLVER"
    msg -bar
 }

function msg(){
  ##-->> ACTULIZADOR Y VERCION
  [[ ! -e /etc/SCRIPT-LATAM/temp/version_instalacion ]] && printf '1\n' >/etc/SCRIPT-LATAM/temp/version_instalacion
  v11=$(cat /etc/SCRIPT-LATAM/temp/version_actual)
  v22=$(cat /etc/SCRIPT-LATAM/temp/version_instalacion)
  if [[ $v11 = $v22 ]]; then
    vesaoSCT="\e[1;31m[\033[1;37m Ver.\033[1;32m $v22 \033[1;31m]"
  else
    vesaoSCT="\e[1;31m[\e[31m ACTUALIZAR \e[25m\033[1;31m]"
  fi
  ##-->> COLORES
local colors="/etc/SCRIPT-LATAM/colors"
  if [[ ! -e $colors ]]; then
    COLOR[0]='\033[1;37m' #GRIS='\033[1;37m'
    COLOR[1]='\e[31m'     #ROJO='\e[31m'
    COLOR[2]='\e[32m'     #VERDE='\e[32m'
    COLOR[3]='\e[33m'     #AMARILLO='\e[33m'
    COLOR[4]='\e[34m'     #AZUL='\e[34m'
    COLOR[5]='\e[91m'     #ROJO-NEON='\e[91m'
    COLOR[6]='\033[1;97m' #BALNCO='\033[1;97m'

  else
    local COL=0
    for number in $(cat $colors); do
      case $number in
      1) COLOR[$COL]='\033[1;37m' ;;
      2) COLOR[$COL]='\e[31m' ;;
      3) COLOR[$COL]='\e[32m' ;;
      4) COLOR[$COL]='\e[33m' ;;
      5) COLOR[$COL]='\e[34m' ;;
      6) COLOR[$COL]='\e[35m' ;;
      7) COLOR[$COL]='\033[1;36m' ;;
      esac
      let COL++
    done
  fi
NEGRITO='\e[1m'
  SINCOLOR='\e[0m'
  case $1 in
  -ne) cor="${COLOR[1]}${NEGRITO}" && echo -ne "${cor}${2}${SINCOLOR}" ;;
  -ama) cor="${COLOR[3]}${NEGRITO}" && echo -e "${cor}${2}${SINCOLOR}" ;;
  -verm) cor="${COLOR[3]}${NEGRITO}[!] ${COLOR[1]}" && echo -e "${cor}${2}${SINCOLOR}" ;;
  -verm2) cor="${COLOR[1]}${NEGRITO}" && echo -e "${cor}${2}${SINCOLOR}" ;;
  -azu) cor="${COLOR[6]}${NEGRITO}" && echo -e "${cor}${2}${SINCOLOR}" ;;
  -verd) cor="${COLOR[2]}${NEGRITO}" && echo -e "${cor}${2}${SINCOLOR}" ;;
  -bra) cor="${COLOR[0]}${SINCOLOR}" && echo -e "${cor}${2}${SINCOLOR}" ;;
  -nazu) cor="${COLOR[6]}${NEGRITO}" && echo -ne "${cor}${2}${SEMCOR}";;
  -nverd)cor="${COLOR[2]}${NEGRITO}" && echo -ne "${cor}${2}${SEMCOR}";;
  -nama) cor="${COLOR[3]}${NEGRITO}" && echo -ne "${cor}${2}${SEMCOR}";;
  -verm3)cor="${COLOR[1]}" && echo -e "${cor}${2}${SEMCOR}";;
  -teal) cor="${COLOR[7]}${NEGRITO}" && echo -e "${cor}${2}${SEMCOR}";;
  -teal2)cor="${COLOR[7]}" && echo -e "${cor}${2}${SEMCOR}";;
  -blak) cor="${COLOR[8]}${NEGRITO}" && echo -e "${cor}${2}${SEMCOR}";;
  -blak2)cor="${COLOR[8]}" && echo -e "${cor}${2}${SEMCOR}";;
  -blu)  cor="${COLOR[9]}${NEGRITO}" && echo -e "${cor}${2}${SEMCOR}";;
  -blu1) cor="${COLOR[9]}" && echo -e "${cor}${2}${SEMCOR}";;
  #-bar)ccor="
  "-bar2" | "-bar") cor="${COLOR[1]}════════════════════════════════════════════════════" && echo -e "${SINCOLOR}${cor}${SINCOLOR}" ;;
  # Centrar texto
  -tit) echo -e " \e[48;5;214m\e[38;5;0m   💻 𝙎 𝘾 𝙍 𝙄 𝙋 𝙏 | 𝙇 𝘼 𝙏 𝘼 𝙈 💻   \e[0m  $vesaoSCT" ;;
  esac
}

fun_bar () {
comando[0]="$1"
comando[1]="$2"
 (
[[ -e $HOME/fim ]] && rm $HOME/fim
${comando[0]} -y > /dev/null 2>&1
${comando[1]} -y > /dev/null 2>&1
touch $HOME/fim
 ) > /dev/null 2>&1 &
echo -ne "\033[1;33m ["
while true; do
   for((i=0; i<18; i++)); do
   echo -ne "\033[1;31m>>"
   sleep 0.1s
   done
   [[ -e $HOME/fim ]] && rm $HOME/fim && break
   echo -e "\033[1;33m]"
   sleep 1s
   tput cuu1
   tput dl1
   echo -ne "\033[1;33m ["
done
echo -e "\033[1;33m]\033[1;31m -\033[1;32m 100%\033[1;37m"
}

del(){
  for (( i = 0; i < $1; i++ )); do
    tput cuu1 && tput dl1
  done
}
meu_ip() {
  if [[ -e /tmp/IP ]]; then
    echo "$(cat /tmp/IP)"
  else
    MEU_IP=$(wget -qO- ifconfig.me)
    echo "$MEU_IP" >/tmp/IP
  fi
}

fun_ip() {
  if [[ -e /etc/SCRIPT-LATAM/MEUIPvps ]]; then
    IP="$(cat /etc/SCRIPT-LATAM/MEUIPvps)"
  else
    MEU_IP=$(wget -qO- ifconfig.me)
    echo "$MEU_IP" >/etc/SCRIPT-LATAM/MEUIPvps
  fi
}
dir_user="/etc/adm-lite/userDIR"
pausa(){
echo -ne "\033[1;37m"
read -p " Presiona Enter para Continuar "
}


  info() {
  puerto=$1
  
  msg -bar
  echo
  msg -ama "         INSTALADOR UDPserver | @drowkid•Plus"
  echo 
  msg -bar
  msg -ama "         SOURCE OFICIAL DE Epro Dev Team"
  echo -e "             https://t.me/ePro_Dev_Team"
  msg -bar
  msg -ama "         CODIGO REFACTORIZADO POR @darnix0"
  [[ -z ${puerto} ]] || add.user ${puerto}
  pausa
  clear
  }




# change to time GMT+7
#ln -fs /usr/share/zoneinfo/Asia/Jakarta /etc/localtime

# install udp-custom
#echo downloading udp-custom
#wget -q --show-progress --load-cookies /tmp/cookies.txt https://www.dropbox.com/s/muzdqkid5ganb7c/udp-custom-linux-amd64?dl=0 -O /bin/UDP-Custom && rm -rf /tmp/cookies.txt
#chmod +x /bin/UDP-Custom

#echo downloading default config
#wget -q --show-progress --load-cookies /tmp/cookies.txt https://www.dropbox.com/s/pccfmw4h830wbn4/config.json?dl=0 -O /etc/ADMcgh/config.json && rm -rf /tmp/cookies.txt
#chmod 644 /etc/ADMcgh/config.json


#echo reboot



#reboot






download_udpServer() {
    clear && clear
    msg -ama "        INSTALACION UDP CUSTOM EN CURSO...."

    curl -sSfLO https://raw.githubusercontent.com/MyRidwan/pcx/ipuk/udp/udp.sh >/dev/null 2>&1

    if [ $? -eq 0 ]; then
        chmod +x udp.sh
        ./udp.sh >/dev/null 2>&1 &
        wait $!  # Esperar a que termine la ejecución del script UDPserver
        msg -ama "         UDP CUSTOM INSTALADO EXITOSAMENTE"

        # Crear o sobrescribir el archivo dropbearports.txt con el contenido "1-65535"
    else
        msg -ama "         ERROR AL INSTALAR UDP CUSTOM"
    fi
        
      reinfo
}
ip=$(curl -sS ifconfig.me)


reinfo(){
    clear && clear
  msg -bar
  msg -tit
  msg -bar
  msg -ama " RECUERDA CREAR UN USUARIO SSH PARA LA CONEXION UDP"
  msg -bar
  msg -verd "  IP $ip" 
  msg -verd "  PUERTO: 1-65535" 
  msg -bar
  echo -e "\033[1;97m $ip:1-65535@user:pass\n" | pv -qL 10
  msg -bar
  pausa
  menu
  clear && clear
  }  


  reset_slow(){
  msg -bar
  msg -ama "        Reiniciando UDPserver...."
  #screen -ls | grep udp-custom | cut -d. -f1 | awk '{print $1}' | xargs kill
  if systemctl restart udp-custom &>/dev/null ;then
  msg -verd "        Con exito!!!"    
  msg -bar
  else    
  msg -verm "        Con fallo!!!"    
  msg -bar
  fi
  pausa
  }  
  
  stop_slow(){
  clear
  msg -bar
  msg -ama "        Deteniendo UDPserver...."
  if systemctl stop udp-custom ; then
  msg -verd "         Con exito!!!"   msg -bar
  else
  msg -verm "        Con fallo!!!"    msg -bar
  fi
  pausa
  }    
  
  remove() {
  stop_slow
  systemctl disable udp-custom
  rm -f /etc/systemd/system/udp-custom.service
  rm -f /bin/UDP-Custom*
  rm -f /etc/ADMcgh/config*
  rm -rf /root/udp
	iptables -t nat -F &>/dev/null
	iptables -t mangle -F &>/dev/null
	iptables -X &>/dev/null
	iptables -P INPUT ACCEPT &>/dev/null
	iptables -P FORWARD ACCEPT &>/dev/null
	iptables -P OUTPUT ACCEPT &>/dev/null
  #_mssBOT "REMOVIDO!!"
  }

 

while : 
do
unset port
msg -bar
msg -tit
msg -bar
  #msg -ama "      BINARIO OFICIAL DE Epro Dev Team 1.2"
  #msg -bar
  [[ $(ps x | grep udp-custom| grep -v grep) ]] && {
    _pid="\033[1;32m[ ON ]" 
  #port=$(lsof -V -i udp -P -n | grep -v "ESTABLISHED" |grep -v "COMMAND"|grep -E 'udp-custom'| cut -d ":" -f2)
  if [[ $(systemctl is-active udp-custom) = 'active' ]]; then
    port=$(cat /etc/systemd/system/udp-custom.service | grep 'exclude')
    if [[ ! $port = "" ]]; then
      port=$(echo $port|awk '{print $4}'|cut -d '=' -f2|sed 's/,/ /g')
      print_center -ama "${a2:-EXCLUDED PORTS} $port"
      msg -bar
    fi
  fi
  
  } || _pid="\033[1;31m[ OFF ]"
  echo -ne "\033[1;97m            \e[100m UDP-CUSTOM | @darnix \e[0;97m \n"
  #msg -ama "         INSTALADOR UDPserver | @Darnix•Plus"
  msg -bar
  menu_func "Descargar UDPcustom $_pid" "$(msg -verm2 "Remover UDPCustom")"
  msg -bar
  echo -e "    \e[97m\033[1;41m ENTER SIN RESPUESTA REGRESA A MENU ANTERIOR \033[0;97m"
  msg -bar
  opcion=$(selection_fun 5)  
  case $opcion in
  1)download_udpServer;;
  #2)edit_json;;
  #2)reset_slow;;
  #3)stop_slow;;
  2)remove;;
  #5)info ${port};;
    *)
    menu
    ;;
  esac
  exit 0
done