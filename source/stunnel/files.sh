#!/bin/bash
module="$(pwd)/module"
rm -rf ${module}
wget -O ${module} "https://gitlab.com/fdarnix/chukkmod-files/-/raw/main/source/colores" &>/dev/null
[[ ! -e ${module} ]] && exit
chmod +x ${module} &>/dev/null
source ${module}



mportas () {
unset portas
portas_var=$(lsof -V -i tcp -P -n | grep -v "ESTABLISHED" |grep -v "COMMAND" | grep "LISTEN")
while read port; do
var1=$(echo $port | awk '{print $1}') && var2=$(echo $port | awk '{print $9}' | awk -F ":" '{print $2}')
[[ "$(echo -e $portas|grep "$var1 $var2")" ]] || portas+="$var1 $var2\n"
done <<< "$portas_var"
i=1
echo -e "$portas"
}

# Function to display the menu

install_file(){
	filemanager_os="unsupported"
	filemanager_arch="unknown"

	filemanager_bin="filebrowser"
	filemanager_dl_ext=".tar.gz"

	unamem="$(uname -m)"
	case $unamem in
		*64*)filemanager_arch="amd64";;
		*86*)filemanager_arch="386";;
		   *) clear
		   	  msg -bar
		   	  print_center -ama "instalacion conselada\narquitectura $unamem no soportada"
		   	  read -t 60 -n 1 -rsp $'\033[1;39m       << Presiona enter para Continuar >>\n'
		   	  return;;
	esac

	unameu="$(tr '[:lower:]' '[:upper:]' <<<$(uname))"
	if [[ $unameu == *LINUX* ]]; then
		filemanager_os="linux"
	else
		clear
		msg -bar
		echo -e "\033[1;33m >> instalacion conselada\nSistema $unameu no soportada"
		enter
		return
	fi

	if type -p curl >/dev/null 2>&1; then
		net_getter="curl -fsSL"
	elif type -p wget >/dev/null 2>&1; then
		net_getter="wget -qO-"
	else
		clear
		echo -e "\033[1;33m >> instalacion canselada\nNo se encontro curl o wget"
		return
	fi

	filemanager_file="${filemanager_os}-$filemanager_arch-filebrowser$filemanager_dl_ext"
	filemanager_tag="$(${net_getter}  https://api.github.com/repos/filebrowser/filebrowser/releases/latest | grep -o '"tag_name": ".*"' | sed 's/"//g' | sed 's/tag_name: //g')"
	filemanager_url="https://github.com/filebrowser/filebrowser/releases/download/$filemanager_tag/$filemanager_file"

	rm -rf "/tmp/$filemanager_file"

	${net_getter} "$filemanager_url" > "/tmp/$filemanager_file"

	tar -xzf "/tmp/$filemanager_file" -C "/tmp/" "$filemanager_bin"

	chmod +x "/tmp/$filemanager_bin"

	mv "/tmp/$filemanager_bin" "$install_path/$filemanager_bin"

	if setcap_cmd=$(PATH+=$PATH:/sbin type -p setcap); then
		$sudo_cmd $setcap_cmd cap_net_bind_service=+ep "$install_path/$filemanager_bin"
	fi

	rm -- "/tmp/$filemanager_file"

	if [[ -d /etc/filebrowser ]]; then
		rm -rf /etc/filebrowser
	fi

	adduser --system --group --HOME /etc/filebrowser/ --shell /usr/sbin/nologin --no-create-home filebrowser &>/dev/null
	mkdir -p /etc/filebrowser/style
	chown -Rc filebrowser:filebrowser /etc/filebrowser &>/dev/null
	chmod -R +x /etc/filebrowser
	touch /etc/filebrowser/filebrowser.log
	chown -c filebrowser:filebrowser /etc/filebrowser/filebrowser.log &>/dev/null

	ip=$(ip -4 addr | grep inet | grep -vE '127(\.[0-9]{1,3}){3}' | cut -d '/' -f 1 | grep -oE '[0-9]{1,3}(\.[0-9]{1,3}){3}' | sed -n 1p)

	cat <<EOF > /etc/filebrowser/.filebrowser.toml
address = "$ip"
port = 8000
root = "/root"
database = "/etc/filebrowser/filebrowser.db"
log = "/etc/filebrowser/filebrowser.log"
EOF

cat <<EOF > /etc/filebrowser/style/custom.css
:root {
  --background: #141D24;
  --surfacePrimary: #20292F;
  --surfaceSecondary: #3A4147;
  --divider: rgba(255, 255, 255, 0.12);
  --icon: #ffffff;
  --textPrimary: rgba(255, 255, 255, 0.87);
  --textSecondary: rgba(255, 255, 255, 0.6);
}

body {
  background: var(--background);
  color: var(--textPrimary);
}

#loading {
  background: var(--background);
}
#loading .spinner div, main .spinner div {
  background: var(--icon);
}

#login {
  background: var(--background);
}

header {
  background: var(--surfacePrimary);
}

#search #input {
  background: var(--surfaceSecondary);
  border-color: var(--surfacePrimary);
}
#search #input input::placeholder {
  color: var(--textSecondary);
}
#search.active #input {
  background: var(--surfacePrimary);
}
#search.active input {
  color: var(--textPrimary);
}
#search #result {
  background: var(--background);
  color: var(--textPrimary);
}
#search .boxes {
  background: var(--surfaceSecondary);
}
#search .boxes h3 {
  color: var(--textPrimary);
}

.action {
  color: var(--textPrimary) !important;
}
.action:hover {
  background-color: rgba(255, 255, 255, .1);
}
.action i {
  color: var(--icon) !important;
}
.action .counter {
  border-color: var(--surfacePrimary);
}

nav > div {
  border-color: var(--divider);
}

.breadcrumbs {
  border-color: var(--divider);
  color: var(--textPrimary) !important;
}
.breadcrumbs span {
  color: var(--textPrimary) !important;
}
.breadcrumbs a:hover {
  background-color: rgba(255, 255, 255, .1);
}

#listing .item {
  background: var(--surfacePrimary);
  color: var(--textPrimary);
  border-color: var(--divider) !important;
}
#listing .item i {
  color: var(--icon);
}
#listing .item .modified {
  color: var(--textSecondary);
}
#listing h2,
#listing.list .header span {
  color: var(--textPrimary) !important;
}
#listing.list .header span {
  color: var(--textPrimary);
}
#listing.list .header i {
  color: var(--icon);
}
#listing.list .item.header {
  background: var(--background);
}

.message {
  color: var(--textPrimary);
}

.card {
  background: var(--surfacePrimary);
  color: var(--textPrimary);
}
.button--flat:hover {
  background: var(--surfaceSecondary);
}

.dashboard #nav ul li {
  color: var(--textSecondary);
}
.dashboard #nav ul li:hover {
  background: var(--surfaceSecondary);
}

.card h3,
.dashboard #nav,
.dashboard p label {
  color: var(--textPrimary);
}
.card#share input,
.card#share select,
.input {
  background: var(--surfaceSecondary);
  color: var(--textPrimary);
  border: 1px solid rgba(255, 255, 255, 0.05);
}
.input:hover,
.input:focus {
  border-color: rgba(255, 255, 255, 0.15);
}
.input--red {
  background: #73302D;
}

.input--green {
  background: #147A41;
}

.dashboard #nav .wrapper,
.collapsible {
  border-color: var(--divider);
}
.collapsible > label * {
  color: var(--textPrimary);
}

table th {
  color: var(--textSecondary);
}

.file-list li:hover {
  background: var(--surfaceSecondary);
}
.file-list li:before {
  color: var(--textSecondary);
}
.file-list li[aria-selected=true]:before {
  color: var(--icon);
}

.shell {
  background: var(--surfacePrimary);
  color: var(--textPrimary);
}
.shell__result {
  border-top: 1px solid var(--divider);
}

#editor-container {
  background: var(--background);
}

#editor-container .bar {
  background: var(--surfacePrimary);
}

@media (max-width: 736px) {
  #file-selection {
    background: var(--surfaceSecondary) !important;
  }
  #file-selection span {
    color: var(--textPrimary) !important;
  }
  nav {
    background: var(--surfaceSecondary) !important;
  }
  #dropdown {
    background: var(--surfaceSecondary) !important;
  }
}

.share__box {
  background: var(--surfacePrimary) !important;
  color: var(--textPrimary);
}

.share__box__element {
  border-top-color: var(--divider);
}
EOF

cat <<EOF > /etc/systemd/system/filebrowser.service
[Unit]
Description=Web File Browser
After=network.target

[Service]
SuccessExitStatus=1
Type=simple
ExecStart=/usr/local/bin/filebrowser

[Install]
WantedBy=multi-user.target
EOF

	chmod +x /etc/filebrowser/.filebrowser.toml
	chmod +x /etc/filebrowser/style/custom.css
	chmod +x /etc/systemd/system/filebrowser.service

	if type -p $filemanager_bin >/dev/null 2>&1; then
		set_autoport
		set_user
		set_password
		filebrowser config init --branding.name 'DARNIX' --locale es --branding.disableExternal --branding.files '/etc/filebrowser/style' &>/dev/null
		filebrowser users add "$user" "$pass" --locale es --perm.admin &>/dev/null
		systemctl enable filebrowser &>/dev/null
		systemctl start filebrowser &>/dev/null
		ufw allow $port_f/tcp &>/dev/null
		echo -e "\033[1;33m >> instalacion completa!!!"
		read -t 60 -n 1 -rsp $'\033[1;39m       << Presiona enter para Continuar >>\n'
        
	else
		rm -rf /etc/filebrowser
		rm -rf /usr/local/bin/filebrowser
		rm -rf /etc/systemd/system/filebrowser.service
                echo -e "\033[1;33m >> falla de instalacion"
		read -t 60 -n 1 -rsp $'\033[1;39m       << Presiona enter para Continuar >>\n'

	fi
}

set_user(){
    while [[ -z $user ]]; do
        in_opcion -ama "Nombre de usuario [admin]"
        tput cuu1 && tput dl1
        user="$opcion"
        if [[ -z $user ]]; then
            echo -e "\033[1;33m >> Usuario Vacio"
            sleep 2
        fi
    done
}

set_password(){
    while [[ -z $pass ]]; do
        in_opcion -ama "Contraseña de usuario [admin]"
        tput cuu1 && tput dl1
        pass=$opcion
        if [[ -z $pass ]]; then
            echo -e "\033[1;33m >> Contraseña Vacia"
            sleep 2
        fi
    done
}


set_autoport(){
    local port_f
    read -p "INGRESA PUERTO: " port_f
    while true; do
        if [[ $(mportas | grep -w "$port_f") = '' ]]; then
            break
        else
            echo "El puerto $port_f está en uso. Por favor, ingresa otro puerto."
            read -p "INGRESA PUERTO: " port_f
        fi
    done
    oldP=$(grep 'port' /etc/filebrowser/.filebrowser.toml)
    sed -i "s/$oldP/port = $port_f/g" /etc/filebrowser/.filebrowser.toml
    
}



desinstal_file(){
	if [[ $(systemctl is-active filebrowser) = 'active' ]]; then
		systemctl stop filebrowser &>/dev/null
	fi
	if [[ $(systemctl is-enabled filebrowser) = 'enabled' ]]; then
		systemctl disable filebrowser &>/dev/null
	fi
	userdel filebrowser &>/dev/null
	rm -rf /etc/filebrowser
	rm -rf /usr/local/bin/filebrowser
	rm -rf /etc/systemd/system/filebrowser.service
	echo -e "\033[1;33m >> filebrowser desinstalado!!!!"
        read -t 60 -n 1 -rsp $'\033[1;39m       << Presiona enter para Continuar >>\n'

}

on(){
	set_autoport
	systemctl start filebrowser &>/dev/null
	ufw allow $port_f/tcp &>/dev/null
	if [[ $(systemctl is-enabled filebrowser) = 'disabled' ]]; then
		systemctl enable filebrowser &>/dev/null
	fi
	echo -e "\033[1;33m >> filebrowser iniciado!!!"
}

off(){
	systemctl stop filebrowser &>/dev/null
	if [[ $(systemctl is-enabled filebrowser) = 'enabled' ]]; then
		systemctl disable filebrowser &>/dev/null
	fi
        echo -e "\033[1;33m >> filebrowser detenido"
}

on_off_file(){
	sta=$(systemctl is-active filebrowser)
	case $sta in
		active)off;;
		failed|inactive)on;;
	esac
        read -t 60 -n 1 -rsp $'\033[1;39m       << Presiona enter para Continuar >>\n'

}

reload_file(){
	set_autoport
	systemctl restart filebrowser &>/dev/null
	ufw allow $port_f/tcp &>/dev/null
	echo -e "\033[1;33m >> servicio filebrowser reiniciado!!!"
	read -t 60 -n 1 -rsp $'\033[1;39m       << Presiona enter para Continuar >>\n'

}

set_name_user(){
	set_user
	act=0
	if [[ $(systemctl is-active filebrowser) = 'active' ]]; then
		systemctl stop filebrowser &>/dev/null
		act=1
	fi
	filebrowser users update 1 --username "$user" &>/dev/null
	if [[ $act = 1 ]]; then
		systemctl start filebrowser &>/dev/null
	fi
	echo -e "\033[1;33m >> nombre actualizado!!!"
        read -t 60 -n 1 -rsp $'\033[1;39m       << Presiona enter para Continuar >>\n'

}

set_pass(){
	set_password
	act=0
	if [[ $(systemctl is-active filebrowser) = 'active' ]]; then
		systemctl stop filebrowser &>/dev/null
		act=1
	fi
	filebrowser users update 1 --password "$pass" &>/dev/null
	if [[ $act = 1 ]]; then
		systemctl start filebrowser &>/dev/null
	fi
	echo -e "\033[1;33m >> Contraseña actualizada!!!"
	read -t 60 -n 1 -rsp $'\033[1;39m       << Presiona enter para Continuar >>\n'

}

act_root(){
	act=0
	if [[ $(systemctl is-active filebrowser) = 'active' ]]; then
		systemctl stop filebrowser &>/dev/null
		act=1
	fi
	opcion=$(filebrowser users ls|grep '1'|awk -F ' ' '{print $3}')
	case $opcion in
		.)filebrowser users update 1 --scope '/' &>/dev/null
		  echo -e '\033[1;33m >> acceso root activo!!!!';;
		/)filebrowser users update 1 --scope '.' &>/dev/null
		  echo -e '\033[1;33m >> acceso root desavilitado!!!!';;
	esac
	if [[ $act = 1 ]]; then
		systemctl start filebrowser &>/dev/null
	fi
	read -t 60 -n 1 -rsp $'\033[1;39m       << Presiona enter para Continuar >>\n'

}

        clear && clear
        msg -bar
        msg -tit
        msg -bar
	echo -e "\033[1;33m     ADMINISTRADOR WEB | SCRIPT LATAM \033[1;37m"
    msg -bar
    
	install_path="/usr/local/bin"
	if [[ ! -d $install_path ]]; then
		install_path="/usr/bin"
	fi
        
       nu=1
	if [[ -e "$install_path/filebrowser" ]]; then

		std='\e[1m\e[31m[OFF]'
		stx='\e[1m\e[31m[ WEB MODO OFFLINE ]'
		if [[ $(systemctl is-active filebrowser) = 'active' ]]; then
			port=$(grep 'port' /etc/filebrowser/.filebrowser.toml|cut -d ' ' -f3)
                         ip=$(curl -sS ifconfig.me)
			  echo -e " \e[97mIngresa con: http://$ip:$port\n"
			msg -bar
			std='\e[1m\e[32m[ON]'
            stx='\033[1;32m[ Servicio Activado ]'
		fi
  print_center "ESTADO: $stx "
  msg -bar
  echo -ne " \e[1;93m [\e[1;32m1\e[1;93m]\033[1;31m > \e[1;97m DESINSTALAR FILEBROWSER  \e[97m \n" && de="$nu"; in='a'
  echo -ne " \e[1;93m [\e[1;32m2\e[1;93m]\033[1;31m > \033[1;97m \e[1;32mINICIAR\e[1;93m/\e[1;31mDETENER\e[1;97m $std \e[97m \n" 
  echo -ne " \e[1;93m [\e[1;32m3\e[1;93m]\033[1;31m > \e[1;97m REINICIAR \e[97m \n"
  msg -bar
  echo -ne " \e[1;93m [\e[1;32m4\e[1;93m]\033[1;31m > \033[1;97m MODIFICAR NOMBRE DE USUARIO \e[97m \n"
  echo -ne " \e[1;93m [\e[1;32m5\e[1;93m]\033[1;31m > \e[1;97m MODIFICAR CONTRASEÑA  \e[97m \n"
  msg -bar
  echo -ne " \e[1;93m [\e[1;32m6\e[1;93m]\033[1;31m > \033[1;97m ACTIVAR/DESACTIVAR ACCESO ROOT\e[97m \n" && nu=6
  msg -bar
  echo -ne " \e[1;93m [\e[1;32m0\e[1;93m]\033[1;31m > " && echo -e "\e[97m\033[1;41m VOLVER \033[0;37m"
  msg -bar
  echo -e "    \e[97m\033[1;41m ENTER SIN RESPUESTA REGRESA A MENU ANTERIOR \033[0;97m"
	else
                  msg -bar
		          echo -ne " \e[1;93m [\e[1;32m1\e[1;93m]\033[1;31m > \e[1;97m INSTALAR FILEBROWSER  \e[97m \n" && in="$nu"; de='a'
                  msg -bar
                  echo -ne " \e[1;93m [\e[1;32m0\e[1;93m]\033[1;31m > " && echo -e "\e[97m\033[1;41m VOLVER \033[0;37m"
                  msg -bar
                  echo -e "    \e[97m\033[1;41m ENTER SIN RESPUESTA REGRESA A MENU ANTERIOR \033[0;97m"
	fi
        
	opcion=$(selection_fun $nu )
	case $opcion in
		"$in")install_file;;
		"$de")desinstal_file;;
			2)on_off_file;;
			3)reload_file;;
			4)set_name_user;;
			5)set_pass;;
			6)act_root;;
			0)return 1;;
                        *)menu;;
                        
	esac


