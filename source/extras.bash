#!/bin/bash

function firewall(){
PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:~/bin
export PATH
declare -A cor=( [0]="\033[1;37m" [1]="\033[1;34m" [2]="\033[1;31m" [3]="\033[1;33m" [4]="\033[1;32m" )
SCPfrm="/etc/adm-lite" # && [[ ! -d ${SCPfrm} ]] && exit
SCPinst="/etc/adm-lite" # && [[ ! -d ${SCPinst} ]] && exit

sh_ver="1.0.11"
Green_font_prefix="\033[32m" && Red_font_prefix="\033[31m" && Green_background_prefix="\033[42;37m" && Red_background_prefix="\033[41;37m" && Font_color_suffix="\033[0m"
Info="${Green_font_prefix}[Informacion]${Font_color_suffix}"
Error="${Red_font_prefix}[Error]${Font_color_suffix}"

smtp_port="25,26,465,587"
pop3_port="109,110,995"
imap_port="143,218,220,993"
other_port="24,50,57,105,106,158,209,1109,24554,60177,60179"
bt_key_word="torrent
.torrent
peer_id=
announce
info_hash
get_peers
find_node
BitTorrent
announce_peer
BitTorrent protocol
announce.php?passkey=
magnet:
xunlei
sandai
Thunder
XLLiveUD"

check_sys(){
	if [[ -f /etc/redhat-release ]]; then
		release="centos"
	elif cat /etc/issue | grep -q -E -i "debian"; then
		release="debian"
	elif cat /etc/issue | grep -q -E -i "ubuntu"; then
		release="ubuntu"
	elif cat /etc/issue | grep -q -E -i "centos|red hat|redhat"; then
		release="centos"
	elif cat /proc/version | grep -q -E -i "debian"; then
		release="debian"
	elif cat /proc/version | grep -q -E -i "ubuntu"; then
		release="ubuntu"
	elif cat /proc/version | grep -q -E -i "centos|red hat|redhat"; then
		release="centos"
    fi
	bit=`uname -m`
}
check_BT(){
	Cat_KEY_WORDS
	BT_KEY_WORDS=$(echo -e "$Ban_KEY_WORDS_list"|grep "torrent")
}
check_SPAM(){
	Cat_PORT
	SPAM_PORT=$(echo -e "$Ban_PORT_list"|grep "${smtp_port}")
}
Cat_PORT(){
	Ban_PORT_list=$(iptables -t filter -L OUTPUT -nvx --line-numbers|grep "REJECT"|awk '{print $13}')
}
Cat_KEY_WORDS(){
	Ban_KEY_WORDS_list=""
	Ban_KEY_WORDS_v6_list=""
	if [[ ! -z ${v6iptables} ]]; then
		Ban_KEY_WORDS_v6_text=$(${v6iptables} -t mangle -L OUTPUT -nvx --line-numbers|grep "DROP")
		Ban_KEY_WORDS_v6_list=$(echo -e "${Ban_KEY_WORDS_v6_text}"|sed -r 's/.*\"(.+)\".*/\1/')
	fi
	Ban_KEY_WORDS_text=$(${v4iptables} -t mangle -L OUTPUT -nvx --line-numbers|grep "DROP")
	Ban_KEY_WORDS_list=$(echo -e "${Ban_KEY_WORDS_text}"|sed -r 's/.*\"(.+)\".*/\1/')
}
View_PORT(){
	Cat_PORT
	echo -e "========${Red_background_prefix} Puerto Bloqueado Actualmente ${Font_color_suffix}========="
	echo -e "$Ban_PORT_list" && echo && echo -e "==============================================="
}
View_KEY_WORDS(){
	Cat_KEY_WORDS
	echo -e "============${Red_background_prefix} Actualmente Prohibido ${Font_color_suffix}============"
	echo -e "$Ban_KEY_WORDS_list" && echo -e "==============================================="
}
View_ALL(){
	echo
	View_PORT
	View_KEY_WORDS
	echo
	msg -bar
}
Save_iptables_v4_v6(){
	if [[ ${release} == "centos" ]]; then
		if [[ ! -z "$v6iptables" ]]; then
			service ip6tables save
			chkconfig --level 2345 ip6tables on
		fi
		service iptables save
		chkconfig --level 2345 iptables on
	else
		if [[ ! -z "$v6iptables" ]]; then
			ip6tables-save > /etc/ip6tables.up.rules
			echo -e "#!/bin/bash\n/sbin/iptables-restore < /etc/iptables.up.rules\n/sbin/ip6tables-restore < /etc/ip6tables.up.rules" > /etc/network/if-pre-up.d/iptables
		else
			echo -e "#!/bin/bash\n/sbin/iptables-restore < /etc/iptables.up.rules" > /etc/network/if-pre-up.d/iptables
		fi
		iptables-save > /etc/iptables.up.rules
		chmod +x /etc/network/if-pre-up.d/iptables
	fi
}
Set_key_word() { $1 -t mangle -$3 OUTPUT -m string --string "$2" --algo bm --to 65535 -j DROP; }
Set_tcp_port() {
	[[ "$1" = "$v4iptables" ]] && $1 -t filter -$3 OUTPUT -p tcp -m multiport --dports "$2" -m state --state NEW,ESTABLISHED -j REJECT --reject-with icmp-port-unreachable
	[[ "$1" = "$v6iptables" ]] && $1 -t filter -$3 OUTPUT -p tcp -m multiport --dports "$2" -m state --state NEW,ESTABLISHED -j REJECT --reject-with tcp-reset
}
Set_udp_port() { $1 -t filter -$3 OUTPUT -p udp -m multiport --dports "$2" -j DROP; }
Set_SPAM_Code_v4(){
	for i in ${smtp_port} ${pop3_port} ${imap_port} ${other_port}
		do
		Set_tcp_port $v4iptables "$i" $s
		Set_udp_port $v4iptables "$i" $s
	done
}
Set_SPAM_Code_v4_v6(){
	for i in ${smtp_port} ${pop3_port} ${imap_port} ${other_port}
	do
		for j in $v4iptables $v6iptables
		do
			Set_tcp_port $j "$i" $s
			Set_udp_port $j "$i" $s
		done
	done
}
Set_PORT(){
	if [[ -n "$v4iptables" ]] && [[ -n "$v6iptables" ]]; then
		Set_tcp_port $v4iptables $PORT $s
		Set_udp_port $v4iptables $PORT $s
		Set_tcp_port $v6iptables $PORT $s
		Set_udp_port $v6iptables $PORT $s
	elif [[ -n "$v4iptables" ]]; then
		Set_tcp_port $v4iptables $PORT $s
		Set_udp_port $v4iptables $PORT $s
	fi
	Save_iptables_v4_v6
}
Set_KEY_WORDS(){
	key_word_num=$(echo -e "${key_word}"|wc -l)
	for((integer = 1; integer <= ${key_word_num}; integer++))
		do
			i=$(echo -e "${key_word}"|sed -n "${integer}p")
			Set_key_word $v4iptables "$i" $s
			[[ ! -z "$v6iptables" ]] && Set_key_word $v6iptables "$i" $s
	done
	Save_iptables_v4_v6
}
Set_BT(){
	key_word=${bt_key_word}
	Set_KEY_WORDS
	Save_iptables_v4_v6
}
Set_SPAM(){
	if [[ -n "$v4iptables" ]] && [[ -n "$v6iptables" ]]; then
		Set_SPAM_Code_v4_v6
	elif [[ -n "$v4iptables" ]]; then
		Set_SPAM_Code_v4
	fi
	Save_iptables_v4_v6
}
Set_ALL(){
	Set_BT
	Set_SPAM
}
Ban_BT(){
	check_BT
	[[ ! -z ${BT_KEY_WORDS} ]] && echo -e "${Error} Torrent bloqueados y Palabras Claves, no es\nnecesario volver a prohibirlas !" && msg -bar && exit 0
	s="A"
	Set_BT
	View_ALL
	echo -e "${Info} Torrent bloqueados y Palabras Claves !"
	msg -bar
}
Ban_SPAM(){
	check_SPAM
	[[ ! -z ${SPAM_PORT} ]] && echo -e "${Error} Se detectó un puerto SPAM bloqueado, no es\nnecesario volver a bloquear !" && msg -bar && exit 0
	s="A"
	Set_SPAM
	View_ALL
	echo -e "${Info} Puertos SPAM Bloqueados !"
	msg -bar
}
Ban_ALL(){
	check_BT
	check_SPAM
	s="A"
	if [[ -z ${BT_KEY_WORDS} ]]; then
		if [[ -z ${SPAM_PORT} ]]; then
			Set_ALL
			View_ALL
			echo -e "${Info} Torrent bloqueados, Palabras Claves y Puertos SPAM !"
			msg -bar
		else
			Set_BT
			View_ALL
			echo -e "${Info} Torrent bloqueados y Palabras Claves !"
		fi
	else
		if [[ -z ${SPAM_PORT} ]]; then
			Set_SPAM
			View_ALL
			echo -e "${Info} Puerto SPAM (spam) prohibido !"
		else
			echo -e "${Error} Torrent Bloqueados, Palabras Claves y Puertos SPAM,\nno es necesario volver a prohibir !" && msg -bar && exit 0
		fi
	fi
}
UnBan_BT(){
	check_BT
	[[ -z ${BT_KEY_WORDS} ]] && echo -e "${Error} Torrent y Palabras Claves no bloqueadas, verifique !"&& msg -bar && exit 0
	s="D"
	Set_BT
	View_ALL
	echo -e "${Info} Torrent Desbloqueados y Palabras Claves !"
	msg -bar
}
UnBan_SPAM(){
	check_SPAM
	[[ -z ${SPAM_PORT} ]] && echo -e "${Error} Puerto SPAM no detectados, verifique !" && msg -bar && exit 0
	s="D"
	Set_SPAM
	View_ALL
	echo -e "${Info} Puertos de SPAM Desbloqueados !"
	msg -bar
}
UnBan_ALL(){
	check_BT
	check_SPAM
	s="D"
	if [[ ! -z ${BT_KEY_WORDS} ]]; then
		if [[ ! -z ${SPAM_PORT} ]]; then
			Set_ALL
			View_ALL
			echo -e "${Info} Torrent, Palabras Claves y Puertos SPAM Desbloqueados !"
			msg -bar
		else
			Set_BT
			View_ALL
			echo -e "${Info} Torrent, Palabras Claves Desbloqueados !"
			msg -bar
		fi
	else
		if [[ ! -z ${SPAM_PORT} ]]; then
			Set_SPAM
			View_ALL
			echo -e "${Info} Puertos SPAM Desbloqueados !"
			msg -bar
		else
			echo -e "${Error} No se  detectan Torrent, Palabras Claves y Puertos SPAM Bloqueados, verifique !" && msg -bar && exit 0
		fi
	fi
}
ENTER_Ban_KEY_WORDS_type(){
	Type=$1
	Type_1=$2
	if [[ $Type_1 != "ban_1" ]]; then
		echo -e "Por favor seleccione un tipo de entrada：
		
 1. Entrada manual (solo se admiten palabras clave únicas)
 
 2. Lectura local de archivos (admite lectura por lotes de palabras clave, una palabra clave por línea)
 
 3. Lectura de dirección de red (admite lectura por lotes de palabras clave, una palabra clave por línea)" && echo
		read -e -p "(Por defecto: 1. Entrada manual):" key_word_type
	fi
	[[ -z "${key_word_type}" ]] && key_word_type="1"
	if [[ ${key_word_type} == "1" ]]; then
		if [[ $Type == "ban" ]]; then
			ENTER_Ban_KEY_WORDS
		else
			ENTER_UnBan_KEY_WORDS
		fi
	elif [[ ${key_word_type} == "2" ]]; then
		ENTER_Ban_KEY_WORDS_file
	elif [[ ${key_word_type} == "3" ]]; then
		ENTER_Ban_KEY_WORDS_url
	else
		if [[ $Type == "ban" ]]; then
			ENTER_Ban_KEY_WORDS
		else
			ENTER_UnBan_KEY_WORDS
		fi
	fi
}
ENTER_Ban_PORT(){
	echo -e "Ingrese el puerto que Bloqueará:\n(segmento de Puerto único / Puerto múltiple / Puerto continuo)\n"
	if [[ ${Ban_PORT_Type_1} != "1" ]]; then
	echo -e "
	${Green_font_prefix}======== Ejemplo Descripción ========${Font_color_suffix}
	
 -Puerto único: 25 (puerto único)
 
 -Multipuerto: 25, 26, 465, 587 (varios puertos están separados por comas)

 -Segmento de puerto continuo: 25: 587 (todos los puertos entre 25-587)" && echo
	fi
	read -e -p "(Intro se cancela por defecto):" PORT
	[[ -z "${PORT}" ]] && echo "Cancelado..." && View_ALL && exit 0
}
ENTER_Ban_KEY_WORDS(){
    msg -bar
	echo -e "Ingrese las palabras clave que se prohibirán\n(nombre de dominio, etc., solo admite una sola palabra clave)"
	if [[ ${Type_1} != "ban_1" ]]; then
	echo ""
	echo -e "${Green_font_prefix}======== Ejemplo Descripción ========${Font_color_suffix}
	
 -Palabras clave: youtube, que prohíbe el acceso a cualquier nombre de dominio que contenga la palabra clave youtube.
 
 -Palabras clave: youtube.com, que prohíbe el acceso a cualquier nombre de dominio (máscara de nombre de pan-dominio) que contenga la palabra clave youtube.com.

 -Palabras clave: www.youtube.com, que prohíbe el acceso a cualquier nombre de dominio (máscara de subdominio) que contenga la palabra clave www.youtube.com.

 -Autoevaluación de más efectos (como la palabra clave .zip se puede usar para deshabilitar la descarga de cualquier archivo de sufijo .zip)." && echo
	fi
	read -e -p "(Intro se cancela por defecto):" key_word
	[[ -z "${key_word}" ]] && echo "Cancelado ..." && View_ALL && exit 0
}
ENTER_Ban_KEY_WORDS_file(){
	echo -e "Ingrese el archivo local de palabras clave que se prohibirá / desbloqueará (utilice la ruta absoluta)" && echo
	read -e -p "(El valor predeterminado es leer key_word.txt en el mismo directorio que el script):" key_word
	[[ -z "${key_word}" ]] && key_word="key_word.txt"
	if [[ -e "${key_word}" ]]; then
		key_word=$(cat "${key_word}")
		[[ -z ${key_word} ]] && echo -e "${Error} El contenido del archivo está vacío. !" && View_ALL && exit 0
	else
		echo -e "${Error} Archivo no encontrado ${key_word} !" && View_ALL && exit 0
	fi
}
ENTER_Ban_KEY_WORDS_url(){
	echo -e "Ingrese la dirección del archivo de red de palabras clave que se prohibirá / desbloqueará (por ejemplo, http: //xxx.xx/key_word.txt)" && echo
	read -e -p "(Intro se cancela por defecto):" key_word
	[[ -z "${key_word}" ]] && echo "Cancelado ..." && View_ALL && exit 0
	key_word=$(wget --no-check-certificate -t3 -T5 -qO- "${key_word}")
	[[ -z ${key_word} ]] && echo -e "${Error} El contenido del archivo de red está vacío o se agotó el tiempo de acceso !" && View_ALL && exit 0
}
ENTER_UnBan_KEY_WORDS(){
	View_KEY_WORDS
	echo -e "Ingrese la palabra clave que desea desbloquear (ingrese la palabra clave completa y precisa de acuerdo con la lista anterior)" && echo
	read -e -p "(Intro se cancela por defecto):" key_word
	[[ -z "${key_word}" ]] && echo "Cancelado ..." && View_ALL && exit 0
}
ENTER_UnBan_PORT(){
	echo -e "Ingrese el puerto que desea desempaquetar:\n(ingrese el puerto completo y preciso de acuerdo con la lista anterior, incluyendo comas, dos puntos)" && echo
	read -e -p "(Intro se cancela por defecto):" PORT
	[[ -z "${PORT}" ]] && echo "Cancelado ..." && View_ALL && exit 0
}
Ban_PORT(){
	s="A"
	ENTER_Ban_PORT
	Set_PORT
	echo -e "${Info} Puerto bloqueado [ ${PORT} ] !\n"
	Ban_PORT_Type_1="1"
	while true
	do
		ENTER_Ban_PORT
		Set_PORT
		echo -e "${Info} Puerto bloqueado [ ${PORT} ] !\n"
	done
	View_ALL
}
Ban_KEY_WORDS(){
	s="A"
	ENTER_Ban_KEY_WORDS_type "ban"
	Set_KEY_WORDS
	echo -e "${Info} Palabras clave bloqueadas [ ${key_word} ] !\n"
	while true
	do
		ENTER_Ban_KEY_WORDS_type "ban" "ban_1"
		Set_KEY_WORDS
		echo -e "${Info} Palabras clave bloqueadas [ ${key_word} ] !\n"
	done
	View_ALL
}
UnBan_PORT(){
	s="D"
	View_PORT
	[[ -z ${Ban_PORT_list} ]] && echo -e "${Error} Se detecta cualquier puerto no bloqueado !" && exit 0
	ENTER_UnBan_PORT
	Set_PORT
	echo -e "${Info} Puerto decapsulado [ ${PORT} ] !\n"
	while true
	do
		View_PORT
		[[ -z ${Ban_PORT_list} ]] && echo -e "${Error} No se detecta puertos bloqueados !" && msg -bar && exit 0
		ENTER_UnBan_PORT
		Set_PORT
		echo -e "${Info} Puerto decapsulado [ ${PORT} ] !\n"
	done
	View_ALL
}
UnBan_KEY_WORDS(){
	s="D"
	Cat_KEY_WORDS
	[[ -z ${Ban_KEY_WORDS_list} ]] && echo -e "${Error} No se ha detectado ningún bloqueo !" && exit 0
	ENTER_Ban_KEY_WORDS_type "unban"
	Set_KEY_WORDS
	echo -e "${Info} Palabras clave desbloqueadas [ ${key_word} ] !\n"
	while true
	do
		Cat_KEY_WORDS
		[[ -z ${Ban_KEY_WORDS_list} ]] && echo -e "${Error} No se ha detectado ningún bloqueo !" && msg -bar && exit 0
		ENTER_Ban_KEY_WORDS_type "unban" "ban_1"
		Set_KEY_WORDS
		echo -e "${Info} Palabras clave desbloqueadas [ ${key_word} ] !\n"
	done
	View_ALL
}
UnBan_KEY_WORDS_ALL(){
	Cat_KEY_WORDS
	[[ -z ${Ban_KEY_WORDS_text} ]] && echo -e "${Error} No se detectó ninguna clave, verifique !" && msg -bar && exit 0
	if [[ ! -z "${v6iptables}" ]]; then
		Ban_KEY_WORDS_v6_num=$(echo -e "${Ban_KEY_WORDS_v6_list}"|wc -l)
		for((integer = 1; integer <= ${Ban_KEY_WORDS_v6_num}; integer++))
			do
				${v6iptables} -t mangle -D OUTPUT 1
		done
	fi
	Ban_KEY_WORDS_num=$(echo -e "${Ban_KEY_WORDS_list}"|wc -l)
	for((integer = 1; integer <= ${Ban_KEY_WORDS_num}; integer++))
		do
			${v4iptables} -t mangle -D OUTPUT 1
	done
	Save_iptables_v4_v6
	View_ALL
	echo -e "${Info} Todas las palabras clave han sido desbloqueadas !"
}
check_iptables(){
	v4iptables=`iptables -V`
	v6iptables=`ip6tables -V`
	if [[ ! -z ${v4iptables} ]]; then
		v4iptables="iptables"
		if [[ ! -z ${v6iptables} ]]; then
			v6iptables="ip6tables"
		fi
	else
		echo -e "${Error} El firewall de iptables no está instalado !
Por favor, instale el firewall de iptables：
CentOS Sistema： yum install iptables -y
Debian / Ubuntu Sistema： apt-get install iptables -y"
	fi
}
Update_Shell(){
	sh_new_ver=$(wget --no-check-certificate -qO- -t1 -T3 "https://raw.githubusercontent.com/emirjorge/Script-Z/master/CHUMO/Recursos/blockT.sh"|grep 'sh_ver="'|awk -F "=" '{print $NF}'|sed 's/\"//g'|head -1)
	[[ -z ${sh_new_ver} ]] && echo -e "${Error} No se puede vincular a Github !" && exit 0
	wget https://raw.githubusercontent.com/emirjorge/Script-Z/master/CHUMO/Recursos/blockT.sh -O /etc/ger-frm/blockBT.sh &> /dev/null
	chmod +x /etc/ger-frm/blockBT.sh
	echo -e "El script ha sido actualizado a la última versión.[ ${sh_new_ver} ]"
	msg -bar 
	exit 0
}
check_sys
check_iptables
action=$1
if [[ ! -z $action ]]; then
	[[ $action = "banbt" ]] && Ban_BT && exit 0
	[[ $action = "banspam" ]] && Ban_SPAM && exit 0
	[[ $action = "banall" ]] && Ban_ALL && exit 0
	[[ $action = "unbanbt" ]] && UnBan_BT && exit 0
	[[ $action = "unbanspam" ]] && UnBan_SPAM && exit 0
	[[ $action = "unbanall" ]] && UnBan_ALL && exit 0
fi
echo -e "  Panel de Firewall ${Red_font_prefix}[v${sh_ver}]${Font_color_suffix}"
msg -bar
echo -e "  ${Green_font_prefix}0.${Font_color_suffix} Ver la lista actual de prohibidos
————————————
  ${Green_font_prefix}1.${Font_color_suffix} Bloquear Torrent, Palabras Clave
  ${Green_font_prefix}2.${Font_color_suffix} Bloquear Puertos SPAM 
  ${Green_font_prefix}3.${Font_color_suffix} Bloquear Torrent, Palabras Clave + Puertos SPAM
  ${Green_font_prefix}4.${Font_color_suffix} Bloquear Puerto personalizado
  ${Green_font_prefix}5.${Font_color_suffix} Bloquear Palabras Clave Personalizadas
————————————
  ${Green_font_prefix}6.${Font_color_suffix} Desbloquear Torrent, Palabras Clave
  ${Green_font_prefix}7.${Font_color_suffix} Desbloquear Puertos SPAM
  ${Green_font_prefix}8.${Font_color_suffix} Desbloquear Torrent, Palabras Clave , Puertos SPAM
  ${Green_font_prefix}9.${Font_color_suffix} Desbloquear Puerto Personalizado
 ${Green_font_prefix}10.${Font_color_suffix} Desbloquear Palabra Clave Personalizadas
 ${Green_font_prefix}11.${Font_color_suffix} Desbloquear Todas las palabras Clave Personalizadas
————————————
 ${Green_font_prefix}12.${Font_color_suffix} Actualizar script" && msg -bar
read -e -p " Por favor ingrese un número [0-12]:" num && msg -bar
case "$num" in
	0)
	View_ALL
	;;
	1)
	Ban_BT
	;;
	2)
	Ban_SPAM
	;;
	3)
	Ban_ALL
	;;
	4)
	Ban_PORT
	;;
	5)
	Ban_KEY_WORDS
	;;
	6)
	UnBan_BT
	;;
	7)
	UnBan_SPAM
	;;
	8)
	UnBan_ALL
	;;
	9)
	UnBan_PORT
	;;
	10)
	UnBan_KEY_WORDS
	;;
	11)
	UnBan_KEY_WORDS_ALL
	;;
	12)
	Update_Shell
	;;
	*)
	echo "Por favor ingrese el número correcto [0-12]"
	;;
esac
}

function socks5(){
ip=$(wget -qO- ifconfig.me);MYIP=$(wget -qO- ifconfig.me)

	function extras(){
	clear
	if [[ $1 == "" ]]
	then
	figlet -p -f slant < /root/name | lolcat
	echo -e "\033[1;37m      【     ★ Reseller :$(cat < /etc/adm-lite/menu_credito) - ADM 2023 ★      】\033[0m"
	echo -e "[\033[1;31m-\033[1;33m]\033[1;31m #######################################\033[1;33m"
	echo -e "\033[1;37mSeleccione una opcion:    Para Salir Ctrl + C\033[1;33m
[\033[1;30m1\033[1;33m] SOCKS5                ›   \033[1;32m$xsocks5 \033[1;33m
[\033[1;30m2\033[1;33m] SOCKS5 (Sockd)        ›   \033[1;32m$xsockd \033[1;33m
[\033[1;30m3\033[1;33m] SOCKS5 (Microsocks)   ›   \033[1;32m$xmicro \033[1;33m
[\033[1;30m0\033[1;33m] < REGRESAR                 \033[1;33m"
	read -p ": " opcao
	else
	opcao=$1
	fi
	case $opcao in
	1)
	socks5;;
	2)
	socks5alter;;
	3)
	microsocks;;
	0)
	exit;;
	esac
	}
	function microsocks (){
	killall microsocks
	echo -e "Instalando Microsocks espere.."
	cd /etc/adm-lite/
	rm -rf /etc/adm-lite/microsocks/ 1> /dev/null 2> /dev/null
	git clone http://github.com/rofl0r/microsocks.git 1> /dev/null 2> /dev/null
	cd /etc/adm-lite/microsocks/
	make 1> /dev/null 2> /dev/null
	make install 1> /dev/null 2> /dev/null
	cd /root
	echo -e "⎇⇥ Escriba un nombre de usuario nuevo"
	read -p ": " microuser
	useradd --shell /usr/sbin/nologin $microuser
	echo -e "⎇⇥ Asigna un password para la cuenta microsocks, repitelo dos veces"
	passwd $microuser
	echo -e "⎇⇥ Escribe el mismo password asignado nuevamente"
	read -p ": " clavemicro
	echo -e "⎇⇥ Escribe un puerto libre para Microsocks"
	read -p ": " puertomicro
	if lsof -Pi :$puertomicro -sTCP:LISTEN -t >/dev/null ; then
	echo "Ya esta en uso ese puerto"
	else
	screen -dmS micro microsocks -1 -i 0.0.0.0 -p $puertomicro -u $microuser -P $clavemicro -b bindaddr
	echo -e "╼╼╼╼⌁⌁⌁◅⌁▻⌁⌁⌁╾╾╾╾"
	echo -e "⌬ Servidor Socks5 [ microsocks ] iniciado"
	echo -e "⌁ IP : $MYIP"
	echo -e "⌁ Puerto : $puertomicro"
	echo -e "⌁ Usuario : $microuser"
	echo -e "⌁ Password : $clavemicro"
	fi
	}
	function socks5alter(){
	echo -e "Instalando Sockd espere..."
	cd /etc/adm-lite/ 1> /dev/null 2> /dev/null
	rm /etc/adm-lite/dante-1.4.1.tar.gz 1> /dev/null 2> /dev/null
	wget --no-check-certificate https://raw.githubusercontent.com/emirjorge/Script-Z/master/CHUMO/Recursos/menu_inst/socks5-sh/dante-1.4.1.tar.gz 1> /dev/null 2> /dev/null
	tar -zxvf dante-1.4.1.tar.gz 1> /dev/null 2> /dev/null
	cd /etc/adm-lite/dante-1.4.1/ 1> /dev/null 2> /dev/null
	mkdir /home/dante 1> /dev/null 2> /dev/null
	./configure --prefix=/home/dante 1> /dev/null 2> /dev/null
	make 1> /dev/null 2> /dev/null
	make install 1> /dev/null 2> /dev/null
	wget -O /home/dante/danted.conf https://raw.githubusercontent.com/emirjorge/Script-Z/master/CHUMO/Recursos/menu_inst/socks5-sh/danted.conf 1> /dev/null 2> /dev/null
	echo -e "Escribe un puerto para Sockd"
	read -p ": " sockcincox
	if lsof -Pi :$sockcincox -sTCP:LISTEN -t >/dev/null ; then
	echo "Ya esta en uso ese puerto"
	else
	sed -i '2i internal: 0.0.0.0 port = '"$sockcincox"'' /home/dante/danted.conf
	sed -i '3i external: '"$MYIP"'' /home/dante/danted.conf
	#
	echo "Finalizando Instalacion"
	screen -dmS sockdx /home/dante/sbin/sockd -f /home/dante/danted.conf
	#¿
	menu
	fi
	}
	function socks5(){
	echo -e "Instalando Socks5 espere..."
	apt remove dante-server -y 1> /dev/null 2> /dev/null
	apt purge dante-server -y 1> /dev/null 2> /dev/null
	apt install dante-server -y 1> /dev/null 2> /dev/null
	#rm /etc/danted.conf 1> /dev/null 2> /dev/null
	#cp /etc/adm-lite/danted.conf /etc/danted.conf 1> /dev/null 2> /dev/null
	echo -e "Escribe un puerto para Socks5"
	read -p ": " sockcinco
	if lsof -Pi :$sockcinco -sTCP:LISTEN -t >/dev/null ; then
	echo "Ya esta en uso ese puerto"
	else
	sed -i '5i internal: 0.0.0.0 port = '"$sockcinco"'' /etc/danted.conf
	sed -i '6i external: '"$ip"'' /etc/danted.conf
	#
		echo "Finalizando Instalacion"
	systemctl restart danted
	#
	menu
	fi
	}

extras
}
function h_beta(){
	verif_ptrs() {
		porta=$1
		PT=$(lsof -V -i tcp -P -n | grep -v "ESTABLISHED" | grep -v "COMMAND" | grep "LISTEN")
		for pton in $(echo -e "$PT" | cut -d: -f2 | cut -d' ' -f1 | uniq); do
			svcs=$(echo -e "$PT" | grep -w "$pton" | awk '{print $1}' | uniq)
			[[ "$porta" = "$pton" ]] && {
				echo -e "\n\033[1;31mPUERTO \033[1;33m$porta \033[1;31mOCUPADO POR \033[1;37m$svcs\033[0m"
				sleep 0.5
				return 0
			}
		done
	}
fun_openssh() {
		clear
		echo -e "\E[44;1;37m            OPENSSH             \E[0m\n"
		echo -e "\033[1;31m[\033[1;36m1\033[1;31m] \033[1;37m• \033[1;33mADICIONAR PORTA\033[1;31m
[\033[1;36m2\033[1;31m] \033[1;37m• \033[1;33mREMOVER PORTA\033[1;31m
[\033[1;36m3\033[1;31m] \033[1;37m• \033[1;33mVOLTAR\033[0m"
		echo ""
		echo -ne "\033[1;32mOQUE DESEJA FAZER \033[1;33m?\033[1;37m "
		read resp
		if [[ "$resp" = '1' ]]; then
			clear
			echo -e "\E[44;1;37m         ADICIONAR PORTA AO SSH         \E[0m\n"
			echo -ne "\033[1;32mQUAL PORTA DESEJA ADICIONAR \033[1;33m?\033[1;37m "
			read pt
			[[ -z "$pt" ]] && {
				echo -e "\n\033[1;31mPorta invalida!"
				sleep 3
				return 0
			}
			verif_ptrs $pt
			echo -e "\n\033[1;32mADICIONANDO PORTA AO SSH\033[0m"
			echo ""
			fun_addpssh() {
				echo "Port $pt" >>/etc/ssh/sshd_config
				service ssh restart
			}
			fun_bar 'fun_addpssh'
			echo -e "\n\033[1;32mPORTA ADICIONADA COM SUCESSO\033[0m"
			sleep 3
			return 0
		elif [[ "$resp" = '2' ]]; then
			clear
			echo -e "\E[41;1;37m         REMOVER PORTA DO SSH         \E[0m"
			echo -e "\n\033[1;33m[\033[1;31m!\033[1;33m] \033[1;32mPORTA PADRAO \033[1;37m22 \033[1;33mCUIDADO !\033[0m"
			echo -e "\n\033[1;33mPUERTAS SSH EN USO: \033[1;37m$(grep 'Port' /etc/ssh/sshd_config | cut -d' ' -f2 | grep -v 'no' | xargs)\n"
			echo -ne "\033[1;32mQUE PUERTO DESEAS REMOVER \033[1;33m?\033[1;37m "
			read pt
			[[ -z "$pt" ]] && {
				echo -e "\n\033[1;31mPUERTO INVALIDO!"
				sleep 2
				return 0
			}
			[[ $(grep -wc "$pt" '/etc/ssh/sshd_config') != '0' ]] && {
				echo -e "\n\033[1;32mREMOVENDO PUERTO DE SSH\033[0m"
				echo ""
				fun_delpssh() {
					sed -i "/Port $pt/d" /etc/ssh/sshd_config
					service ssh restart
				}
				fun_bar 'fun_delpssh'
				echo -e "\n\033[1;32mPORTA REMOVIDA COM SUCESSO\033[0m"
				sleep 2
				return 0
			} || {
				echo -e "\n\033[1;31mPorta invalida!"
				sleep 2
				return 0
			}
		elif [[ "$resp" = '3' ]]; then
			echo -e "\n\033[1;31mRetornando.."
			sleep 2
			return 0
		else
			echo -e "\n\033[1;31mOpcao invalida!"
			sleep 2
			return 0
		fi
	}


menu_udp () {
_udp=$(lsof -V -i tcp -P -n | grep -v "ESTABLISHED" |grep -v "COMMAND"|grep "badvpn-ud"|awk '{print $1}')
[[ -z $_udp ]] && v_udp="\e[31m[ OFF ]" || v_udp="\e[32m[ ON ] " 
msg -bar 
echo -e " \033[0;35m [\033[0;36m1\033[0;35m]\033[0;31m ➮ ${cor[3]} PARAR TODOS LOS BADVPN $v_udp"
echo -e " \033[0;35m [\033[0;36m2\033[0;35m]\033[0;31m ➮ ${cor[3]} ADD + BADVPN ( CUSTOM PORT )"
#echo -e " \033[0;35m [\033[0;36m2\033[0;35m]\033[0;31m ➮ ${cor[3]} AGREGAR / REMOVER HOST-SQUID"
#echo -e " \033[0;35m [\033[0;36m3\033[0;35m]\033[0;31m ➮ ${cor[3]} DESINSTALAR SQUID"
msg -bar
echo -e " \033[0;35m [\033[0;36m0\033[0;35m]\033[0;31m ➮ $(msg -bra "\033[1;41m[ REGRESAR ]\e[0m")"
msg -bar
selection=$(selection_fun 2)
case ${selection} in
0)
return 0
;;
1)
for pid in $(pgrep badvpn-udpgw);do 
kill $pid 
done
return 0
;;
2)
badcustom
return 0
;;
esac
}

badcustom () {
msg -bar
echo -e "BIENVENIDO AL MENU DE CUSTOM PORT "
msg -bar 
read -p " DIJITA TU PUERTO CUSTOM PARA BADVPN :" -e -i "7100" port
echo -e " VERIFICANDO BADVPN "
msg -bar 
screen -dmS badvpn$port /bin/badvpn-udpgw --listen-addr 127.0.0.1:${port} --max-clients 10000 --max-connections-for-client 10000 --client-socket-sndbuf 10000 && msg -ama "               BadVPN ACTIVADA CON EXITO"  || msg -ama "                Error al Activar BadVPN" 
echo -e "netstat -tlpn | grep -w ${port} > /dev/null || {  screen -r -S 'badvpn'$port -X quit;  screen -dmS badvpn $(which badvpn-udpgw) --listen-addr 127.0.0.1:${port} --max-clients 10000 --max-connections-for-client 10000 --client-socket-sndbuf 10000; }" >>/bin/autoboot
msg -bar
#echo -e ""

}

_badfix () {
https://github.com/rudi9999/ADMRufu/raw/main/Utils/badvpn/badvpn-master.zip

}

packobs () {
msg -ama "Buscando Paquetes Obsoletos"
dpkg -l | grep -i ^rc
msg -ama "Limpiando Paquetes Obsoloteos"
dpkg -l |grep -i ^rc | cut -d " " -f 3 | xargs dpkg --purge
sudo sync
sudo sysctl -w vm.drop_caches=3 > /dev/null 2>&1
msg -ama "Limpieza Completa"
}

############

SCPdir="/etc/adm-lite" 
#SCPfrm="${SCPdir}" && [[ ! -d ${SCPfrm} ]] && exit 
#SCPinst="${SCPdir}"&& [[ ! -d ${SCPinst} ]] && exit 
#declare -A cor=( [0]="\033[1;37m" [1]="\033[1;34m" [2]="\033[1;31m" [3]="\033[1;33m" [4]="\033[1;32m" [5]="\e[1;36m" )

#LISTA PORTAS
mportas () {
unset portas
portas_var=$(lsof -V -i tcp -P -n | grep -v "ESTABLISHED" |grep -v "COMMAND" | grep "LISTEN")
while read port; do
var1=$(echo $port | awk '{print $1}') && var2=$(echo $port | awk '{print $9}' | awk -F ":" '{print $2}')
[[ "$(echo -e $portas|grep "$var1:$var2")" ]] || portas+="$var1:$var2\n"
done <<< "$portas_var"
i=1
echo -e "$portas"
} 


fun_apache () {
echo -e "FUNCION DE MENU APACHE MODO BETA"
msg -bar
read -p " INGRESA PUERTO APACHE NUEVO :" nwPP
[[ -z $nwPP ]] && nwPP="81"
msg -bar
echo "ESPERE MIENTRAS COMPLETAMOS EL PROCESO"
fun_bar "apt purge apache2 -y " 
echo "REINSTALANDO Y RECONFIGURANDO"
fun_bar "apt install apache2 -y "
sed -i "s;Listen 80;Listen ${nwPP};g" /etc/apache2/ports.conf
echo "REINICIANDO Y APLICANDO CAMBIOS"
service apache2 restart &>/dev/null 
}

filemanager () {
[[ $(ps x | grep filebrowser | grep -v grep) ]] && {
killall filebrowser &> /dev/null
} || {
[[ -z $(which filebrowser) ]] && curl -fsSL https://raw.githubusercontent.com/filebrowser/get/master/get.sh | bash &> /dev/null
read -p " INGRESA PUERTO : " webrowser
nohup filebrowser -a $(ip -4 addr | sed -ne 's|^.* inet \([^/]*\)/.* scope global.*$|\1|p' | awk '{print $1}' | head -1) -b / -p ${webrowser} -r /root/& > /dev/null
msg -bar 
echo ""
echo -e " SERVICIO ACTIVO EN URL : http://$(wget -qO- ifconfig.me):${webrowser}/"
echo ""
echo -e " ACCEDE CON LAS CREDENCIALES : admin "
echo ""
msg -bar
}
read -p " PRESIONA ENTER PARA CONTINUAR"
}


if netstat -tnlp |grep 'apache2' &>/dev/null; then 
_apa="\e[32m[ ON ] " 
else 
_apa="\e[31m[ OFF ]" 
fi 
clear&&clear
[[ -e /etc/wireguard/params ]] && _wir="\e[32m[ ON ] " || _wir="\e[31m[ OFF ]" 
[[ $(ps x | grep filebrowser | grep -v grep) ]] && file="\e[32m[ ON ] " || file="\e[31m[ OFF ]" 
msg -bar
echo -e " \033[7;49;35m =>►► 🦖  FUNCIONES GENERALES  🦖◄◄<= \033[0m\033[1;31m"
msg -bar
echo -e " \033[0;35m[\033[0;36m1\033[0;35m] \033[0;34m➮\033[0;33m PUERTO APACHE CUSTOM ${_apa}      "
echo -e " \033[0;35m[\033[0;36m2\033[0;35m] \033[0;34m➮\033[0;33m LIMPIAR RAM && PAQUETES ANTIGUOS  "
echo -e " \033[0;35m[\033[0;36m3\033[0;35m] \033[0;34m➮\033[0;31m ADD / REMOVE PORTS CUSTOM BADVPN  "
echo -e " \033[0;35m[\033[0;36m4\033[0;35m] \033[0;34m➮\033[0;31m ADD / REMOVE PORTS CUSTOM OPENSSH "
echo -e " \033[0;35m[\033[0;36m5\033[0;35m] \033[0;34m➮\033[0;31m TROJAN GO - BETA                  "
echo -e " \033[0;35m[\033[0;36m6\033[0;35m] \033[0;34m➮\033[0;31m CREAR CERTIFICADO CON DOMINIO     "
echo -e " \033[0;35m[\033[0;36m7\033[0;35m] \033[0;34m➮\033[0;31m Modulo WireGuard VPN Client ${_wir}    "
echo -e " \033[0;35m[\033[0;36m8\033[0;35m] \033[0;34m➮\033[0;31m FIILEMANAGER WEB ${file}    "
msg -bar
echo -e " \033[0;35m[\033[0;36m0\033[0;35m] \033[0;34m➮\033[0;33m $(msg -bra "\033[1;41m[ REGRESAR ]\e[0m")  "
msg -bar
  selection=$(selection_fun 8)
case ${selection} in
0)
return 0
;;
1)
fun_apache
return 0
;;
2)
packobs
return 0
;;
3)
menu_udp
return 0
;;
4)
fun_openssh
return 0
;;
5)
source <(curl -sSL https://raw.githubusercontent.com/emirjorge/Script-Z/master/CHUMO/Recursos/menu_inst/h_beta-sh/trojan-go)
;;
6)
clear&&clear
echo -e "================================================"
echo -e "A CONTINUACION CREAREMOS UN CETIFICADO SSL"
echo -e "   LA VERIFICACION ES MEDIANTE DOMINIO"
echo -e "  NECECITAS TENER EL PUERTO 80 Y 443 LIBRES"
echo -e "================================================"
source <(curl -sSL https://raw.githubusercontent.com/emirjorge/Script-Z/master/CHUMO/Recursos/certificadossl.sh)
echo -e "================================================"
echo -e ""
echo -e "================================================"
echo -e " SI LA EMICION FUE CORRECTA, TU CERTIFICADO"
echo -e "        SE ENCUENTR ALOJADO EN /data "
echo -e "================================================"
echo -e "       /data/cert.crt && /data/cert.key "
echo -e "================================================"
echo -e ""
;;
7)
rm -f /tmp/wireguard-install.sh* && wget -q -O /tmp/wireguard-install.sh https://raw.githubusercontent.com/emirjorge/Script-Z/master/CHUMO/Recursos/menu_inst/h_beta-sh/wireguard-install.sh && chmod +x /tmp/wireguard-install.sh && /tmp/wireguard-install.sh
;;
8)
filemanager
;;
esac

}

function openvpn(){
clear&&clear
enter () {
read -p "Enter para Continuar"
}
# Funcoes Globais

mportas () {
unset portas
portas_var=$(lsof -V -i tcp -P -n | grep -v "ESTABLISHED" |grep -v "COMMAND" | grep "LISTEN") 
while read port; do 
var1=$(echo $port | awk '{print $1}') && var2=$(echo $port | awk '{print $9}' | awk -F ":" '{print $2}') 
[[ "$(echo -e $portas|grep "$var1 $var2")" ]] || portas+="$var1 $var2\n" 
done <<< "$portas_var" 
i=1 
echo -e "$portas" 
} 

print_center(){
    local x
    local y
    text="$*"
    x=$(( ($(tput cols) - ${#text}) / 2))
    echo -ne "\E[6n";read -sdR y; y=$(echo -ne "${y#*[}" | cut -d';' -f1)
    echo -ne "\033[${y};${x}f$*"
	echo ""
}

msg -bar


# Detect Debian users running the script with "sh" instead of bash
if readlink /proc/$$/exe | grep -q "dash"; then
	print_center  "Este script se utiliza con bash"
	enter
fi

if [[ "$EUID" -ne 0 ]]; then
	print_center  "Sorry, solo funciona como root"
	enter
fi

if [[ ! -e /dev/net/tun ]]; then
	print_center  "El TUN device no esta disponible"
	print_center  "Necesitas habilitar TUN antes de usar este script"
	enter
fi

if [[ -e /etc/debian_version ]]; then
	OS=debian
	GROUPNAME=nogroup
	RCLOCAL='/etc/rc.local'
elif [[ -e /etc/centos-release || -e /etc/redhat-release ]]; then
	OS=centos
	GROUPNAME=nobody
	RCLOCAL='/etc/rc.d/rc.local'
else
	print_center   "Sistema no compatible para este script"
	enter
fi
del(){
	for (( i = 0; i < $1; i++ )); do
		tput cuu1 && tput dl1
	done
}

agrega_dns(){
	msg -ama " Escriba el HOST DNS que desea Agregar"
	read -p " [NewDNS]: " SDNS
	cat /etc/hosts|grep -v "$SDNS" > /etc/hosts.bak && mv -f /etc/hosts.bak /etc/hosts
	if [[ -e /etc/opendns ]]; then
		cat /etc/opendns > /tmp/opnbak
		mv -f /tmp/opnbak /etc/opendns
		echo "$SDNS" >> /etc/opendns 
	else
		echo "$SDNS" > /etc/opendns
	fi
	[[ -z $NEWDNS ]] && NEWDNS="$SDNS" || NEWDNS="$NEWDNS $SDNS"
	unset SDNS
}

dns_fun(){
	case $1 in
		1)
			if grep -q "127.0.0.53" "/etc/resolv.conf"; then
				RESOLVCONF='/run/systemd/resolve/resolv.conf'
			else
				RESOLVCONF='/etc/resolv.conf'
			fi 
			grep -v '#' $RESOLVCONF | grep 'nameserver' | grep -E -o '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' | while read line; do
				echo "push \"dhcp-option DNS $line\"" >> /etc/openvpn/server.conf
			done;;
		2) #cloudflare
			echo 'push "dhcp-option DNS 1.1.1.1"' >> /etc/openvpn/server.conf
			echo 'push "dhcp-option DNS 1.0.0.1"' >> /etc/openvpn/server.conf;;
		3) #google
			echo 'push "dhcp-option DNS 8.8.8.8"' >> /etc/openvpn/server.conf
			echo 'push "dhcp-option DNS 8.8.4.4"' >> /etc/openvpn/server.conf;;
		4) #OpenDNS
			echo 'push "dhcp-option DNS 208.67.222.222"' >> /etc/openvpn/server.conf
			echo 'push "dhcp-option DNS 208.67.220.220"' >> /etc/openvpn/server.conf;;
		5) #Verisign
			echo 'push "dhcp-option DNS 64.6.64.6"' >> /etc/openvpn/server.conf
			echo 'push "dhcp-option DNS 64.6.65.6"' >> /etc/openvpn/server.conf;;
		6) #Quad9
			echo 'push "dhcp-option DNS 9.9.9.9"' >> /etc/openvpn/server.conf
			echo 'push "dhcp-option DNS 149.112.112.112"' >> /etc/openvpn/server.conf;;
		7) #UncensoredDNS
			echo 'push "dhcp-option DNS 91.239.100.100"' >> /etc/openvpn/server.conf
			echo 'push "dhcp-option DNS 89.233.43.71"' >> /etc/openvpn/server.conf;;
	esac
}

meu_ip




instala_ovpn(){
	clear
	msg -bar
	print_center "INSTALADOR DE OPENVPN"
	msg -bar
	# OpenVPN setup and first user creation
	msg -ama " Algunos ajustes son necesario para conf OpenVPN"
	msg -bar
	# Autodetect IP address and pre-fill for the user
	IP=$(ip addr | grep 'inet' | grep -v inet6 | grep -vE '127\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' | grep -oE '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' | head -1)
	if echo "$IP" | grep -qE '^(10\.|172\.1[6789]\.|172\.2[0-9]\.|172\.3[01]\.|192\.168)'; then
		PUBLICIP=$(wget -qO- ipv4.icanhazip.com)
	fi
	msg -ama "    Seleccione el protocolo de conexiones OpenVPN"
	msg -bar
	menu_func "UDP" "TCP"
	msg -bar
	while [[ -z $PROTOCOL ]]; do
		msg -ne " opcion: "
		read PROTOCOL
		case $PROTOCOL in
			1) PROTOCOL=udp; del "6"; msg -nazu " PROTOCOLO: "; msg -verd "UDP";;
			2) PROTOCOL=tcp; del "6"; msg -nazu " PROTOCOLO: "; msg -verd "TCP";;
			*) tput cuu1 && tput dl1; print_center -verm2 "selecciona una opcion entre 1 y 2"; sleep 2s; tput cuu1 && tput dl1; unset PROTOCOL;;
		esac
	done
	msg -bar
	print_center   "Ingresa un puerto OpenVPN (Default 1194)"
	msg -bar
	while [[ -z $PORT ]]; do
		msg -ne " Puerto: "
		read PORT
		if [[ -z $PORT ]]; then
			PORT="1194"
		elif [[ ! $PORT =~ $numero ]]; then
			tput cuu1 && tput dl1
			print_center -verm2 "ingresa solo numeros"
			sleep 2s
			tput cuu1 && tput dl1
			unset PORT
		fi

		[[ $(mportas|grep -w "${PORT}") ]] && {
			tput cuu1 && tput dl1
			print_center -verm2 "Puerto en uso"
			sleep 2s
			tput cuu1 && tput dl1
			unset PORT
        }
	done
	del "3"
	msg -nazu " PUERTO: "; msg -verd "$PORT"
	msg -bar
	print_center   "Seleccione DNS (default VPS)"
	msg -bar
	menu_func "DNS del Sistema" "Cloudflare" "Google" "OpenDNS" "Verisign" "Quad9" "UncensoredDNS"
	msg -bar
	while [[ -z $DNS ]]; do
		msg -ne " opcion: "
		read DNS
		if [[ -z $DNS ]]; then
			DNS="1"
		elif [[ ! $DNS =~ $numero ]]; then
			tput cuu1 && tput dl1
			print_center -verm2 "ingresa solo numeros"
			sleep 2s
			tput cuu1 && tput dl1
			unset DNS
		elif [[ $DNS != @([1-7]) ]]; then
			tput cuu1 && tput dl1
			print_center   "solo numeros entre 1 y 7"
			sleep 2s
			tput cuu1 && tput dl1
			unset DNS
		fi
	done
	case $DNS in
		1) P_DNS="DNS del Sistema";;
		2) P_DNS="Cloudflare";;
		3) P_DNS="Google";;
		4) P_DNS="OpenDNS";;
		5) P_DNS="Verisign";;
		6) P_DNS="Quad9";;
		7) P_DNS="UncensoredDNS";;
	esac
	del "11"
	msg -nazu " DNS: "; msg -verd "$P_DNS"
	msg -bar
	print_center   " Seleccione la codificacion para el canal de datos"
	msg -bar
	menu_func "AES-128-CBC" "AES-192-CBC" "AES-256-CBC" "CAMELLIA-128-CBC" "CAMELLIA-192-CBC" "CAMELLIA-256-CBC" "SEED-CBC" "NONE"
	msg -bar
	while [[ -z $CIPHER ]]; do
		msg -ne " opcion: "
		read CIPHER
		if [[ -z $CIPHER ]]; then
			CIPHER="1"
		elif [[ ! $CIPHER =~ $numero ]]; then
			tput cuu1 && tput dl1
			print_center -verm2 "ingresa solo numeros"
			sleep 2s
			tput cuu1 && tput dl1
			unset CIPHER
		elif [[ $CIPHER != @([1-8]) ]]; then
			tput cuu1 && tput dl1
			print_center   "solo numeros entre 1 y 8"
			sleep 2s
			tput cuu1 && tput dl1
			unset CIPHER
		fi
	done
	case $CIPHER in
		1) CIPHER="cipher AES-128-CBC";;
		2) CIPHER="cipher AES-192-CBC";;
		3) CIPHER="cipher AES-256-CBC";;
		4) CIPHER="cipher CAMELLIA-128-CBC";;
		5) CIPHER="cipher CAMELLIA-192-CBC";;
		6) CIPHER="cipher CAMELLIA-256-CBC";;
		7) CIPHER="cipher SEED-CBC";;
		8) CIPHER="cipher none";;
	esac
	del "12"
	codi=$(echo $CIPHER|awk -F ' ' '{print $2}')
	msg -nazu " CODIFICACION: "; msg -verd "$codi"
	msg -bar
	msg -ama " Estamos listos para configurar su servidor OpenVPN"
	enter
	if [[ "$OS" = 'debian' ]]; then
		apt-get update
		apt-get install openvpn iptables openssl ca-certificates -y
	else
		# 
		yum install epel-release -y
		yum install openvpn iptables openssl ca-certificates -y
	fi
	# Get easy-rsa
	EASYRSAURL='https://github.com/OpenVPN/easy-rsa/releases/download/v3.0.7/EasyRSA-3.0.7.tgz'
	wget -O ~/easyrsa.tgz "$EASYRSAURL" 2>/dev/null || curl -Lo ~/easyrsa.tgz "$EASYRSAURL"
	tar xzf ~/easyrsa.tgz -C ~/
	mv ~/EasyRSA-3.0.7/ /etc/openvpn/
	mv /etc/openvpn/EasyRSA-3.0.7/ /etc/openvpn/easy-rsa/
	chown -R root:root /etc/openvpn/easy-rsa/
	rm -f ~/easyrsa.tgz
	cd /etc/openvpn/easy-rsa/
	# 
	./easyrsa init-pki
	./easyrsa --batch build-ca nopass
	./easyrsa gen-dh
	./easyrsa build-server-full server nopass
	EASYRSA_CRL_DAYS=3650 ./easyrsa gen-crl
	# 
	cp pki/ca.crt pki/private/ca.key pki/dh.pem pki/issued/server.crt pki/private/server.key pki/crl.pem /etc/openvpn
	# 
	chown nobody:$GROUPNAME /etc/openvpn/crl.pem
	# 
	openvpn --genkey --secret /etc/openvpn/ta.key
	# 
	echo "port $PORT
proto $PROTOCOL
dev tun
sndbuf 0
rcvbuf 0
ca ca.crt
cert server.crt
key server.key
dh dh.pem
auth SHA512
tls-auth ta.key 0
topology subnet
server 10.8.0.0 255.255.255.0
ifconfig-pool-persist ipp.txt" > /etc/openvpn/server.conf
	echo 'push "redirect-gateway def1 bypass-dhcp"' >> /etc/openvpn/server.conf
	# DNS

	dns_fun "$DNS"
	
	echo "keepalive 10 120
${CIPHER}
user nobody
group $GROUPNAME
persist-key
persist-tun
status openvpn-status.log
verb 3
crl-verify crl.pem" >> /etc/openvpn/server.conf
updatedb
PLUGIN=$(locate openvpn-plugin-auth-pam.so | head -1)
[[ ! -z $(echo ${PLUGIN}) ]] && {
echo "client-to-client
client-cert-not-required
username-as-common-name
plugin $PLUGIN login" >> /etc/openvpn/server.conf
}
	# 
	echo 'net.ipv4.ip_forward=1' > /etc/sysctl.d/30-openvpn-forward.conf
	# 
	echo 1 > /proc/sys/net/ipv4/ip_forward
	if pgrep firewalld; then
		# 
		#
		# 
		# 
		firewall-cmd --zone=public --add-port=$PORT/$PROTOCOL
		firewall-cmd --zone=trusted --add-source=10.8.0.0/24
		firewall-cmd --permanent --zone=public --add-port=$PORT/$PROTOCOL
		firewall-cmd --permanent --zone=trusted --add-source=10.8.0.0/24
		# 
		firewall-cmd --direct --add-rule ipv4 nat POSTROUTING 0 -s 10.8.0.0/24 ! -d 10.8.0.0/24 -j SNAT --to $IP
		firewall-cmd --permanent --direct --add-rule ipv4 nat POSTROUTING 0 -s 10.8.0.0/24 ! -d 10.8.0.0/24 -j SNAT --to $IP
	else
		# 
		if [[ "$OS" = 'debian' && ! -e $RCLOCAL ]]; then
			echo '#!/bin/sh -e
exit 0' > $RCLOCAL
		fi
		chmod +x $RCLOCAL
		# 
		iptables -t nat -A POSTROUTING -s 10.8.0.0/24 ! -d 10.8.0.0/24 -j SNAT --to $IP
		sed -i "1 a\iptables -t nat -A POSTROUTING -s 10.8.0.0/24 ! -d 10.8.0.0/24 -j SNAT --to $IP" $RCLOCAL
		if iptables -L -n | grep -qE '^(REJECT|DROP)'; then
			#
			# 
			# 
			iptables -I INPUT -p $PROTOCOL --dport $PORT -j ACCEPT
			iptables -I FORWARD -s 10.8.0.0/24 -j ACCEPT
			iptables -I FORWARD -m state --state RELATED,ESTABLISHED -j ACCEPT
			sed -i "1 a\iptables -I INPUT -p $PROTOCOL --dport $PORT -j ACCEPT" $RCLOCAL
			sed -i "1 a\iptables -I FORWARD -s 10.8.0.0/24 -j ACCEPT" $RCLOCAL
			sed -i "1 a\iptables -I FORWARD -m state --state RELATED,ESTABLISHED -j ACCEPT" $RCLOCAL
		fi
	fi
	# 
	if sestatus 2>/dev/null | grep "Current mode" | grep -q "enforcing" && [[ "$PORT" != '1194' ]]; then
		# 
		if ! hash semanage 2>/dev/null; then
			yum install policycoreutils-python -y
		fi
		semanage port -a -t openvpn_port_t -p $PROTOCOL $PORT
	fi
	# 
	if [[ "$OS" = 'debian' ]]; then
		# 
		if pgrep systemd-journal; then
			systemctl restart openvpn@server.service
		else
			/etc/init.d/openvpn restart
		fi
	else
		if pgrep systemd-journal; then
			systemctl restart openvpn@server.service
			systemctl enable openvpn@server.service
		else
			service openvpn restart
			chkconfig openvpn on
		fi
	fi
	# 
	if [[ "$PUBLICIP" != "" ]]; then
		IP=$PUBLICIP
	fi
	# 
	echo "# OVPN_ACCESS_SERVER_PROFILE=ChuKK_SCRIPT
client
dev tun
proto $PROTOCOL
sndbuf 0
rcvbuf 0
remote $IP $PORT
resolv-retry infinite
nobind
persist-key
persist-tun
remote-cert-tls server
auth SHA512
${CIPHER}
setenv opt block-outside-dns
key-direction 1
verb 3
auth-user-pass" > /etc/openvpn/client-common.txt
clear
msg -bar
print_center -verd "Configuracion Finalizada!"
msg -bar
print_center   " Crear un usuario SSH para generar el (.ovpn)!"
enter
}

edit_ovpn_host(){
	msg -ama " CONFIGURACION HOST DNS OPENVPN"
	msg -bar
	while [[ $DDNS != @(n|N) ]]; do
		echo -ne "\033[1;33m"
		read -p " Agregar host [S/N]: " -e -i n DDNS
		[[ $DDNS = @(s|S|y|Y) ]] && agrega_dns
	done
	[[ ! -z $NEWDNS ]] && sed -i "/127.0.0.1[[:blank:]]\+localhost/a 127.0.0.1 $NEWDNS" /etc/hosts
	msg -bar
	msg -ama " Es Necesario el Reboot del Servidor Para"
	msg -ama " Para que las configuraciones sean efectudas"
	enter
}

fun_openvpn(){
	[[ -e /etc/openvpn/server.conf ]] && {
		unset OPENBAR
		[[ $(mportas|grep -w "openvpn") ]] && OPENBAR="\033[1;32m [ONLINE]" || OPENBAR="\033[1;31m [OFFLINE]"
		clear
		msg -bar
		echo -e   "CONFIGURACION OPENVPN"
		msg -bar
		echo -e " \033[0;35m[\033[0;36m1\033[0;35m] \033[0;34m➮\033[0;33m $(msg -verd 'INICIAR O PARAR OPENVPN') $OPENBAR" 
		echo -e " \033[0;35m[\033[0;36m2\033[0;35m] \033[0;34m➮\033[0;33m EDITAR CONFIGURACION CLIENTE $(msg -ama "(MEDIANTE NANO)")" 
		echo -e " \033[0;35m[\033[0;36m3\033[0;35m] \033[0;34m➮\033[0;33m EDITAR CONFIGURACION SERVIDOR $(msg -ama "(MEDIANTE NANO)")" 
		echo -e " \033[0;35m[\033[0;36m4\033[0;35m] \033[0;34m➮\033[0;33m CAMBIAR HOST DE OPENVPN" 
		echo -e " \033[0;35m[\033[0;36m5\033[0;35m] \033[0;34m➮\033[0;33m $(msg -verm2 "DESINSTALAR OPENVPN")"
		msg -bar
		while [[ $xption != @([0-5]) ]]; do
			echo -ne "\033[1;33m Opcion : " && read xption
			tput cuu1 && tput dl1
		done
		case $xption in 
			5)
				clear
				msg -bar
				echo -ne "\033[1;97m"
				read -p "QUIERES DESINTALAR OPENVPN? [Y/N]: " -e REMOVE
				msg -bar
				if [[ "$REMOVE" = 'y' || "$REMOVE" = 'Y' ]]; then
					PORT=$(grep '^port ' /etc/openvpn/server.conf | cut -d " " -f 2)
					PROTOCOL=$(grep '^proto ' /etc/openvpn/server.conf | cut -d " " -f 2)
					if pgrep firewalld; then
						IP=$(firewall-cmd --direct --get-rules ipv4 nat POSTROUTING | grep '\-s 10.8.0.0/24 '"'"'!'"'"' -d 10.8.0.0/24 -j SNAT --to ' | cut -d " " -f 10)
						# 
						firewall-cmd --zone=public --remove-port=$PORT/$PROTOCOL
						firewall-cmd --zone=trusted --remove-source=10.8.0.0/24
						firewall-cmd --permanent --zone=public --remove-port=$PORT/$PROTOCOL
						firewall-cmd --permanent --zone=trusted --remove-source=10.8.0.0/24
						firewall-cmd --direct --remove-rule ipv4 nat POSTROUTING 0 -s 10.8.0.0/24 ! -d 10.8.0.0/24 -j SNAT --to $IP
						firewall-cmd --permanent --direct --remove-rule ipv4 nat POSTROUTING 0 -s 10.8.0.0/24 ! -d 10.8.0.0/24 -j SNAT --to $IP
					else
						IP=$(grep 'iptables -t nat -A POSTROUTING -s 10.8.0.0/24 ! -d 10.8.0.0/24 -j SNAT --to ' $RCLOCAL | cut -d " " -f 14)
						iptables -t nat -D POSTROUTING -s 10.8.0.0/24 ! -d 10.8.0.0/24 -j SNAT --to $IP
						sed -i '/iptables -t nat -A POSTROUTING -s 10.8.0.0\/24 ! -d 10.8.0.0\/24 -j SNAT --to /d' $RCLOCAL
						if iptables -L -n | grep -qE '^ACCEPT'; then
							iptables -D INPUT -p $PROTOCOL --dport $PORT -j ACCEPT
							iptables -D FORWARD -s 10.8.0.0/24 -j ACCEPT
							iptables -D FORWARD -m state --state RELATED,ESTABLISHED -j ACCEPT
							sed -i "/iptables -I INPUT -p $PROTOCOL --dport $PORT -j ACCEPT/d" $RCLOCAL
							sed -i "/iptables -I FORWARD -s 10.8.0.0\/24 -j ACCEPT/d" $RCLOCAL
							sed -i "/iptables -I FORWARD -m state --state RELATED,ESTABLISHED -j ACCEPT/d" $RCLOCAL
						fi
					fi
					if sestatus 2>/dev/null | grep "Current mode" | grep -q "enforcing" && [[ "$PORT" != '1194' ]]; then
						semanage port -d -t openvpn_port_t -p $PROTOCOL $PORT
					fi
					if [[ "$OS" = 'debian' ]]; then
						apt-get remove --purge -y openvpn
					else
						yum remove openvpn -y
					fi
					rm -rf /etc/openvpn
					rm -f /etc/sysctl.d/30-openvpn-forward.conf
					clear
					msg -bar
					print_center -verd "OpenVPN removido!"
					enter
				else
					clear
					msg -bar
					print_center -verm2 "Desinstalacion abortada!"
					enter
				fi
				return 1;;
			2)
				nano /etc/openvpn/client-common.txt;;
			3)
				nano /etc/openvpn/server.conf;;
			4)
				edit_ovpn_host;;
			1)
				[[ $(mportas|grep -w openvpn) ]] && {
					/etc/init.d/openvpn stop > /dev/null 2>&1
					killall openvpn &>/dev/null
					systemctl stop openvpn@server.service &>/dev/null
					service openvpn stop &>/dev/null
					#ps x |grep openvpn |grep -v grep|awk '{print $1}' | while read pid; do kill -9 $pid; done
				} || {
					cd /etc/openvpn
					screen -dmS ovpnscr openvpn --config "server.conf" > /dev/null 2>&1
					touch /etc/openvpn/openvpn-status.log &
					cd $HOME
				}
				print_center   "Procedimiento con Exito"
				enter;;
			0)
				return 1;;
		esac
		return 0
	}
	[[ -e /etc/squid/squid.conf ]] && instala_ovpn && return 0
	[[ -e /etc/squid3/squid.conf ]] && instala_ovpn && return 0
	instala_ovpn || return 1
}

while [[ ! $rec = 1 ]]; do
	fun_openvpn
	rec="$?"
	unset xption
done
exit
}

function clash_beta(){
install_ini () {
add-apt-repository universe
apt update -y; apt upgrade -y
clear
msg -bar
echo -e "\033[92m        -- INSTALANDO PAQUETES NECESARIOS -- "
msg -bar
#bc
[[ $(dpkg --get-selections|grep -w "golang-go"|head -1) ]] || apt-get install golang-go -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "golang-go"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "golang-go"|head -1) ]] && ESTATUS=`echo -e "\033[92mINSTALADO"` &>/dev/null
echo -e "\033[97m  # apt-get install golang-go............ $ESTATUS "
#jq
[[ $(dpkg --get-selections|grep -w "jq"|head -1) ]] || apt-get install jq -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "jq"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "jq"|head -1) ]] && ESTATUS=`echo -e "\033[92mINSTALADO"` &>/dev/null
echo -e "\033[97m  # apt-get install jq................... $ESTATUS "
#curl
[[ $(dpkg --get-selections|grep -w "curl"|head -1) ]] || apt-get install curl -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "curl"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "curl"|head -1) ]] && ESTATUS=`echo -e "\033[92mINSTALADO"` &>/dev/null
echo -e "\033[97m  # apt-get install curl................. $ESTATUS "
#npm
[[ $(dpkg --get-selections|grep -w "npm"|head -1) ]] || apt-get install npm -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "npm"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "npm"|head -1) ]] && ESTATUS=`echo -e "\033[92mINSTALADO"` &>/dev/null
echo -e "\033[97m  # apt-get install npm.................. $ESTATUS "
#nodejs
[[ $(dpkg --get-selections|grep -w "nodejs"|head -1) ]] || apt-get install nodejs -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "nodejs"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "nodejs"|head -1) ]] && ESTATUS=`echo -e "\033[92mINSTALADO"` &>/dev/null
echo -e "\033[97m  # apt-get install nodejs............... $ESTATUS "
#socat
[[ $(dpkg --get-selections|grep -w "socat"|head -1) ]] || apt-get install socat -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "socat"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "socat"|head -1) ]] && ESTATUS=`echo -e "\033[92mINSTALADO"` &>/dev/null
echo -e "\033[97m  # apt-get install socat................ $ESTATUS "
#netcat
[[ $(dpkg --get-selections|grep -w "netcat"|head -1) ]] || apt-get install netcat -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "netcat"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "netcat"|head -1) ]] && ESTATUS=`echo -e "\033[92mINSTALADO"` &>/dev/null
echo -e "\033[97m  # apt-get install netcat............... $ESTATUS "
#net-tools
[[ $(dpkg --get-selections|grep -w "net-tools"|head -1) ]] || apt-get net-tools -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "net-tools"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "net-tools"|head -1) ]] && ESTATUS=`echo -e "\033[92mINSTALADO"` &>/dev/null
echo -e "\033[97m  # apt-get install net-tools............ $ESTATUS "
#figlet
[[ $(dpkg --get-selections|grep -w "figlet"|head -1) ]] || apt-get install figlet -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "figlet"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "figlet"|head -1) ]] && ESTATUS=`echo -e "\033[92mINSTALADO"` &>/dev/null
echo -e "\033[97m  # apt-get install figlet............... $ESTATUS "
msg -bar
echo -e "\033[92m La instalacion de paquetes necesarios a finalizado"
msg -bar
echo -e "\033[97m Si la instalacion de paquetes tiene fallas"
echo -ne "\033[97m Puede intentar de nuevo [s/n]: "
read inst
[[ $inst = @(s|S|y|Y) ]] && install_ini
echo -ne "\033[97m Deseas agregar Menu Clash Rapido [s/n]: "
read insta
[[ $insta = @(s|S|y|Y) ]] && enttrada
}


fun_insta(){
fun_ip
install_ini
msg -bar
killall clash 1> /dev/null 2> /dev/null
echo -e " ➣ Creando Directorios y Archivos"
msg -bar 
[[ -d /root/.config ]] && rm -rf /root/.config/* || mkdir /root/.config 
mkdir /root/.config/clash 1> /dev/null 2> /dev/null
last_version=$(curl -Ls "https://api.github.com/repos/emirjorge/clash-core/releases/latest" | grep '"tag_name":' | sed -E 's/.*"([^"]+)".*/\1/')
arch=$(arch)
if [[ $arch == "x86_64" || $arch == "x64" || $arch == "amd64" ]]; then
  arch="amd64"
elif [[ $arch == "aarch64" || $arch == "arm64" ]]; then
  arch="arm64"
else
  arch="amd64"
fi
wget -N --no-check-certificate -O /root/.config/clash/clash.gz https://github.com/emirjorge/clash-core/releases/download/${last_version}/clash-linux-${arch}-${last_version}.gz
gzip -d /root/.config/clash/clash.gz
chmod +x /root/.config/clash/clash
echo -e " ➣ Clonando Repositorio Original Dreamacro "
go get -u -v github.com/emirjorge/clash-core
clear
}



numero='^[0-9]+$'
hora=$(printf '%(%H:%M:%S)T') 
fecha=$(printf '%(%D)T')
[[ ! -d /bin/ejecutar/clashFiles ]] && mkdir /bin/ejecutar/clashFiles
clashFiles='/bin/ejecutar/clashFiles/'

INITClash(){
msg -bar
conFIN
read -p "Ingrese Nombre del Poster WEB de la configuracion: " cocolon
[[ -e /root/.config/clash/config.yaml ]] && sed -i "s%_dAtE%${fecha}%g" /root/.config/clash/config.yaml
[[ -e /root/.config/clash/config.yaml ]] && sed -i "s/_h0rA/${hora}/g" /root/.config/clash/config.yaml
cp /root/.config/clash/config.yaml /var/www/html/$cocolon.yaml && chmod +x /var/www/html/$cocolon.yaml
service apache2 restart
echo -e "[\033[1;31m-\033[1;33m]\033[1;31m \033[1;33m"
echo -e "\033[1;33mClash Server Instalado"
echo -e "-------------------------------------------------------"
echo -e "		\033[4;31mNOTA importante\033[0m"
echo -e "Recuerda Descargar el Fichero, o cargarlo como URL!!"
echo -e "-------------------------------------------------------"
echo -e " \033[0;31mSi Usas Clash For Android, Ultima Version  "
echo -e "  Para luego usar el Link del Fichero, y puedas ."
echo -e " Descargarlo desde cualquier sitio con acceso WEB"
echo -e "        Link Clash Valido por 30 minutos "
echo -e "    Link : \033[1;42m  http://$IP:${portFTP}/$cocolon.yaml\033[0m"
echo -e "-------------------------------------------------------"
#read -p "PRESIONA ENTER PARA CARGAR ONLINE"
echo -e "\033[1;32mRuta de Configuracion: /root/.config/clash/config.yaml"
echo -e "\033[1;31mPRESIONE ENTER PARA CONTINUAR\033[0m"
scr=$(echo $(($RANDOM*3))|head -c 3)
unset yesno
echo -e " ENLACE VALIDO POR 30 MINUTOS? " 
while [[ ${yesno} != @(s|S|y|Y|n|N) ]]; do
read -p "[S/N]: " yesno
tput cuu1 && tput dl1
done
[[ ${yesno} = @(s|S|y|Y) ]] &&  { 
killall clash > /dev/null &1>&2
screen -dmS clashse_$cocolon /root/.config/clash/clash
echo '#!/bin/bash -e' > /root/.config/clash/$cocolon.sh
echo "sleep 1800s" >> /root/.config/clash/$cocolon.sh && echo -e " ACTIVO POR 30 MINUTOS "  || echo " Validacion Incorrecta "
echo "mv /var/www/html/$cocolon.yaml ${clashFiles}$cocolon.yaml" >> /root/.config/clash/$cocolon.sh
echo 'echo "Fichero removido a ${clashFiles}$cocolon.yaml"' >> /root/.config/clash/$cocolon.sh
echo "service apache2 restart" >> /root/.config/clash/$cocolon.sh
echo "rm -f /root/.config/clash/$cocolon.sh" >> /root/.config/clash/$cocolon.sh
echo 'exit' >> /root/.config/clash/$cocolon.sh && screen -dmS clash${scr} bash /root/.config/clash/$cocolon.sh
} 
echo -e "Proceso Finalizado"

}

configINIT_rule () {
mode=$1
[[ -z ${mode} ]] && exit
unset tropass
echo '#SCRIPT OFICIAL DrowKid|Plus
# Formato Creado por @drowkid01 | +593987072611 Whatsapp Personal
# Creado el _dAtE - _h0rA
port: 8080
socks-port: 7891
redir-port: 7892
allow-lan: true
bind-address: "*"
mode: rule
log-level: info
external-controller: "0.0.0.0:9090"
secret: ""

dns:
  enable: true
  listen: :53
  enhanced-mode: fake-ip
  nameserver:
    - 114.114.114.114
    - 223.5.5.5
    - 8.8.8.8
    - 45.71.185.100
    - 204.199.156.138
    - 1.1.1.1
  fallback: []
  fake-ip-filter:
    - +.stun.*.*
    - +.stun.*.*.*
    - +.stun.*.*.*.*
    - +.stun.*.*.*.*.*
    - "*.n.n.srv.nintendo.net"
    - +.stun.playstation.net
    - xbox.*.*.microsoft.com
    - "*.*.xboxlive.com"
    - "*.msftncsi.com"
    - "*.msftconnecttest.com"
    - WORKGROUP    
tun:
  enable: true
  stack: gvisor
  auto-route: true
  auto-detect-interface: true
  dns-hijack:
    - any:53

# Clash for Windows
cfw-bypass:
  - qq.com
  - music.163.com
  - "*.music.126.net"
  - localhost
  - 127.*
  - 10.*
  - 172.16.*
  - 172.17.*
  - 172.18.*
  - 172.19.*
  - 172.20.*
  - 172.21.*
  - 172.22.*
  - 172.23.*
  - 172.24.*
  - 172.25.*
  - 172.26.*
  - 172.27.*
  - 172.28.*
  - 172.29.*
  - 172.30.*
  - 172.31.*
  - 192.168.*
  - <local>
cfw-latency-timeout: 5000
    
proxy-groups:
- name: "ChuKK-SCRIPT"
  type: select
  proxies:    ' > /root/.config/clash/config.yaml
#sed -i "s/+/'/g"  /root/.config/clash/config.yaml
foc=1
[[ -e /usr/local/etc/trojan/config.json ]] && ConfTrojINI
unset yesno
foc=1
[[ -e /etc/v2ray/config.json ]] && ConfV2RINI
unset yesno
foc=1								
[[ -e /etc/xray/config.json ]] && ConfXRINI
}

configINIT_global () {
mode=$1
[[ -z ${mode} ]] && exit
unset tropass
echo '#SCRIPT OFICIAL NO CREADO POR CHUMO [SELOROBÓ]
port: 8080
socks-port: 7891
redir-port: 7892
allow-lan: true
bind-address: "*"
mode: global
log-level: info
external-controller: "0.0.0.0:9090"
secret: ""
dns:
  enable: true
  listen: :53
  enhanced-mode: fake-ip
  nameserver:
    - 114.114.114.114
    - 223.5.5.5
    - 8.8.8.8
    - 45.71.185.100
    - 204.199.156.138
    - 1.1.1.1
  fallback: []
  fake-ip-filter:
    - +.stun.*.*
    - +.stun.*.*.*
    - +.stun.*.*.*.*
    - +.stun.*.*.*.*.*
    - "*.n.n.srv.nintendo.net"
    - +.stun.playstation.net
    - xbox.*.*.microsoft.com
    - "*.*.xboxlive.com"
    - "*.msftncsi.com"
    - "*.msftconnecttest.com"
    - WORKGROUP    
tun:
  enable: true
  stack: gvisor
  auto-route: true
  auto-detect-interface: true
  dns-hijack:
    - any:53

# Clash for Windows
cfw-bypass:
  - qq.com
  - music.163.com
  - "*.music.126.net"
  - localhost
  - 127.*
  - 10.*
  - 172.16.*
  - 172.17.*
  - 172.18.*
  - 172.19.*
  - 172.20.*
  - 172.21.*
  - 172.22.*
  - 172.23.*
  - 172.24.*
  - 172.25.*
  - 172.26.*
  - 172.27.*
  - 172.28.*
  - 172.29.*
  - 172.30.*
  - 172.31.*
  - 192.168.*
  - <local>
cfw-latency-timeout: 5000   
 ' > /root/.config/clash/config.yaml
#sed -i "s/+/'/g"  /root/.config/clash/config.yaml
foc=1
[[ -e /usr/local/etc/trojan/config.json ]] && ConfTrojINI
unset yesno
foc=1
[[ -e /etc/v2ray/config.json ]] && ConfV2RINI
unset yesno
foc=1								
[[ -e /etc/xray/config.json ]] && ConfXRINI
}

proxyTRO() {
fun_ip
[[ $mode = 1 ]] && echo -e "    - $1" >> /root/.config/clash/config.yaml
proTRO+="- name: $1\n  type: trojan\n  server: ${IP}\n  port: ${troport}\n  password: "$2"\n  udp: true\n  sni: $3\n  alpn:\n  - h2\n  - http/1.1\n  skip-cert-verify: true\n\n" 
  }

ConfTrojINI() {
echo -e " DESEAS AÑADIR TU ${foc} CONFIG TROJAN " 
while [[ ${yesno} != @(s|S|y|Y|n|N) ]]; do
read -p " [S/N]: " yesno

tput cuu1 && tput dl1
done
[[ ${yesno} = @(s|S|y|Y) ]] &&  { 
unset yesno
foc=$(($foc + 1))
echo -ne "\033[1;33m ➣ PERFIL TROJAN CLASH "
read -p ": " nameperfil
msg -bar
[[ -z ${UUID} ]] && view_usert || { 
echo -e " USER ${Usr} : ${UUID}"
msg -bar
}
echo -ne "\033[1;33m ➣ SNI o HOST "
read -p ": " trosni
msg -bar
proxyTRO ${nameperfil} ${UUID} ${trosni}
ConfTrojINI
								}
}

proxyV2R() {
#proxyV2R ${nameperfil} ${trosni} ${uid} ${aluuiid} ${net} ${parche} ${v2port}
fun_ip
[[ $mode = 1 ]] && echo -e "    - $1" >> /root/.config/clash/config.yaml
proV2R+="- name: $1\n  type: vmess\n  server: ${IP}\n  port: $7\n  uuid: $3\n  alterId: $4\n  cipher: auto\n  udp: true\n  tls: true\n  skip-cert-verify: true\n  servername: $2\n  network: $5\n  ws-opts:  \n       path: $6\n       headers:\n         Host: $2\n  \n\n" 
  }
  
proxyV2Rgprc() {
#config=/usr/local/x-ui/bin/config.json
#cat $config | jq .inbounds[].settings.clients | grep id
#proxyV2R ${nameperfil} ${trosni} ${uid} ${aluuiid} ${net} ${parche} ${v2port}
fun_ip
[[ $mode = 1 ]] && echo -e "    - $1" >> /root/.config/clash/config.yaml
proV2R+="
- name: $1\n  server: ${IP}\n  port: $7\n  type: vmess\n  uuid: $3\n  alterId: $4\n  cipher: auto\n  tls: true\n  skip-cert-verify: true\n  network: grpc\n  servername: $2\n  grpc-opts:\n    grpc-mode: gun\n    grpc-service-name: $6\n  udp: true \n\n"
  }
proxyXR() {
#proxyV2R ${nameperfil} ${trosni} ${uid} ${aluuiid} ${net} ${parche} ${v2port}
fun_ip
[[ $mode = 1 ]] && echo -e "    - $1" >> /root/.config/clash/config.yaml
proXR+="- name: $1\n  type: vmess\n  server: ${IP}\n  port: $7\n  uuid: $3\n  alterId: $4\n  cipher: auto\n  udp: true\n  tls: true\n  skip-cert-verify: true\n  servername: $2\n  network: $5\n  ws-opts:  \n       path: $6\n       headers:\n         Host: $2\n  \n\n" 
  }
  
proxyXRgprc() {
#config=/usr/local/x-ui/bin/config.json
#cat $config | jq .inbounds[].settings.clients | grep id
#proxyV2R ${nameperfil} ${trosni} ${uid} ${aluuiid} ${net} ${parche} ${v2port}
fun_ip
[[ $mode = 1 ]] && echo -e "    - $1" >> /root/.config/clash/config.yaml
proXR+="
- name: $1\n  server: ${IP}\n  port: $7\n  type: vmess\n  uuid: $3\n  alterId: $4\n  cipher: auto\n  tls: true\n  skip-cert-verify: true\n  network: grpc\n  servername: $2\n  grpc-opts:\n    grpc-mode: gun\n    grpc-service-name: $6\n  udp: true \n\n"
  }

ConfV2RINI() {
echo -e " DESEAS AÑADIR TU ${foc} CONFIG V2RAY " 
while [[ ${yesno} != @(s|S|y|Y|n|N) ]]; do
read -p "[S/N]: " yesno
tput cuu1 && tput dl1
done
[[ ${yesno} = @(s|S|y|Y) ]] &&  { 
unset yesno
foc=$(($foc + 1))
echo -ne "\033[1;33m ➣ PERFIL V2RAY CLASH "
read -p ": " nameperfil
msg -bar
[[ -z ${uid} ]] && view_user || { 
echo -e " USER ${ps}"
msg -bar
}
echo -ne "\033[1;33m ➣ SNI o HOST "
read -p ": " trosni
msg -bar

		ps=$(jq .inbounds[].settings.clients[$opcion].email $config) && [[ $ps = null ]] && ps="default"
		uid=$(jq .inbounds[].settings.clients[$opcion].id $config)
		aluuiid=$(jq .inbounds[].settings.clients[$opcion].alterId $config)
		add=$(jq '.inbounds[].domain' $config) && [[ $add = null ]] && add=$(wget -qO- ipv4.icanhazip.com)
		host=$(jq '.inbounds[].streamSettings.wsSettings.headers.Host' $config) && [[ $host = null ]] && host=''
		net=$(jq '.inbounds[].streamSettings.network' $config)
		parche=$(jq -r .inbounds[].streamSettings.wsSettings.path $config) && [[ $path = null ]] && parche='' 
		v2port=$(jq '.inbounds[].port' $config)
		tls=$(jq '.inbounds[].streamSettings.security' $config)
		[[ $net = '"grpc"' ]] && path=$(jq '.inbounds[].streamSettings.grpcSettings.serviceName'  $config) || path=$(jq '.inbounds[].streamSettings.wsSettings.path' $config)
		addip=$(wget -qO- ifconfig.me)

[[ $net = '"grpc"' ]] && {
proxyV2Rgprc ${nameperfil} ${trosni} ${uid} ${aluuiid} ${net} ${path} ${v2port}
} || {
proxyV2R ${nameperfil} ${trosni} ${uid} ${aluuiid} ${net} ${parche} ${v2port}
}

ConfV2RINI
	} 
}

ConfXRINI() {
echo -e " DESEAS AÑADIR TU ${foc} CONFIG XRAY " 
while [[ ${yesno} != @(s|S|y|Y|n|N) ]]; do
read -p "[S/N]: " yesno
tput cuu1 && tput dl1
done
[[ ${yesno} = @(s|S|y|Y) ]] &&  { 
unset yesno
foc=$(($foc + 1))
echo -ne "\033[1;33m ➣ PERFIL XRAY CLASH "
read -p ": " nameperfilX
msg -bar
[[ -z ${uidX} ]] && _view_userXR || { 
echo -e " USER ${ps} XRAY"
msg -bar
}
echo -ne "\033[1;33m ➣ SNI o HOST "
read -p ": " trosniX
msg -bar
		psX=$(jq .inbounds[].settings.clients[$opcion].email $config) && [[ $ps = null ]] && ps="default"
		uidX=$(jq .inbounds[].settings.clients[$opcion].id $config)
		aluuiidX=$(jq .inbounds[].settings.clients[$opcion].alterId $config)
		addX=$(jq '.inbounds[].domain' $config) && [[ $add = null ]] && addX=$(wget -qO- ipv4.icanhazip.com)
		hostX=$(jq '.inbounds[].streamSettings.wsSettings.headers.Host' $config) && [[ $host = null ]] && host=''
		netX=$(jq '.inbounds[].streamSettings.network' $config)
		parcheX=$(jq -r .inbounds[].streamSettings.wsSettings.path $config) && [[ $pathX = null ]] && parcheX='' 
		v2portX=$(jq '.inbounds[].port' $config)
		tlsX=$(jq '.inbounds[].streamSettings.security' $config)
		[[ $netX = '"grpc"' ]] && pathX=$(jq '.inbounds[].streamSettings.grpcSettings.serviceName'  $config) || pathX=$(jq '.inbounds[].streamSettings.wsSettings.path' $config)
		addip=$(wget -qO- ifconfig.me)

[[ $netX = '"grpc"' ]] && {
proxyXRgprc ${nameperfilX} ${trosniX} ${uidX} ${aluuiidX} ${netX} ${pathX} ${v2portX}
} || {
proxyXR ${nameperfilX} ${trosniX} ${uidX} ${aluuiidX} ${netX} ${parcheX} ${v2portX}
}

ConfXRINI
							}
}

confRULE() {
[[ $mode = 1 ]] && echo -e '
  url: http://www.gstatic.com/generate_204
  interval: 300
 
###################################
# ChuKK-SCRIPT

- name: "【 ✵ 𝚂𝚎𝚛𝚟𝚎𝚛-𝙿𝚁𝙴𝙼𝙸𝚄𝙼 ✵ 】"
  type: select
  proxies: 
    - "ChuKK-SCRIPT"

Rule:
# Unbreak
# > Google
- DOMAIN-SUFFIX,googletraveladservices.com,ChuKK-SCRIPT
- DOMAIN,dl.google.com,ChuKK-SCRIPT
- DOMAIN,mtalk.google.com,ChuKK-SCRIPT

# Internet Service Providers ChuKK-SCRIPT 运营商劫持
- DOMAIN-SUFFIX,17gouwuba.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,186078.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,189zj.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,285680.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,3721zh.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,4336wang.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,51chumoping.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,51mld.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,51mypc.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,58mingri.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,58mingtian.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,5vl58stm.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,6d63d3.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,7gg.cc,ChuKK-SCRIPT
- DOMAIN-SUFFIX,91veg.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,9s6q.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,adsame.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,aiclk.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,akuai.top,ChuKK-SCRIPT
- DOMAIN-SUFFIX,atplay.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,baiwanchuangyi.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,beerto.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,beilamusi.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,benshiw.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,bianxianmao.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,bryonypie.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,cishantao.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,cszlks.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,cudaojia.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,dafapromo.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,daitdai.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,dsaeerf.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,dugesheying.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,dv8c1t.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,echatu.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,erdoscs.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,fan-yong.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,feih.com.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,fjlqqc.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,fkku194.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,freedrive.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,gclick.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,goufanli100.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,goupaoerdai.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,gouwubang.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,gzxnlk.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,haoshengtoys.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,hyunke.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,ichaosheng.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,ishop789.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,jdkic.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,jiubuhua.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,jsncke.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,junkucm.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,jwg365.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,kawo77.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,kualianyingxiao.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,kumihua.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,ltheanine.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,maipinshangmao.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,minisplat.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,mkitgfs.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,mlnbike.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,mobjump.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,nbkbgd.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,newapi.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,pinzhitmall.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,poppyta.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,qianchuanghr.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,qichexin.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,qinchugudao.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,quanliyouxi.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,qutaobi.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,ry51w.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,sg536.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,sifubo.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,sifuce.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,sifuda.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,sifufu.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,sifuge.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,sifugu.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,sifuhe.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,sifuhu.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,sifuji.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,sifuka.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,smgru.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,taoggou.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,tcxshop.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,tjqonline.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,topitme.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,tt3sm4.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,tuia.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,tuipenguin.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,tuitiger.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,websd8.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,wsgblw.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,wx16999.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,xchmai.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,xiaohuau.xyz,ChuKK-SCRIPT
- DOMAIN-SUFFIX,ygyzx.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,yinmong.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,yitaopt.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,yjqiqi.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,yukhj.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,zhaozecheng.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,zhenxinet.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,zlne800.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,zunmi.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,zzd6.com,ChuKK-SCRIPT
- IP-CIDR,39.107.15.115/32,ChuKK-SCRIPT,no-resolve
- IP-CIDR,47.89.59.182/32,ChuKK-SCRIPT,no-resolve
- IP-CIDR,103.49.209.27/32,ChuKK-SCRIPT,no-resolve
- IP-CIDR,123.56.152.96/32,ChuKK-SCRIPT,no-resolve
# > ChinaTelecom
- IP-CIDR,61.160.200.223/32,ChuKK-SCRIPT,no-resolve
- IP-CIDR,61.160.200.242/32,ChuKK-SCRIPT,no-resolve
- IP-CIDR,61.160.200.252/32,ChuKK-SCRIPT,no-resolve
- IP-CIDR,61.174.50.214/32,ChuKK-SCRIPT,no-resolve
- IP-CIDR,111.175.220.163/32,ChuKK-SCRIPT,no-resolve
- IP-CIDR,111.175.220.164/32,ChuKK-SCRIPT,no-resolve
- IP-CIDR,122.229.8.47/32,ChuKK-SCRIPT,no-resolve
- IP-CIDR,122.229.29.89/32,ChuKK-SCRIPT,no-resolve
- IP-CIDR,124.232.160.178/32,ChuKK-SCRIPT,no-resolve
- IP-CIDR,175.6.223.15/32,ChuKK-SCRIPT,no-resolve
- IP-CIDR,183.59.53.237/32,ChuKK-SCRIPT,no-resolve
- IP-CIDR,218.93.127.37/32,ChuKK-SCRIPT,no-resolve
- IP-CIDR,221.228.17.152/32,ChuKK-SCRIPT,no-resolve
- IP-CIDR,221.231.6.79/32,ChuKK-SCRIPT,no-resolve
- IP-CIDR,222.186.61.91/32,ChuKK-SCRIPT,no-resolve
- IP-CIDR,222.186.61.95/32,ChuKK-SCRIPT,no-resolve
- IP-CIDR,222.186.61.96/32,ChuKK-SCRIPT,no-resolve
- IP-CIDR,222.186.61.97/32,ChuKK-SCRIPT,no-resolve
# > ChinaUnicom
- IP-CIDR,106.75.231.48/32,ChuKK-SCRIPT,no-resolve
- IP-CIDR,119.4.249.166/32,ChuKK-SCRIPT,no-resolve
- IP-CIDR,220.196.52.141/32,ChuKK-SCRIPT,no-resolve
- IP-CIDR,221.6.4.148/32,ChuKK-SCRIPT,no-resolve
# > ChinaMobile
- IP-CIDR,114.247.28.96/32,ChuKK-SCRIPT,no-resolve
- IP-CIDR,221.179.131.72/32,ChuKK-SCRIPT,no-resolve
- IP-CIDR,221.179.140.145/32,ChuKK-SCRIPT,no-resolve
# > Dr.Peng
# - IP-CIDR,10.72.25.0/24,ChuKK-SCRIPT,no-resolve
- IP-CIDR,115.182.16.79/32,ChuKK-SCRIPT,no-resolve
- IP-CIDR,118.144.88.126/32,ChuKK-SCRIPT,no-resolve
- IP-CIDR,118.144.88.215/32,ChuKK-SCRIPT,no-resolve
- IP-CIDR,118.144.88.216/32,ChuKK-SCRIPT,no-resolve
- IP-CIDR,120.76.189.132/32,ChuKK-SCRIPT,no-resolve
- IP-CIDR,124.14.21.147/32,ChuKK-SCRIPT,no-resolve
- IP-CIDR,124.14.21.151/32,ChuKK-SCRIPT,no-resolve
- IP-CIDR,180.166.52.24/32,ChuKK-SCRIPT,no-resolve
- IP-CIDR,211.161.101.106/32,ChuKK-SCRIPT,no-resolve
- IP-CIDR,220.115.251.25/32,ChuKK-SCRIPT,no-resolve
- IP-CIDR,222.73.156.235/32,ChuKK-SCRIPT,no-resolve

# Malware 恶意网站
# > 快压
# https://zhuanlan.zhihu.com/p/39534279
- DOMAIN-SUFFIX,kuaizip.com,ChuKK-SCRIPT
# > MacKeeper
# https://www.lizhi.io/blog/40002904
- DOMAIN-SUFFIX,mackeeper.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,zryydi.com,ChuKK-SCRIPT
# > Adobe Flash China Special Edition
# https://www.zhihu.com/question/281163698/answer/441388130
- DOMAIN-SUFFIX,flash.cn,ChuKK-SCRIPT
- DOMAIN,geo2.adobe.com,ChuKK-SCRIPT
# > C&J Marketing 思杰马克丁软件
# https://www.zhihu.com/question/46746200
- DOMAIN-SUFFIX,4009997658.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,abbyychina.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,bartender.cc,ChuKK-SCRIPT
- DOMAIN-SUFFIX,betterzip.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,betterzipcn.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,beyondcompare.cc,ChuKK-SCRIPT
- DOMAIN-SUFFIX,bingdianhuanyuan.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,chemdraw.com.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,cjmakeding.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,cjmkt.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,codesoftchina.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,coreldrawchina.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,crossoverchina.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,dongmansoft.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,earmasterchina.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,easyrecoverychina.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,ediuschina.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,flstudiochina.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,formysql.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,guitarpro.cc,ChuKK-SCRIPT
- DOMAIN-SUFFIX,huishenghuiying.com.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,hypersnap.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,iconworkshop.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,imindmap.cc,ChuKK-SCRIPT
- DOMAIN-SUFFIX,jihehuaban.com.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,keyshot.cc,ChuKK-SCRIPT
- DOMAIN-SUFFIX,kingdeecn.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,logoshejishi.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,luping.net.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,mairuan.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,mairuan.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,mairuan.com.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,mairuan.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,mairuanwang.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,makeding.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,mathtype.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,mindmanager.cc,ChuKK-SCRIPT
- DOMAIN-SUFFIX,mindmanager.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,mindmapper.cc,ChuKK-SCRIPT
- DOMAIN-SUFFIX,mycleanmymac.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,nicelabel.cc,ChuKK-SCRIPT
- DOMAIN-SUFFIX,ntfsformac.cc,ChuKK-SCRIPT
- DOMAIN-SUFFIX,ntfsformac.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,overturechina.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,passwordrecovery.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,pdfexpert.cc,ChuKK-SCRIPT
- DOMAIN-SUFFIX,photozoomchina.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,shankejingling.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,ultraiso.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,vegaschina.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,xmindchina.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,xshellcn.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,yihuifu.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,yuanchengxiezuo.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,zbrushcn.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,zhzzx.com,ChuKK-SCRIPT

# Global Area Network
# (ChuKK-SCRIPT)
# (Music)
# > Deezer
# USER-AGENT,Deezer*,ChuKK-SCRIPT
- DOMAIN-SUFFIX,deezer.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,dzcdn.net,ChuKK-SCRIPT
# > KKBOX
- DOMAIN-SUFFIX,kkbox.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,kkbox.com.tw,ChuKK-SCRIPT
- DOMAIN-SUFFIX,kfs.io,ChuKK-SCRIPT
# > JOOX
# USER-AGENT,WeMusic*,ChuKK-SCRIPT
# USER-AGENT,JOOX*,ChuKK-SCRIPT
- DOMAIN-SUFFIX,joox.com,ChuKK-SCRIPT
# > Pandora
# USER-AGENT,Pandora*,ChuKK-SCRIPT
- DOMAIN-SUFFIX,pandora.com,ChuKK-SCRIPT
# > SoundCloud
# USER-AGENT,SoundCloud*,ChuKK-SCRIPT
- DOMAIN-SUFFIX,p-cdn.us,ChuKK-SCRIPT
- DOMAIN-SUFFIX,sndcdn.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,soundcloud.com,ChuKK-SCRIPT
# > Spotify
# USER-AGENT,Spotify*,ChuKK-SCRIPT
- DOMAIN-SUFFIX,pscdn.co,ChuKK-SCRIPT
- DOMAIN-SUFFIX,scdn.co,ChuKK-SCRIPT
- DOMAIN-SUFFIX,spotify.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,spoti.fi,ChuKK-SCRIPT
- DOMAIN-KEYWORD,spotify.com,ChuKK-SCRIPT
- DOMAIN-KEYWORD,-spotify-com,ChuKK-SCRIPT
# > TIDAL
# USER-AGENT,TIDAL*,ChuKK-SCRIPT
- DOMAIN-SUFFIX,tidal.com,ChuKK-SCRIPT
# > YouTubeMusic
# USER-AGENT,com.google.ios.youtubemusic*,ChuKK-SCRIPT
# USER-AGENT,YouTubeMusic*,ChuKK-SCRIPT
# (Video)
# > All4
# USER-AGENT,All4*,ChuKK-SCRIPT
- DOMAIN-SUFFIX,c4assets.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,channel4.com,ChuKK-SCRIPT
# > AbemaTV
# USER-AGENT,AbemaTV*,ChuKK-SCRIPT
- DOMAIN-SUFFIX,abema.io,ChuKK-SCRIPT
- DOMAIN-SUFFIX,ameba.jp,ChuKK-SCRIPT
- DOMAIN-SUFFIX,abema.tv,ChuKK-SCRIPT
- DOMAIN-SUFFIX,hayabusa.io,ChuKK-SCRIPT
- DOMAIN,abematv.akamaized.net,ChuKK-SCRIPT
- DOMAIN,ds-linear-abematv.akamaized.net,ChuKK-SCRIPT
- DOMAIN,ds-vod-abematv.akamaized.net,ChuKK-SCRIPT
- DOMAIN,linear-abematv.akamaized.net,ChuKK-SCRIPT
# > Amazon Prime Video
# USER-AGENT,InstantVideo.US*,ChuKK-SCRIPT
# USER-AGENT,Prime%20Video*,ChuKK-SCRIPT
- DOMAIN-SUFFIX,aiv-cdn.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,aiv-delivery.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,amazonvideo.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,primevideo.com,ChuKK-SCRIPT
- DOMAIN,avodmp4s3ww-a.akamaihd.net,ChuKK-SCRIPT
- DOMAIN,d25xi40x97liuc.cloudfront.net,ChuKK-SCRIPT
- DOMAIN,dmqdd6hw24ucf.cloudfront.net,ChuKK-SCRIPT
- DOMAIN,d22qjgkvxw22r6.cloudfront.net,ChuKK-SCRIPT
- DOMAIN,d1v5ir2lpwr8os.cloudfront.net,ChuKK-SCRIPT
- DOMAIN-KEYWORD,avoddashs,ChuKK-SCRIPT
# > Bahamut
# USER-AGENT,Anime*,ChuKK-SCRIPT
- DOMAIN-SUFFIX,bahamut.com.tw,ChuKK-SCRIPT
- DOMAIN-SUFFIX,gamer.com.tw,ChuKK-SCRIPT
- DOMAIN,gamer-cds.cdn.hinet.net,ChuKK-SCRIPT
- DOMAIN,gamer2-cds.cdn.hinet.net,ChuKK-SCRIPT
# > BBC iPlayer
# USER-AGENT,BBCiPlayer*,ChuKK-SCRIPT
- DOMAIN-SUFFIX,bbc.co.uk,ChuKK-SCRIPT
- DOMAIN-SUFFIX,bbci.co.uk,ChuKK-SCRIPT
- DOMAIN-KEYWORD,bbcfmt,ChuKK-SCRIPT
- DOMAIN-KEYWORD,uk-live,ChuKK-SCRIPT
# > DAZN
# USER-AGENT,DAZN*,ChuKK-SCRIPT
- DOMAIN-SUFFIX,dazn.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,dazn-api.com,ChuKK-SCRIPT
- DOMAIN,d151l6v8er5bdm.cloudfront.net,ChuKK-SCRIPT
- DOMAIN-KEYWORD,voddazn,ChuKK-SCRIPT
# > Disney+
# USER-AGENT,Disney+*,ChuKK-SCRIPT
- DOMAIN-SUFFIX,bamgrid.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,disney-plus.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,disneyplus.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,dssott.com,ChuKK-SCRIPT
- DOMAIN,cdn.registerdisney.go.com,ChuKK-SCRIPT
# > encoreTVB
# USER-AGENT,encoreTVB*,ChuKK-SCRIPT
- DOMAIN-SUFFIX,encoretvb.com,ChuKK-SCRIPT
- DOMAIN,edge.api.brightcove.com,ChuKK-SCRIPT
- DOMAIN,bcbolt446c5271-a.akamaihd.net,ChuKK-SCRIPT
# > FOX NOW
# USER-AGENT,FOX%20NOW*,ChuKK-SCRIPT
- DOMAIN-SUFFIX,fox.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,foxdcg.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,theplatform.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,uplynk.com,ChuKK-SCRIPT
# > HBO NOW
# USER-AGENT,HBO%20NOW*,ChuKK-SCRIPT
- DOMAIN-SUFFIX,hbo.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,hbogo.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,hbonow.com,ChuKK-SCRIPT
# > HBO GO HKG
# USER-AGENT,HBO%20GO%20PROD%20HKG*,ChuKK-SCRIPT
- DOMAIN-SUFFIX,hbogoasia.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,hbogoasia.hk,ChuKK-SCRIPT
- DOMAIN,bcbolthboa-a.akamaihd.net,ChuKK-SCRIPT
- DOMAIN,players.brightcove.net,ChuKK-SCRIPT
- DOMAIN,s3-ap-southeast-1.amazonaws.com,ChuKK-SCRIPT
- DOMAIN,dai3fd1oh325y.cloudfront.net,ChuKK-SCRIPT
- DOMAIN,44wilhpljf.execute-api.ap-southeast-1.amazonaws.com,ChuKK-SCRIPT
- DOMAIN,hboasia1-i.akamaihd.net,ChuKK-SCRIPT
- DOMAIN,hboasia2-i.akamaihd.net,ChuKK-SCRIPT
- DOMAIN,hboasia3-i.akamaihd.net,ChuKK-SCRIPT
- DOMAIN,hboasia4-i.akamaihd.net,ChuKK-SCRIPT
- DOMAIN,hboasia5-i.akamaihd.net,ChuKK-SCRIPT
- DOMAIN,cf-images.ap-southeast-1.prod.boltdns.net,ChuKK-SCRIPT
# > 华文电视
# USER-AGENT,HWTVMobile*,ChuKK-SCRIPT
- DOMAIN-SUFFIX,5itv.tv,ChuKK-SCRIPT
- DOMAIN-SUFFIX,ocnttv.com,ChuKK-SCRIPT
# > Hulu
- DOMAIN-SUFFIX,hulu.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,huluim.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,hulustream.com,ChuKK-SCRIPT
# > Hulu(フールー)
- DOMAIN-SUFFIX,happyon.jp,ChuKK-SCRIPT
- DOMAIN-SUFFIX,hulu.jp,ChuKK-SCRIPT
# > ITV
# USER-AGENT,ITV_Player*,ChuKK-SCRIPT
- DOMAIN-SUFFIX,itv.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,itvstatic.com,ChuKK-SCRIPT
- DOMAIN,itvpnpmobile-a.akamaihd.net,ChuKK-SCRIPT
# > KKTV
# USER-AGENT,KKTV*,ChuKK-SCRIPT
# USER-AGENT,com.kktv.ios.kktv*,ChuKK-SCRIPT
- DOMAIN-SUFFIX,kktv.com.tw,ChuKK-SCRIPT
- DOMAIN-SUFFIX,kktv.me,ChuKK-SCRIPT
- DOMAIN,kktv-theater.kk.stream,ChuKK-SCRIPT
# > Line TV
# USER-AGENT,LINE%20TV*,ChuKK-SCRIPT
- DOMAIN-SUFFIX,linetv.tw,ChuKK-SCRIPT
- DOMAIN,d3c7rimkq79yfu.cloudfront.net,ChuKK-SCRIPT
# > LiTV
- DOMAIN-SUFFIX,litv.tv,ChuKK-SCRIPT
- DOMAIN,litvfreemobile-hichannel.cdn.hinet.net,ChuKK-SCRIPT
# > My5
# USER-AGENT,My5*,ChuKK-SCRIPT
- DOMAIN-SUFFIX,channel5.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,my5.tv,ChuKK-SCRIPT
- DOMAIN,d349g9zuie06uo.cloudfront.net,ChuKK-SCRIPT
# > myTV SUPER
# USER-AGENT,mytv*,ChuKK-SCRIPT
- DOMAIN-SUFFIX,mytvsuper.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,tvb.com,ChuKK-SCRIPT
# > Netflix
# USER-AGENT,Argo*,ChuKK-SCRIPT
- DOMAIN-SUFFIX,netflix.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,netflix.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,nflxext.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,nflximg.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,nflximg.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,nflxso.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,nflxvideo.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,netflixdnstest0.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,netflixdnstest1.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,netflixdnstest2.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,netflixdnstest3.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,netflixdnstest4.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,netflixdnstest5.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,netflixdnstest6.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,netflixdnstest7.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,netflixdnstest8.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,netflixdnstest9.com,ChuKK-SCRIPT
- IP-CIDR,23.246.0.0/18,ChuKK-SCRIPT,no-resolve
- IP-CIDR,37.77.184.0/21,ChuKK-SCRIPT,no-resolve
- IP-CIDR,45.57.0.0/17,ChuKK-SCRIPT,no-resolve
- IP-CIDR,64.120.128.0/17,ChuKK-SCRIPT,no-resolve
- IP-CIDR,66.197.128.0/17,ChuKK-SCRIPT,no-resolve
- IP-CIDR,108.175.32.0/20,ChuKK-SCRIPT,no-resolve
- IP-CIDR,192.173.64.0/18,ChuKK-SCRIPT,no-resolve
- IP-CIDR,198.38.96.0/19,ChuKK-SCRIPT,no-resolve
- IP-CIDR,198.45.48.0/20,ChuKK-SCRIPT,no-resolve
# > niconico
# USER-AGENT,Niconico*,ChuKK-SCRIPT
- DOMAIN-SUFFIX,dmc.nico,ChuKK-SCRIPT
- DOMAIN-SUFFIX,nicovideo.jp,ChuKK-SCRIPT
- DOMAIN-SUFFIX,nimg.jp,ChuKK-SCRIPT
- DOMAIN-SUFFIX,socdm.com,ChuKK-SCRIPT
# > PBS
# USER-AGENT,PBS*,ChuKK-SCRIPT
- DOMAIN-SUFFIX,pbs.org,ChuKK-SCRIPT
# > Pornhub
- DOMAIN-SUFFIX,phncdn.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,pornhub.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,pornhubpremium.com,ChuKK-SCRIPT
# > 台湾好
# USER-AGENT,TaiwanGood*,ChuKK-SCRIPT
- DOMAIN-SUFFIX,skyking.com.tw,ChuKK-SCRIPT
- DOMAIN,hamifans.emome.net,ChuKK-SCRIPT
# > Twitch
- DOMAIN-SUFFIX,twitch.tv,ChuKK-SCRIPT
- DOMAIN-SUFFIX,twitchcdn.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,ttvnw.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,jtvnw.net,ChuKK-SCRIPT
# > ViuTV
# USER-AGENT,Viu*,ChuKK-SCRIPT
# USER-AGENT,ViuTV*,ChuKK-SCRIPT
- DOMAIN-SUFFIX,viu.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,viu.tv,ChuKK-SCRIPT
- DOMAIN,api.viu.now.com,ChuKK-SCRIPT
- DOMAIN,d1k2us671qcoau.cloudfront.net,ChuKK-SCRIPT
- DOMAIN,d2anahhhmp1ffz.cloudfront.net,ChuKK-SCRIPT
- DOMAIN,dfp6rglgjqszk.cloudfront.net,ChuKK-SCRIPT
# > YouTube
# USER-AGENT,com.google.ios.youtube*,ChuKK-SCRIPT
# USER-AGENT,YouTube*,ChuKK-SCRIPT
- DOMAIN-SUFFIX,googlevideo.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,youtube.com,ChuKK-SCRIPT
- DOMAIN,youtubei.googleapis.com,ChuKK-SCRIPT

# (ChuKK-SCRIPT)
# > 愛奇藝台灣站
- DOMAIN,cache.video.iqiyi.com,ChuKK-SCRIPT
# > bilibili
- DOMAIN-SUFFIX,bilibili.com,ChuKK-SCRIPT
- DOMAIN,upos-hz-mirrorakam.akamaized.net,ChuKK-SCRIPT

# (DNS Cache Pollution Protection)
# > Google
- DOMAIN-SUFFIX,ampproject.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,appspot.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,blogger.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,getoutline.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,gvt0.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,gvt1.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,gvt3.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,xn--ngstr-lra8j.com,ChuKK-SCRIPT
- DOMAIN-KEYWORD,google,ChuKK-SCRIPT
- DOMAIN-KEYWORD,blogspot,ChuKK-SCRIPT
# > Microsoft
- DOMAIN-SUFFIX,onedrive.live.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,xboxlive.com,ChuKK-SCRIPT
# > Facebook
- DOMAIN-SUFFIX,cdninstagram.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,fb.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,fb.me,ChuKK-SCRIPT
- DOMAIN-SUFFIX,fbaddins.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,fbcdn.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,fbsbx.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,fbworkmail.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,instagram.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,m.me,ChuKK-SCRIPT
- DOMAIN-SUFFIX,messenger.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,oculus.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,oculuscdn.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,rocksdb.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,whatsapp.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,whatsapp.net,ChuKK-SCRIPT
- DOMAIN-KEYWORD,facebook,ChuKK-SCRIPT
- IP-CIDR,3.123.36.126/32,ChuKK-SCRIPT,no-resolve
- IP-CIDR,35.157.215.84/32,ChuKK-SCRIPT,no-resolve
- IP-CIDR,35.157.217.255/32,ChuKK-SCRIPT,no-resolve
- IP-CIDR,52.58.209.134/32,ChuKK-SCRIPT,no-resolve
- IP-CIDR,54.93.124.31/32,ChuKK-SCRIPT,no-resolve
- IP-CIDR,54.162.243.80/32,ChuKK-SCRIPT,no-resolve
- IP-CIDR,54.173.34.141/32,ChuKK-SCRIPT,no-resolve
- IP-CIDR,54.235.23.242/32,ChuKK-SCRIPT,no-resolve
- IP-CIDR,169.45.248.118/32,ChuKK-SCRIPT,no-resolve
# > Twitter
- DOMAIN-SUFFIX,pscp.tv,ChuKK-SCRIPT
- DOMAIN-SUFFIX,periscope.tv,ChuKK-SCRIPT
- DOMAIN-SUFFIX,t.co,ChuKK-SCRIPT
- DOMAIN-SUFFIX,twimg.co,ChuKK-SCRIPT
- DOMAIN-SUFFIX,twimg.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,twitpic.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,vine.co,ChuKK-SCRIPT
- DOMAIN-KEYWORD,twitter,ChuKK-SCRIPT
# > Telegram
- DOMAIN-SUFFIX,t.me,ChuKK-SCRIPT
- DOMAIN-SUFFIX,tdesktop.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,telegra.ph,ChuKK-SCRIPT
- DOMAIN-SUFFIX,telegram.me,ChuKK-SCRIPT
- DOMAIN-SUFFIX,telegram.org,ChuKK-SCRIPT
- IP-CIDR,91.108.4.0/22,ChuKK-SCRIPT,no-resolve
- IP-CIDR,91.108.8.0/22,ChuKK-SCRIPT,no-resolve
- IP-CIDR,91.108.12.0/22,ChuKK-SCRIPT,no-resolve
- IP-CIDR,91.108.16.0/22,ChuKK-SCRIPT,no-resolve
- IP-CIDR,91.108.56.0/22,ChuKK-SCRIPT,no-resolve
- IP-CIDR,149.154.160.0/20,ChuKK-SCRIPT,no-resolve
# > Line
- DOMAIN-SUFFIX,line.me,ChuKK-SCRIPT
- DOMAIN-SUFFIX,line-apps.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,line-scdn.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,naver.jp,ChuKK-SCRIPT
- IP-CIDR,103.2.30.0/23,ChuKK-SCRIPT,no-resolve
- IP-CIDR,125.209.208.0/20,ChuKK-SCRIPT,no-resolve
- IP-CIDR,147.92.128.0/17,ChuKK-SCRIPT,no-resolve
- IP-CIDR,203.104.144.0/21,ChuKK-SCRIPT,no-resolve
# > Other
- DOMAIN-SUFFIX,4shared.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,520cc.cc,ChuKK-SCRIPT
- DOMAIN-SUFFIX,881903.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,9cache.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,9gag.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,abc.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,abc.net.au,ChuKK-SCRIPT
- DOMAIN-SUFFIX,abebooks.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,amazon.co.jp,ChuKK-SCRIPT
- DOMAIN-SUFFIX,apigee.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,apk-dl.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,apkfind.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,apkmirror.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,apkmonk.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,apkpure.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,aptoide.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,archive.is,ChuKK-SCRIPT
- DOMAIN-SUFFIX,archive.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,arte.tv,ChuKK-SCRIPT
- DOMAIN-SUFFIX,artstation.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,arukas.io,ChuKK-SCRIPT
- DOMAIN-SUFFIX,ask.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,avg.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,avgle.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,badoo.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,bandwagonhost.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,bbc.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,behance.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,bibox.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,biggo.com.tw,ChuKK-SCRIPT
- DOMAIN-SUFFIX,binance.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,bitcointalk.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,bitfinex.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,bitmex.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,bit-z.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,bloglovin.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,bloomberg.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,bloomberg.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,blubrry.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,book.com.tw,ChuKK-SCRIPT
- DOMAIN-SUFFIX,booklive.jp,ChuKK-SCRIPT
- DOMAIN-SUFFIX,books.com.tw,ChuKK-SCRIPT
- DOMAIN-SUFFIX,boslife.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,box.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,businessinsider.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,bwh1.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,castbox.fm,ChuKK-SCRIPT
- DOMAIN-SUFFIX,cbc.ca,ChuKK-SCRIPT
- DOMAIN-SUFFIX,cdw.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,change.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,channelnewsasia.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,ck101.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,clarionproject.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,clyp.it,ChuKK-SCRIPT
- DOMAIN-SUFFIX,cna.com.tw,ChuKK-SCRIPT
- DOMAIN-SUFFIX,comparitech.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,conoha.jp,ChuKK-SCRIPT
- DOMAIN-SUFFIX,crucial.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,cts.com.tw,ChuKK-SCRIPT
- DOMAIN-SUFFIX,cw.com.tw,ChuKK-SCRIPT
- DOMAIN-SUFFIX,cyberctm.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,dailymotion.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,dailyview.tw,ChuKK-SCRIPT
- DOMAIN-SUFFIX,daum.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,daumcdn.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,dcard.tw,ChuKK-SCRIPT
- DOMAIN-SUFFIX,deepdiscount.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,depositphotos.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,deviantart.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,disconnect.me,ChuKK-SCRIPT
- DOMAIN-SUFFIX,discordapp.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,discordapp.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,disqus.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,dlercloud.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,dns2go.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,dowjones.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,dropbox.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,dropboxusercontent.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,duckduckgo.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,dw.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,dynu.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,earthcam.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,ebookservice.tw,ChuKK-SCRIPT
- DOMAIN-SUFFIX,economist.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,edgecastcdn.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,edu,ChuKK-SCRIPT
- DOMAIN-SUFFIX,elpais.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,enanyang.my,ChuKK-SCRIPT
- DOMAIN-SUFFIX,encyclopedia.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,esoir.be,ChuKK-SCRIPT
- DOMAIN-SUFFIX,etherscan.io,ChuKK-SCRIPT
- DOMAIN-SUFFIX,euronews.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,evozi.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,feedly.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,firech.at,ChuKK-SCRIPT
- DOMAIN-SUFFIX,flickr.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,flitto.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,foreignpolicy.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,freebrowser.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,freewechat.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,freeweibo.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,friday.tw,ChuKK-SCRIPT
- DOMAIN-SUFFIX,ftchinese.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,ftimg.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,gate.io,ChuKK-SCRIPT
- DOMAIN-SUFFIX,getlantern.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,getsync.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,globalvoices.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,goo.ne.jp,ChuKK-SCRIPT
- DOMAIN-SUFFIX,goodreads.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,gov,ChuKK-SCRIPT
- DOMAIN-SUFFIX,gov.tw,ChuKK-SCRIPT
- DOMAIN-SUFFIX,greatfire.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,gumroad.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,hbg.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,heroku.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,hightail.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,hk01.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,hkbf.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,hkbookcity.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,hkej.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,hket.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,hkgolden.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,hootsuite.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,hudson.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,hyread.com.tw,ChuKK-SCRIPT
- DOMAIN-SUFFIX,ibtimes.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,i-cable.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,icij.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,icoco.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,imgur.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,initiummall.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,insecam.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,ipfs.io,ChuKK-SCRIPT
- DOMAIN-SUFFIX,issuu.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,istockphoto.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,japantimes.co.jp,ChuKK-SCRIPT
- DOMAIN-SUFFIX,jiji.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,jinx.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,jkforum.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,joinmastodon.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,justmysocks.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,justpaste.it,ChuKK-SCRIPT
- DOMAIN-SUFFIX,kakao.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,kakaocorp.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,kik.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,kobo.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,kobobooks.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,kodingen.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,lemonde.fr,ChuKK-SCRIPT
- DOMAIN-SUFFIX,lepoint.fr,ChuKK-SCRIPT
- DOMAIN-SUFFIX,lihkg.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,listennotes.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,livestream.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,logmein.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,mail.ru,ChuKK-SCRIPT
- DOMAIN-SUFFIX,mailchimp.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,marc.info,ChuKK-SCRIPT
- DOMAIN-SUFFIX,matters.news,ChuKK-SCRIPT
- DOMAIN-SUFFIX,maying.co,ChuKK-SCRIPT
- DOMAIN-SUFFIX,medium.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,mega.nz,ChuKK-SCRIPT
- DOMAIN-SUFFIX,mil,ChuKK-SCRIPT
- DOMAIN-SUFFIX,mingpao.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,mobile01.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,myspace.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,myspacecdn.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,nanyang.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,naver.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,neowin.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,newstapa.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,nexitally.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,nhk.or.jp,ChuKK-SCRIPT
- DOMAIN-SUFFIX,nicovideo.jp,ChuKK-SCRIPT
- DOMAIN-SUFFIX,nii.ac.jp,ChuKK-SCRIPT
- DOMAIN-SUFFIX,nikkei.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,nofile.io,ChuKK-SCRIPT
- DOMAIN-SUFFIX,now.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,nrk.no,ChuKK-SCRIPT
- DOMAIN-SUFFIX,nyt.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,nytchina.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,nytcn.me,ChuKK-SCRIPT
- DOMAIN-SUFFIX,nytco.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,nytimes.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,nytimg.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,nytlog.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,nytstyle.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,ok.ru,ChuKK-SCRIPT
- DOMAIN-SUFFIX,okex.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,on.cc,ChuKK-SCRIPT
- DOMAIN-SUFFIX,orientaldaily.com.my,ChuKK-SCRIPT
- DOMAIN-SUFFIX,overcast.fm,ChuKK-SCRIPT
- DOMAIN-SUFFIX,paltalk.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,pao-pao.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,parsevideo.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,pbxes.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,pcdvd.com.tw,ChuKK-SCRIPT
- DOMAIN-SUFFIX,pchome.com.tw,ChuKK-SCRIPT
- DOMAIN-SUFFIX,pcloud.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,picacomic.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,pinimg.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,pixiv.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,player.fm,ChuKK-SCRIPT
- DOMAIN-SUFFIX,plurk.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,po18.tw,ChuKK-SCRIPT
- DOMAIN-SUFFIX,potato.im,ChuKK-SCRIPT
- DOMAIN-SUFFIX,potatso.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,prism-break.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,proxifier.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,pt.im,ChuKK-SCRIPT
- DOMAIN-SUFFIX,pts.org.tw,ChuKK-SCRIPT
- DOMAIN-SUFFIX,pubu.com.tw,ChuKK-SCRIPT
- DOMAIN-SUFFIX,pubu.tw,ChuKK-SCRIPT
- DOMAIN-SUFFIX,pureapk.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,quora.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,quoracdn.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,rakuten.co.jp,ChuKK-SCRIPT
- DOMAIN-SUFFIX,readingtimes.com.tw,ChuKK-SCRIPT
- DOMAIN-SUFFIX,readmoo.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,redbubble.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,reddit.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,redditmedia.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,resilio.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,reuters.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,reutersmedia.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,rfi.fr,ChuKK-SCRIPT
- DOMAIN-SUFFIX,rixcloud.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,roadshow.hk,ChuKK-SCRIPT
- DOMAIN-SUFFIX,scmp.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,scribd.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,seatguru.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,shadowsocks.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,shopee.tw,ChuKK-SCRIPT
- DOMAIN-SUFFIX,slideshare.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,softfamous.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,soundcloud.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,ssrcloud.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,startpage.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,steamcommunity.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,steemit.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,steemitwallet.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,t66y.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,tapatalk.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,teco-hk.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,teco-mo.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,teddysun.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,textnow.me,ChuKK-SCRIPT
- DOMAIN-SUFFIX,theguardian.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,theinitium.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,thetvdb.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,tineye.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,torproject.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,tumblr.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,turbobit.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,tutanota.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,tvboxnow.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,udn.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,unseen.is,ChuKK-SCRIPT
- DOMAIN-SUFFIX,upmedia.mg,ChuKK-SCRIPT
- DOMAIN-SUFFIX,uptodown.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,urbandictionary.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,ustream.tv,ChuKK-SCRIPT
- DOMAIN-SUFFIX,uwants.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,v2ray.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,viber.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,videopress.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,vimeo.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,voachinese.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,voanews.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,voxer.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,vzw.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,w3schools.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,washingtonpost.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,wattpad.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,whoer.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,wikimapia.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,wikipedia.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,wikiquote.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,wikiwand.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,winudf.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,wire.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,wordpress.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,workflow.is,ChuKK-SCRIPT
- DOMAIN-SUFFIX,worldcat.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,wsj.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,wsj.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,xhamster.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,xn--90wwvt03e.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,xn--i2ru8q2qg.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,xnxx.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,xvideos.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,yahoo.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,yandex.ru,ChuKK-SCRIPT
- DOMAIN-SUFFIX,ycombinator.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,yesasia.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,yes-news.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,yomiuri.co.jp,ChuKK-SCRIPT
- DOMAIN-SUFFIX,you-get.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,zaobao.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,zb.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,zello.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,zeronet.io,ChuKK-SCRIPT
- DOMAIN-SUFFIX,zoom.us,ChuKK-SCRIPT
- DOMAIN-KEYWORD,github,ChuKK-SCRIPT
- DOMAIN-KEYWORD,jav,ChuKK-SCRIPT
- DOMAIN-KEYWORD,pinterest,ChuKK-SCRIPT
- DOMAIN-KEYWORD,porn,ChuKK-SCRIPT
- DOMAIN-KEYWORD,wikileaks,ChuKK-SCRIPT

# (Region-Restricted Access Denied)
- DOMAIN-SUFFIX,apartmentratings.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,apartments.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,bankmobilevibe.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,bing.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,booktopia.com.au,ChuKK-SCRIPT
- DOMAIN-SUFFIX,cccat.io,ChuKK-SCRIPT
- DOMAIN-SUFFIX,centauro.com.br,ChuKK-SCRIPT
- DOMAIN-SUFFIX,clearsurance.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,costco.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,crackle.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,depositphotos.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,dish.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,dmm.co.jp,ChuKK-SCRIPT
- DOMAIN-SUFFIX,dmm.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,dnvod.tv,ChuKK-SCRIPT
- DOMAIN-SUFFIX,esurance.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,extmatrix.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,fastpic.ru,ChuKK-SCRIPT
- DOMAIN-SUFFIX,flipboard.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,fnac.be,ChuKK-SCRIPT
- DOMAIN-SUFFIX,fnac.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,funkyimg.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,fxnetworks.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,gettyimages.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,go.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,here.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,jcpenney.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,jiehua.tv,ChuKK-SCRIPT
- DOMAIN-SUFFIX,mailfence.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,nationwide.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,nbc.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,nexon.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,nordstrom.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,nordstromimage.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,nordstromrack.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,superpages.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,target.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,thinkgeek.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,tracfone.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,unity3d.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,uploader.jp,ChuKK-SCRIPT
- DOMAIN-SUFFIX,vevo.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,viu.tv,ChuKK-SCRIPT
- DOMAIN-SUFFIX,vk.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,vsco.co,ChuKK-SCRIPT
- DOMAIN-SUFFIX,xfinity.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,zattoo.com,ChuKK-SCRIPT
# USER-AGENT,Roam*,ChuKK-SCRIPT

# (The Most Popular Sites)
# > ChuKK-SCRIPT
# >> TestFlight
- DOMAIN,testflight.apple.com,ChuKK-SCRIPT
# >> ChuKK-SCRIPT URL Shortener
- DOMAIN-SUFFIX,appsto.re,ChuKK-SCRIPT
# >> iBooks Store download
- DOMAIN,books.itunes.apple.com,ChuKK-SCRIPT
# >> iTunes Store Moveis Trailers
- DOMAIN,hls.itunes.apple.com,ChuKK-SCRIPT
# >> App Store Preview
- DOMAIN,apps.apple.com,ChuKK-SCRIPT
- DOMAIN,itunes.apple.com,ChuKK-SCRIPT
# >> Spotlight
- DOMAIN,api-glb-sea.smoot.apple.com,ChuKK-SCRIPT
# >> Dictionary
- DOMAIN,lookup-api.apple.com,ChuKK-SCRIPT
# > Google
- DOMAIN-SUFFIX,abc.xyz,ChuKK-SCRIPT
- DOMAIN-SUFFIX,android.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,androidify.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,dialogflow.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,autodraw.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,capitalg.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,certificate-transparency.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,chrome.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,chromeexperiments.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,chromestatus.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,chromium.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,creativelab5.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,debug.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,deepmind.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,firebaseio.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,getmdl.io,ChuKK-SCRIPT
- DOMAIN-SUFFIX,ggpht.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,gmail.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,gmodules.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,godoc.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,golang.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,gstatic.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,gv.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,gwtproject.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,itasoftware.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,madewithcode.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,material.io,ChuKK-SCRIPT
- DOMAIN-SUFFIX,polymer-project.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,admin.recaptcha.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,recaptcha.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,shattered.io,ChuKK-SCRIPT
- DOMAIN-SUFFIX,synergyse.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,tensorflow.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,tfhub.dev,ChuKK-SCRIPT
- DOMAIN-SUFFIX,tiltbrush.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,waveprotocol.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,waymo.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,webmproject.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,webrtc.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,whatbrowser.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,widevine.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,x.company,ChuKK-SCRIPT
- DOMAIN-SUFFIX,youtu.be,ChuKK-SCRIPT
- DOMAIN-SUFFIX,yt.be,ChuKK-SCRIPT
- DOMAIN-SUFFIX,ytimg.com,ChuKK-SCRIPT
# > Microsoft
# >> Microsoft OneDrive
- DOMAIN-SUFFIX,1drv.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,1drv.ms,ChuKK-SCRIPT
- DOMAIN-SUFFIX,blob.core.windows.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,livefilestore.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,onedrive.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,storage.live.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,storage.msn.com,ChuKK-SCRIPT
- DOMAIN,oneclient.sfx.ms,ChuKK-SCRIPT
# > Other
- DOMAIN-SUFFIX,0rz.tw,ChuKK-SCRIPT
- DOMAIN-SUFFIX,4bluestones.biz,ChuKK-SCRIPT
- DOMAIN-SUFFIX,9bis.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,allconnected.co,ChuKK-SCRIPT
- DOMAIN-SUFFIX,aol.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,bcc.com.tw,ChuKK-SCRIPT
- DOMAIN-SUFFIX,bit.ly,ChuKK-SCRIPT
- DOMAIN-SUFFIX,bitshare.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,blog.jp,ChuKK-SCRIPT
- DOMAIN-SUFFIX,blogimg.jp,ChuKK-SCRIPT
- DOMAIN-SUFFIX,blogtd.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,broadcast.co.nz,ChuKK-SCRIPT
- DOMAIN-SUFFIX,camfrog.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,cfos.de,ChuKK-SCRIPT
- DOMAIN-SUFFIX,citypopulation.de,ChuKK-SCRIPT
- DOMAIN-SUFFIX,cloudfront.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,ctitv.com.tw,ChuKK-SCRIPT
- DOMAIN-SUFFIX,cuhk.edu.hk,ChuKK-SCRIPT
- DOMAIN-SUFFIX,cusu.hk,ChuKK-SCRIPT
- DOMAIN-SUFFIX,discord.gg,ChuKK-SCRIPT
- DOMAIN-SUFFIX,discuss.com.hk,ChuKK-SCRIPT
- DOMAIN-SUFFIX,dropboxapi.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,duolingo.cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,edditstatic.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,flickriver.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,focustaiwan.tw,ChuKK-SCRIPT
- DOMAIN-SUFFIX,free.fr,ChuKK-SCRIPT
- DOMAIN-SUFFIX,gigacircle.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,hk-pub.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,hosting.co.uk,ChuKK-SCRIPT
- DOMAIN-SUFFIX,hwcdn.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,ifixit.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,iphone4hongkong.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,iphonetaiwan.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,iptvbin.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,linksalpha.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,manyvids.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,myactimes.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,newsblur.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,now.im,ChuKK-SCRIPT
- DOMAIN-SUFFIX,nowe.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,redditlist.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,s3.amazonaws.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,signal.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,smartmailcloud.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,sparknotes.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,streetvoice.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,supertop.co,ChuKK-SCRIPT
- DOMAIN-SUFFIX,tv.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,typepad.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,udnbkk.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,urbanairship.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,whispersystems.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,wikia.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,wn.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,wolframalpha.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,x-art.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,yimg.com,ChuKK-SCRIPT
- DOMAIN,api.steampowered.com,ChuKK-SCRIPT
- DOMAIN,store.steampowered.com,ChuKK-SCRIPT

# China Area Network
# > 360
- DOMAIN-SUFFIX,qhres.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,qhimg.com,ChuKK-SCRIPT
# > Akamai
- DOMAIN-SUFFIX,akadns.net,ChuKK-SCRIPT
# - DOMAIN-SUFFIX,akamai.net,ChuKK-SCRIPT
# - DOMAIN-SUFFIX,akamaiedge.net,ChuKK-SCRIPT
# - DOMAIN-SUFFIX,akamaihd.net,ChuKK-SCRIPT
# - DOMAIN-SUFFIX,akamaistream.net,ChuKK-SCRIPT
# - DOMAIN-SUFFIX,akamaized.net,ChuKK-SCRIPT
# > Alibaba
# USER-AGENT,%E4%BC%98%E9%85%B7*,ChuKK-SCRIPT
- DOMAIN-SUFFIX,alibaba.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,alicdn.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,alikunlun.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,alipay.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,amap.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,autonavi.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,dingtalk.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,mxhichina.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,soku.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,taobao.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,tmall.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,tmall.hk,ChuKK-SCRIPT
- DOMAIN-SUFFIX,ykimg.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,youku.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,xiami.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,xiami.net,ChuKK-SCRIPT
# > Baidu
- DOMAIN-SUFFIX,baidu.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,baidubcr.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,bdstatic.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,yunjiasu-cdn.net,ChuKK-SCRIPT
# > bilibili
- DOMAIN-SUFFIX,acgvideo.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,biliapi.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,biliapi.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,bilibili.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,bilibili.tv,ChuKK-SCRIPT
- DOMAIN-SUFFIX,hdslb.com,ChuKK-SCRIPT
# > Blizzard
- DOMAIN-SUFFIX,blizzard.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,battle.net,ChuKK-SCRIPT
- DOMAIN,blzddist1-a.akamaihd.net,ChuKK-SCRIPT
# > ByteDance
- DOMAIN-SUFFIX,feiliao.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,pstatp.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,snssdk.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,iesdouyin.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,toutiao.com,ChuKK-SCRIPT
# > CCTV
- DOMAIN-SUFFIX,cctv.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,cctvpic.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,livechina.com,ChuKK-SCRIPT
# > DiDi
- DOMAIN-SUFFIX,didialift.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,didiglobal.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,udache.com,ChuKK-SCRIPT
# > 蛋蛋赞
- DOMAIN-SUFFIX,343480.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,baduziyuan.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,com-hs-hkdy.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,czybjz.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,dandanzan.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,fjhps.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,kuyunbo.club,ChuKK-SCRIPT
# > ChinaNet
- DOMAIN-SUFFIX,21cn.com,ChuKK-SCRIPT
# > HunanTV
- DOMAIN-SUFFIX,hitv.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,mgtv.com,ChuKK-SCRIPT
# > iQiyi
- DOMAIN-SUFFIX,iqiyi.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,iqiyipic.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,71.am.com,ChuKK-SCRIPT
# > JD
- DOMAIN-SUFFIX,jd.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,jd.hk,ChuKK-SCRIPT
- DOMAIN-SUFFIX,jdpay.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,360buyimg.com,ChuKK-SCRIPT
# > Kingsoft
- DOMAIN-SUFFIX,iciba.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,ksosoft.com,ChuKK-SCRIPT
# > Meitu
- DOMAIN-SUFFIX,meitu.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,meitudata.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,meitustat.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,meipai.com,ChuKK-SCRIPT
# > MI
- DOMAIN-SUFFIX,duokan.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,mi-img.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,miui.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,miwifi.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,xiaomi.com,ChuKK-SCRIPT
# > Microsoft
- DOMAIN-SUFFIX,microsoft.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,msecnd.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,office365.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,outlook.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,s-microsoft.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,visualstudio.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,windows.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,windowsupdate.com,ChuKK-SCRIPT
- DOMAIN,officecdn-microsoft-com.akamaized.net,ChuKK-SCRIPT
# > NetEase
# USER-AGENT,NeteaseMusic*,ChuKK-SCRIPT
# USER-AGENT,%E7%BD%91%E6%98%93%E4%BA%91%E9%9F%B3%E4%B9%90*,ChuKK-SCRIPT
- DOMAIN-SUFFIX,163.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,126.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,127.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,163yun.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,lofter.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,netease.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,ydstatic.com,ChuKK-SCRIPT
# > Sina
- DOMAIN-SUFFIX,sina.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,weibo.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,weibocdn.com,ChuKK-SCRIPT
# > Sohu
- DOMAIN-SUFFIX,sohu.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,sohucs.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,sohu-inc.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,v-56.com,ChuKK-SCRIPT
# > Sogo
- DOMAIN-SUFFIX,sogo.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,sogou.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,sogoucdn.com,ChuKK-SCRIPT
# > Steam
- DOMAIN-SUFFIX,steampowered.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,steam-chat.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,steamgames.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,steamusercontent.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,steamcontent.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,steamstatic.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,steamcdn-a.akamaihd.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,steamstat.us,ChuKK-SCRIPT
# > Tencent
# USER-AGENT,MicroMessenger%20Client,ChuKK-SCRIPT
# USER-AGENT,WeChat*,ChuKK-SCRIPT
- DOMAIN-SUFFIX,gtimg.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,idqqimg.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,igamecj.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,myapp.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,myqcloud.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,qq.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,tencent.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,tencent-cloud.net,ChuKK-SCRIPT
# > YYeTs
# USER-AGENT,YYeTs*,ChuKK-SCRIPT
- DOMAIN-SUFFIX,jstucdn.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,zimuzu.io,ChuKK-SCRIPT
- DOMAIN-SUFFIX,zimuzu.tv,ChuKK-SCRIPT
- DOMAIN-SUFFIX,zmz2019.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,zmzapi.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,zmzapi.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,zmzfile.com,ChuKK-SCRIPT
# > Content Delivery Network
- DOMAIN-SUFFIX,ccgslb.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,ccgslb.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,chinanetcenter.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,meixincdn.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,ourdvs.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,staticdn.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,wangsu.com,ChuKK-SCRIPT
# > IP Query
- DOMAIN-SUFFIX,ipip.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,ip.la,ChuKK-SCRIPT
- DOMAIN-SUFFIX,ip-cdn.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,ipv6-test.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,test-ipv6.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,whatismyip.com,ChuKK-SCRIPT
# > Speed Test
# - DOMAIN-SUFFIX,speedtest.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,netspeedtestmaster.com,ChuKK-SCRIPT
- DOMAIN,speedtest.macpaw.com,ChuKK-SCRIPT
# > Private Tracker
- DOMAIN-SUFFIX,awesome-hd.me,ChuKK-SCRIPT
- DOMAIN-SUFFIX,broadcasthe.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,chdbits.co,ChuKK-SCRIPT
- DOMAIN-SUFFIX,classix-unlimited.co.uk,ChuKK-SCRIPT
- DOMAIN-SUFFIX,empornium.me,ChuKK-SCRIPT
- DOMAIN-SUFFIX,gazellegames.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,hdchina.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,hdsky.me,ChuKK-SCRIPT
- DOMAIN-SUFFIX,icetorrent.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,jpopsuki.eu,ChuKK-SCRIPT
- DOMAIN-SUFFIX,keepfrds.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,madsrevolution.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,m-team.cc,ChuKK-SCRIPT
- DOMAIN-SUFFIX,nanyangpt.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,ncore.cc,ChuKK-SCRIPT
- DOMAIN-SUFFIX,open.cd,ChuKK-SCRIPT
- DOMAIN-SUFFIX,ourbits.club,ChuKK-SCRIPT
- DOMAIN-SUFFIX,passthepopcorn.me,ChuKK-SCRIPT
- DOMAIN-SUFFIX,privatehd.to,ChuKK-SCRIPT
- DOMAIN-SUFFIX,redacted.ch,ChuKK-SCRIPT
- DOMAIN-SUFFIX,springsunday.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,tjupt.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,totheglory.im,ChuKK-SCRIPT
# > Scholar
- DOMAIN-SUFFIX,acm.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,acs.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,aip.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,ams.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,annualreviews.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,aps.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,ascelibrary.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,asm.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,asme.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,astm.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,bmj.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,cambridge.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,cas.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,clarivate.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,ebscohost.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,emerald.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,engineeringvillage.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,icevirtuallibrary.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,ieee.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,imf.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,iop.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,jamanetwork.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,jhu.edu,ChuKK-SCRIPT
- DOMAIN-SUFFIX,jstor.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,karger.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,libguides.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,madsrevolution.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,mpg.de,ChuKK-SCRIPT
- DOMAIN-SUFFIX,myilibrary.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,nature.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,oecd-ilibrary.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,osapublishing.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,oup.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,ovid.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,oxfordartonline.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,oxfordbibliographies.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,oxfordmusiconline.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,pnas.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,proquest.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,rsc.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,sagepub.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,sciencedirect.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,sciencemag.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,scopus.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,siam.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,spiedigitallibrary.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,springer.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,springerlink.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,tandfonline.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,un.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,uni-bielefeld.de,ChuKK-SCRIPT
- DOMAIN-SUFFIX,webofknowledge.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,westlaw.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,wiley.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,worldbank.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,worldscientific.com,ChuKK-SCRIPT
# > Plex Media Server
- DOMAIN-SUFFIX,plex.tv,ChuKK-SCRIPT
# > Other
- DOMAIN-SUFFIX,cn,ChuKK-SCRIPT
- DOMAIN-SUFFIX,360in.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,51ym.me,ChuKK-SCRIPT
- DOMAIN-SUFFIX,8686c.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,abchina.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,accuweather.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,aicoinstorge.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,air-matters.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,air-matters.io,ChuKK-SCRIPT
- DOMAIN-SUFFIX,aixifan.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,amd.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,b612.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,bdatu.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,beitaichufang.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,bjango.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,booking.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,bstatic.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,cailianpress.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,camera360.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,chinaso.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,chua.pro,ChuKK-SCRIPT
- DOMAIN-SUFFIX,chuimg.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,chunyu.mobi,ChuKK-SCRIPT
- DOMAIN-SUFFIX,chushou.tv,ChuKK-SCRIPT
- DOMAIN-SUFFIX,cmbchina.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,cmbimg.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,ctrip.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,dfcfw.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,docschina.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,douban.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,doubanio.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,douyu.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,dxycdn.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,dytt8.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,eastmoney.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,eudic.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,feng.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,fengkongcloud.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,frdic.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,futu5.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,futunn.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,gandi.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,geilicdn.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,getpricetag.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,gifshow.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,godic.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,hicloud.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,hongxiu.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,hostbuf.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,huxiucdn.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,huya.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,infinitynewtab.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,ithome.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,java.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,jidian.im,ChuKK-SCRIPT
- DOMAIN-SUFFIX,kaiyanapp.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,kaspersky-labs.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,keepcdn.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,kkmh.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,licdn.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,linkedin.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,loli.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,luojilab.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,maoyan.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,maoyun.tv,ChuKK-SCRIPT
- DOMAIN-SUFFIX,meituan.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,meituan.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,mobike.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,moke.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,mubu.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,myzaker.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,nim-lang-cn.org,ChuKK-SCRIPT
- DOMAIN-SUFFIX,nvidia.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,oracle.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,paypal.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,paypalobjects.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,qdaily.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,qidian.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,qyer.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,qyerstatic.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,raychase.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,ronghub.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,ruguoapp.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,s-reader.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,sankuai.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,scomper.me,ChuKK-SCRIPT
- DOMAIN-SUFFIX,seafile.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,sm.ms,ChuKK-SCRIPT
- DOMAIN-SUFFIX,smzdm.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,snapdrop.net,ChuKK-SCRIPT
- DOMAIN-SUFFIX,snwx.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,sspai.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,takungpao.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,teamviewer.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,tianyancha.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,udacity.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,uning.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,vmware.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,weather.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,weico.cc,ChuKK-SCRIPT
- DOMAIN-SUFFIX,weidian.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,xiachufang.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,ximalaya.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,xinhuanet.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,xmcdn.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,yangkeduo.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,zhangzishi.cc,ChuKK-SCRIPT
- DOMAIN-SUFFIX,zhihu.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,zhimg.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,zhuihd.com,ChuKK-SCRIPT
- DOMAIN,download.jetbrains.com,ChuKK-SCRIPT
- DOMAIN,images-cn.ssl-images-amazon.com,ChuKK-SCRIPT

# > ChuKK-SCRIPT
- DOMAIN-SUFFIX,aaplimg.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,apple.co,ChuKK-SCRIPT
- DOMAIN-SUFFIX,apple.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,apple-cloudkit.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,appstore.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,cdn-apple.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,crashlytics.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,icloud.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,icloud-content.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,me.com,ChuKK-SCRIPT
- DOMAIN-SUFFIX,mzstatic.com,ChuKK-SCRIPT
- DOMAIN,www-cdn.icloud.com.akadns.net,ChuKK-SCRIPT
- DOMAIN,clash.razord.top,ChuKK-SCRIPT
- DOMAIN,v2ex.com,ChuKK-SCRIPT
- IP-CIDR,17.0.0.0/8,ChuKK-SCRIPT,no-resolve

# Local Area Network
- IP-CIDR,192.168.0.0/16,ChuKK-SCRIPT
- IP-CIDR,10.0.0.0/8,ChuKK-SCRIPT
- IP-CIDR,172.16.0.0/12,ChuKK-SCRIPT
- IP-CIDR,127.0.0.0/8,ChuKK-SCRIPT
- IP-CIDR,100.64.0.0/10,ChuKK-SCRIPT

# DNSPod Public DNS+
- IP-CIDR,119.28.28.28/32,ChuKK-SCRIPT,no-resolve
# GeoIP China
- GEOIP,CN,ChuKK-SCRIPT

- MATCH,ChuKK-SCRIPT

proxies:' >> /root/.config/clash/config.yaml 
[[ $mode = 2 ]] && echo -e '
proxies:' >> /root/.config/clash/config.yaml 
}

conFIN() {
confRULE
[[ ! -z ${proTRO} ]] && echo -e "${proTRO}" >> /root/.config/clash/config.yaml
[[ ! -z ${proV2R} ]] && echo -e "${proV2R}" >> /root/.config/clash/config.yaml
[[ ! -z ${proXR} ]] && echo -e "${proXR}" >> /root/.config/clash/config.yaml

#echo ''

echo "#POWER BY @drowkid01" >> /root/.config/clash/config.yaml
}

enon(){
		clear
		msg -bar
		blanco " Se ha agregado un autoejecutor en el Sector de Inicios Rapidos"
		msg -bar
		blanco "	  Para Acceder al menu Rapido \n	     Utilize * clash.sh * !!!"
		msg -bar
		echo -e "		\033[4;31mNOTA importante\033[0m"
		echo -e " \033[0;31mSi deseas desabilitar esta opcion, apagala"
		echo -e " Y te recomiendo, no alterar nada en este menu, para"
		echo -e "             Evitar Errores Futuros"
		echo -e " y causar problemas en futuras instalaciones.\033[0m"
		msg -bar
		continuar
		read foo
}
enoff(){
rm -f /bin/clash.sh
		msg -bar
		echo -e "		\033[4;31mNOTA importante\033[0m"
		echo -e " \033[0;31mSe ha Desabilitado el menu Rapido de clash.sh"
		echo -e " Y te recomiendo, no alterar nada en este menu, para"
		echo -e "             Evitar Errores Futuros"
		echo -e " y causar problemas en futuras instalaciones.\033[0m"
		msg -bar
		continuar
		read foo
}

enttrada () {
echo 'source <(curl -sSL https://raw.githubusercontent.com/emirjorge/Script-Z/master/CHUMO/Recursos/ClashForAndroidGLOBAL.sh)' > /bin/clash.sh && chmod +x /bin/clash.sh
}

blanco(){
	[[ !  $2 = 0 ]] && {
		echo -e "\033[1;37m$1\033[0m"
	} || {
		echo -ne " \033[1;37m$1:\033[0m "
	}
}
title(){
	msg -bar
	blanco "$1"
	msg -bar
}
col(){
	nom=$(printf '%-55s' "\033[0;92m${1} \033[0;31m>> \033[1;37m${2}")
	echo -e "	$nom\033[0;31m${3}   \033[0;92m${4}\033[0m"
}
col2(){
	echo -e " \033[1;91m$1\033[0m \033[1;37m$2\033[0m"
}
vacio(){
blanco "\n no se puede ingresar campos vacios..."
}
cancelar(){
echo -e "\n \033[3;49;31minstalacion cancelada...\033[0m"
}
continuar(){
echo -e " \033[3;49;32mEnter para continuar...\033[0m"
}
userDat(){
	blanco "	NÂ°    Usuarios 		  fech exp   dias"
	msg -bar
}
view_usert(){
configt="/usr/local/etc/trojan/config.json"
tempt="/etc/trojan/temp.json"
trojdirt="/etc/trojan" 
user_conf="/etc/trojan/user"
backdirt="/etc/trojan/back" 
tmpdirt="$backdir/tmp"
	unset seg
	seg=$(date +%s)
	while :
	do
	nick="$(cat $configt | grep ',"')"
	users="$(cat $configt | jq -r .password[])"
		title "	ESCOJE USUARIO TROJAN"
		userDat

		n=1
		for i in $users
		do
			unset DateExp
			unset seg_exp
			unset exp

			[[ $i = chumoghscript ]] && {
				Usr="Admin"
				DateExp=" Ilimitado"
			} || {
			Usr="$(cat ${user_conf}|grep -w "${i}"|cut -d'|' -f1)"
				DateExp="$(cat ${user_conf}|grep -w "${i}"|cut -d'|' -f3)"
				seg_exp=$(date +%s --date="$DateExp")
				exp="[$(($(($seg_exp - $seg)) / 86400))]"
			}
			col "$n)" "${Usr}" "$DateExp" "$exp"
			let n++
		done
		msg -bar
		col "0)" "VOLVER"
		msg -bar
		blanco "SELECCIONA USUARIO" 0
		read opcion
		[[ -z $opcion ]] && vacio && sleep 0.3s && continue
		[[ $opcion = 0 ]] && tropass="user_null" && break
		n=1
		unset i
		for i in $users
		do
		[[ $n = $opcion ]] && tropass=$i
			let n++
		done
		let opcion--
		addip=$(wget -qO- ifconfig.me)
		host=$(cat $configt | jq -r .ssl.sni)
		trojanport=$(cat $configt | jq -r .local_port)
		UUID=$(cat $configt | jq -r .password[$opcion])
		Usr="$(cat ${user_conf}|grep -w "${UUID}"|cut -d'|' -f1)"
		echo "USER ${Usr} : $UUID " 
		break
	done
}

view_user(){
config="/etc/v2ray/config.json"
temp="/etc/v2ray/temp.json"
v2rdir="/etc/v2r" && [[ ! -d $v2rdir ]] && mkdir $v2rdir
user_conf="/etc/v2r/user" && [[ ! -e $user_conf ]] && touch $user_conf
backdir="/etc/v2r/back" && [[ ! -d ${backdir} ]] && mkdir ${backdir}
tmpdir="$backdir/tmp"
	unset seg
	seg=$(date +%s)
	while :
	do
		users=$(cat $config | jq .inbounds[].settings.clients[] | jq -r .email)

		title "	VER USUARIO V2RAY REGISTRADO"
		userDat

		n=1
		for i in $users
		do
			unset DateExp
			unset seg_exp
			unset exp

			[[ $i = null ]] && {
				i="Admin"
				DateExp=" Ilimitado"
			} || {
				DateExp="$(cat ${user_conf}|grep -w "${i}"|cut -d'|' -f3)"
				seg_exp=$(date +%s --date="$DateExp")
				exp="[$(($(($seg_exp - $seg)) / 86400))]"
			}

			col "$n)" "$i" "$DateExp" "$exp"
			let n++
		done

		msg -bar
		col "0)" "VOLVER"
		msg -bar
		blanco "Escoje Tu Usuario : " 0
		read opcion
		[[ -z $opcion ]] && vacio && sleep 0.3s && continue
		[[ $opcion = 0 ]] && break
		let opcion--
		ps=$(jq .inbounds[].settings.clients[$opcion].email $config) && [[ $ps = null ]] && ps="default"
		uid=$(jq .inbounds[].settings.clients[$opcion].id $config)
		aluuiid=$(jq .inbounds[].settings.clients[$opcion].alterId $config)
		add=$(jq '.inbounds[].domain' $config) && [[ $add = null ]] && add=$(wget -qO- ipv4.icanhazip.com)
		host=$(jq '.inbounds[].streamSettings.wsSettings.headers.Host' $config) && [[ $host = null ]] && host=''
		net=$(jq '.inbounds[].streamSettings.network' $config)
		parche=$(jq -r .inbounds[].streamSettings.wsSettings.path $config) && [[ $path = null ]] && parche='' 
		v2port=$(jq '.inbounds[].port' $config)
		tls=$(jq '.inbounds[].streamSettings.security' $config)
		[[ $net = '"grpc"' ]] && path=$(jq '.inbounds[].streamSettings.grpcSettings.serviceName'  $config) || path=$(jq '.inbounds[].streamSettings.wsSettings.path' $config)
		addip=$(wget -qO- ifconfig.me)
		echo "Usuario $ps Seleccionado" 
		break
	done
}

_view_userXR(){
config="/etc/xray/config.json"
temp="/etc/xray/temp.json"
v2rdir="/etc/xr" && [[ ! -d $v2rdir ]] && mkdir $v2rdir
user_conf="/etc/xr/user" && [[ ! -e $user_conf ]] && touch $user_conf
backdir="/etc/xr/back" && [[ ! -d ${backdir} ]] && mkdir ${backdir}
tmpdir="$backdir/tmp"
	unset seg
	seg=$(date +%s)
	while :
	do
		users=$(cat $config | jq .inbounds[].settings.clients[] | jq -r .email)

		title "	VER USUARIO XRAY REGISTRADO"
		userDat

		n=1
		for i in $users
		do
			unset DateExp
			unset seg_exp
			unset exp

			[[ $i = null ]] && {
				i="Admin"
				DateExp=" Ilimitado"
			} || {
				DateExp="$(cat ${user_conf}|grep -w "${i}"|cut -d'|' -f3)"
				seg_exp=$(date +%s --date="$DateExp")
				exp="[$(($(($seg_exp - $seg)) / 86400))]"
			}

			col "$n)" "$i" "$DateExp" "$exp"
			let n++
		done

		msg -bar
		col "0)" "VOLVER"
		msg -bar
		blanco "Escoje Tu Usuario : " 0
		read opcion
		[[ -z $opcion ]] && vacio && sleep 0.3s && continue
		[[ $opcion = 0 ]] && break
		let opcion--
		psX=$(jq .inbounds[].settings.clients[$opcion].email $config) && [[ $psX = null ]] && ps="default"
		uidX=$(jq .inbounds[].settings.clients[$opcion].id $config)
		aluuiidX=$(jq .inbounds[].settings.clients[$opcion].alterId $config)
		addX=$(jq '.inbounds[].domain' $config) && [[ $addX = null ]] && add=$(wget -qO- ipv4.icanhazip.com)
		hostX=$(jq '.inbounds[].streamSettings.wsSettings.headers.Host' $config) && [[ $hostX = null ]] && hostX=''
		netX=$(jq '.inbounds[].streamSettings.network' $config)
		parcheX=$(jq -r .inbounds[].streamSettings.wsSettings.path $config) && [[ $pathX = null ]] && parcheX='' 
		v2portX=$(jq '.inbounds[].port' $config)
		tlsX=$(jq '.inbounds[].streamSettings.security' $config)
		[[ $netX = '"grpc"' ]] && pathX=$(jq '.inbounds[].streamSettings.grpcSettings.serviceName'  $config) || pathX=$(jq '.inbounds[].streamSettings.wsSettings.path' $config)
		addipX=$(wget -qO- ifconfig.me)
		echo "Usuario XRAY SERA  $psX Seleccionado" 
		break
	done
}

[[ ! -d /root/.config/clash ]] && fun_insta || fun_ip
clear
[[ -e /root/name ]] && figlet -p -f slant < /root/name || echo -e "\033[7;49;35m    =====>>â–ºâ–º ðŸ² New DrowKidðŸ’¥VPS ðŸ² â—„â—„<<=====      \033[0m"
fileon=$(ls -la /var/www/html | grep "yaml" | wc -l)
filelo=$(ls -la /root/.config/clash | grep "yaml" | wc -l)
cd
msg -bar
echo -e "\033[1;37m ✬  Linux Dist: $(less /etc/issue.net)\033[0m"
msg -bar
echo -e "\033[1;37m ✬ Ficheros Online:	$fileon  ✬ Ficheros Locales: $filelo\033[0m"
msg -bar
echo -e "\033[1;37m - Menu Iterativo Clash for Android - DrowKid \033[0m"
msg -bar
echo -e "\033[1;37mSeleccione :    Para Salir Ctrl + C o 0 Para Regresar\033[1;33m"
unset yesno
echo -e " DESEAS CONTINUAR CON LA CARGA DE CONFIG CLASH?"
while [[ ${yesno} != @(s|S|y|Y|n|N) ]]; do
read -p "[S/N]: " yesno
tput cuu1 && tput dl1
done
if [[ ${yesno} = @(s|S|y|Y) ]]; then
unset yesno numwt
#[[ -e /root/name ]] && figlet -p -f slant < /root/name || echo -e "\033[7;49;35m    =====>>â–ºâ–º ðŸ² New DrowKidðŸ’¥VPS ðŸ² â—„â—„<<=====      \033[0m"
echo -e "[\033[1;31m-\033[1;33m]\033[1;31m \033[1;33m"
echo -e "\033[1;33m ✬ Ingresa tu Whatsapp junto a tu codigo de Pais"
read -p " Ejemplo: +593987072611 : " numwt
echo -e "[\033[1;31m-\033[1;33m]\033[1;31m \033[1;33m"
echo -e "\033[1;33m ✬ Ingresa Clase de Servidor ( Gratis - PREMIUM )"
read -p " Ejemplo: PREMIUM : " srvip
if [[ -z $srvip ]]; then
srvip="NewADM"
fi
	while :
	do
	[[ -z ${opcion} ]] || break
		clear
		echo -e " ESCOJE TU METODO DE SELECCION "
		echo -e "  "
		echo -e " SINO CONOCES DE ESTO, ESCOJE 2 "
		echo -e "  "
		msg -bar
		echo -e "1 - SELECTOR RULES"
		echo -e "2 - SELECTOR GLOBAL"
		msg -bar
		echo -e " 0) CANCELAR"
		msg -bar
		read -p " ESCOJE : " opcion
		case $opcion in
			1)configINIT_rule "$opcion"
			break;;
			2)configINIT_global "$opcion"
			break;;
			0) break;;
			*) echo -e "\n selecione una opcion del 0 al 2" && sleep 0.3s;;
		esac
	done
INITClash
fi
}

function shadowsocks(){
eval PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:~/bin
export PATH
red='\033[0;31m'
green='\033[0;32m'
yellow='\033[0;33m'
plain='\033[0m'
[[ $EUID -ne 0 ]] && echo -e "[${red}Error${plain}] This script must be run as root!" && exit 1
cur_dir=$( pwd )
software=(Shadowsocks-Python ShadowsocksR Shadowsocks-Go Shadowsocks-libev)
libsodium_file="libsodium-1.0.17"
libsodium_url="https://github.com/jedisct1/libsodium/releases/download/1.0.17/libsodium-1.0.17.tar.gz"
mbedtls_file="mbedtls-2.16.0"
mbedtls_url="https://tls.mbed.org/download/mbedtls-2.16.0-gpl.tgz"
shadowsocks_python_file="shadowsocks-master"
shadowsocks_python_url="https://github.com/shadowsocks/shadowsocks/archive/master.zip"
shadowsocks_python_init="/etc/init.d/shadowsocks-python"
shadowsocks_python_config="/etc/shadowsocks-python/config.json"
shadowsocks_python_centos="https://raw.githubusercontent.com/teddysun/shadowsocks_install/master/shadowsocks"
shadowsocks_python_debian="https://raw.githubusercontent.com/teddysun/shadowsocks_install/master/shadowsocks-debian"
shadowsocks_r_file="shadowsocksr-3.2.2"
shadowsocks_r_url="https://github.com/shadowsocksrr/shadowsocksr/archive/3.2.2.tar.gz"
shadowsocks_r_init="/etc/init.d/shadowsocks-r"
shadowsocks_r_config="/etc/shadowsocks-r/config.json"
shadowsocks_r_centos="https://raw.githubusercontent.com/teddysun/shadowsocks_install/master/shadowsocksR"
shadowsocks_r_debian="https://raw.githubusercontent.com/teddysun/shadowsocks_install/master/shadowsocksR-debian"
shadowsocks_go_file_64="shadowsocks-server-linux64-1.2.2"
shadowsocks_go_url_64="https://dl.lamp.sh/shadowsocks/shadowsocks-server-linux64-1.2.2.gz"
shadowsocks_go_file_32="shadowsocks-server-linux32-1.2.2"
shadowsocks_go_url_32="https://dl.lamp.sh/shadowsocks/shadowsocks-server-linux32-1.2.2.gz"
shadowsocks_go_init="/etc/init.d/shadowsocks-go"
shadowsocks_go_config="/etc/shadowsocks-go/config.json"
shadowsocks_go_centos="https://raw.githubusercontent.com/teddysun/shadowsocks_install/master/shadowsocks-go"
shadowsocks_go_debian="https://raw.githubusercontent.com/teddysun/shadowsocks_install/master/shadowsocks-go-debian"
shadowsocks_libev_init="/etc/init.d/shadowsocks-libev"
shadowsocks_libev_config="/etc/shadowsocks-libev/config.json"
shadowsocks_libev_centos="https://raw.githubusercontent.com/teddysun/shadowsocks_install/master/shadowsocks-libev"
shadowsocks_libev_debian="https://raw.githubusercontent.com/teddysun/shadowsocks_install/master/shadowsocks-libev-debian"
common_ciphers=(
aes-256-gcm
aes-192-gcm
aes-128-gcm
aes-256-ctr
aes-192-ctr
aes-128-ctr
aes-256-cfb
aes-192-cfb
aes-128-cfb
camellia-128-cfb
camellia-192-cfb
camellia-256-cfb
xchacha20-ietf-poly1305
chacha20-ietf-poly1305
chacha20-ietf
chacha20
salsa20
rc4-md5
)
go_ciphers=(
aes-256-cfb
aes-192-cfb
aes-128-cfb
aes-256-ctr
aes-192-ctr
aes-128-ctr
chacha20-ietf
chacha20
salsa20
rc4-md5
)
r_ciphers=(
none
aes-256-cfb
aes-192-cfb
aes-128-cfb
aes-256-cfb8
aes-192-cfb8
aes-128-cfb8
aes-256-ctr
aes-192-ctr
aes-128-ctr
chacha20-ietf
chacha20
salsa20
xchacha20
xsalsa20
rc4-md5
)
protocols=(
origin
verify_deflate
auth_sha1_v4
auth_sha1_v4_compatible
auth_aes128_md5
auth_aes128_sha1
auth_chain_a
auth_chain_b
auth_chain_c
auth_chain_d
auth_chain_e
auth_chain_f
)
obfs=(
plain
http_simple
http_simple_compatible
http_post
http_post_compatible
tls1.2_ticket_auth
tls1.2_ticket_auth_compatible
tls1.2_ticket_fastauth
tls1.2_ticket_fastauth_compatible
)
obfs_libev=(http tls)
libev_obfs=""
disable_selinux(){
if [ -s /etc/selinux/config ] && grep 'SELINUX=enforcing' /etc/selinux/config; then
sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config
setenforce 0
fi
}
check_sys(){
local checkType=$1
local value=$2
local release=''
local systemPackage=''
if [[ -f /etc/redhat-release ]]; then
release="centos"
systemPackage="yum"
elif grep -Eqi "debian|raspbian" /etc/issue; then
release="debian"
systemPackage="apt"
elif grep -Eqi "ubuntu" /etc/issue; then
release="ubuntu"
systemPackage="apt"
elif grep -Eqi "centos|red hat|redhat" /etc/issue; then
release="centos"
systemPackage="yum"
elif grep -Eqi "debian|raspbian" /proc/version; then
release="debian"
systemPackage="apt"
elif grep -Eqi "ubuntu" /proc/version; then
release="ubuntu"
systemPackage="apt"
elif grep -Eqi "centos|red hat|redhat" /proc/version; then
release="centos"
systemPackage="yum"
fi
if [[ "${checkType}" == "sysRelease" ]]; then
if [ "${value}" == "${release}" ]; then
return 0
else
return 1
fi
elif [[ "${checkType}" == "packageManager" ]]; then
if [ "${value}" == "${systemPackage}" ]; then
return 0
else
return 1
fi
fi
}
version_ge(){
test "$(echo "$@" | tr " " "\n" | sort -rV | head -n 1)" == "$1"
}
version_gt(){
test "$(echo "$@" | tr " " "\n" | sort -V | head -n 1)" != "$1"
}
check_kernel_version(){
local kernel_version=$(uname -r | cut -d- -f1)
if version_gt ${kernel_version} 3.7.0; then
return 0
else
return 1
fi
}
check_kernel_headers(){
if check_sys packageManager yum; then
if rpm -qa | grep -q headers-$(uname -r); then
return 0
else
return 1
fi
elif check_sys packageManager apt; then
if dpkg -s linux-headers-$(uname -r) > /dev/null 2>&1; then
return 0
else
return 1
fi
fi
return 1
}
getversion(){
if [[ -s /etc/redhat-release ]]; then
grep -oE  "[0-9.]+" /etc/redhat-release
else
grep -oE  "[0-9.]+" /etc/issue
fi
}
centosversion(){
if check_sys sysRelease centos; then
local code=$1
local version="$(getversion)"
local main_ver=${version%%.*}
if [ "$main_ver" == "$code" ]; then
return 0
else
return 1
fi
else
return 1
fi
}
autoconf_version(){
if [ ! "$(command -v autoconf)" ]; then
echo -e "[${green}Info${plain}] Starting install package autoconf"
if check_sys packageManager yum; then
yum install -y autoconf > /dev/null 2>&1 || echo -e "[${red}Error:${plain}] Failed to install autoconf"
elif check_sys packageManager apt; then
apt-get -y update > /dev/null 2>&1
apt-get -y install autoconf > /dev/null 2>&1 || echo -e "[${red}Error:${plain}] Failed to install autoconf"
fi
fi
local autoconf_ver=$(autoconf --version | grep autoconf | grep -oE "[0-9.]+")
if version_ge ${autoconf_ver} 2.67; then
return 0
else
return 1
fi
}
get_ip(){
local IP=$( ip addr | egrep -o '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' | egrep -v "^192\.168|^172\.1[6-9]\.|^172\.2[0-9]\.|^172\.3[0-2]\.|^10\.|^127\.|^255\.|^0\." | head -n 1 )
[ -z ${IP} ] && IP=$( wget -qO- -t1 -T2 ipv4.icanhazip.com )
[ -z ${IP} ] && IP=$( wget -qO- -t1 -T2 ipinfo.io/ip )
echo ${IP}
}
get_ipv6(){
local ipv6=$(wget -qO- -t1 -T2 ipv6.icanhazip.com)
[ -z ${ipv6} ] && return 1 || return 0
}
get_libev_ver(){
libev_ver=$(wget --no-check-certificate -qO- https://api.github.com/repos/shadowsocks/shadowsocks-libev/releases/latest | grep 'tag_name' | cut -d\" -f4)
[ -z ${libev_ver} ] && echo -e "[${red}Error${plain}] Get shadowsocks-libev latest version failed" && exit 1
}
get_opsy(){
[ -f /etc/redhat-release ] && awk '{print ($1,$3~/^[0-9]/?$3:$4)}' /etc/redhat-release && return
[ -f /etc/os-release ] && awk -F'[= "]' '/PRETTY_NAME/{print $3,$4,$5}' /etc/os-release && return
[ -f /etc/lsb-release ] && awk -F'[="]+' '/DESCRIPTION/{print $2}' /etc/lsb-release && return
}
is_64bit(){
if [ `getconf WORD_BIT` = '32' ] && [ `getconf LONG_BIT` = '64' ] ; then
return 0
else
return 1
fi
}
debianversion(){
if check_sys sysRelease debian;then
local version=$( get_opsy )
local code=${1}
local main_ver=$( echo ${version} | sed 's/[^0-9]//g')
if [ "${main_ver}" == "${code}" ];then
return 0
else
return 1
fi
else
return 1
fi
}
download(){
local filename=$(basename $1)
if [ -f ${1} ]; then
echo "${filename} [found]"
else
echo "${filename} not found, download now..."
wget --no-check-certificate -c -t3 -T60 -O ${1} ${2}
if [ $? -ne 0 ]; then
echo -e "[${red}Error${plain}] Download ${filename} failed."
exit 1
fi
fi
}
download_files(){
cd ${cur_dir}
if   [ "${selected}" == "1" ]; then
download "${shadowsocks_python_file}.zip" "${shadowsocks_python_url}"
if check_sys packageManager yum; then
download "${shadowsocks_python_init}" "${shadowsocks_python_centos}"
elif check_sys packageManager apt; then
download "${shadowsocks_python_init}" "${shadowsocks_python_debian}"
fi
elif [ "${selected}" == "2" ]; then
download "${shadowsocks_r_file}.tar.gz" "${shadowsocks_r_url}"
if check_sys packageManager yum; then
download "${shadowsocks_r_init}" "${shadowsocks_r_centos}"
elif check_sys packageManager apt; then
download "${shadowsocks_r_init}" "${shadowsocks_r_debian}"
fi
elif [ "${selected}" == "3" ]; then
if is_64bit; then
download "${shadowsocks_go_file_64}.gz" "${shadowsocks_go_url_64}"
else
download "${shadowsocks_go_file_32}.gz" "${shadowsocks_go_url_32}"
fi
if check_sys packageManager yum; then
download "${shadowsocks_go_init}" "${shadowsocks_go_centos}"
elif check_sys packageManager apt; then
download "${shadowsocks_go_init}" "${shadowsocks_go_debian}"
fi
elif [ "${selected}" == "4" ]; then
get_libev_ver
shadowsocks_libev_file="shadowsocks-libev-$(echo ${libev_ver} | sed -e 's/^[a-zA-Z]//g')"
shadowsocks_libev_url="https://github.com/shadowsocks/shadowsocks-libev/releases/download/${libev_ver}/${shadowsocks_libev_file}.tar.gz"
download "${shadowsocks_libev_file}.tar.gz" "${shadowsocks_libev_url}"
if check_sys packageManager yum; then
download "${shadowsocks_libev_init}" "${shadowsocks_libev_centos}"
elif check_sys packageManager apt; then
download "${shadowsocks_libev_init}" "${shadowsocks_libev_debian}"
fi
fi
}
get_char(){
SAVEDSTTY=$(stty -g)
stty -echo
stty cbreak
dd if=/dev/tty bs=1 count=1 2> /dev/null
stty -raw
stty echo
stty $SAVEDSTTY
}
error_detect_depends(){
local command=$1
local depend=`echo "${command}" | awk '{print $4}'`
echo -e "[${green}Info${plain}] Starting to install package ${depend}"
${command} > /dev/null 2>&1
if [ $? -ne 0 ]; then
echo -e "[${red}Error${plain}] Failed to install ${red}${depend}${plain}"
exit 1
fi
}
config_firewall(){
if centosversion 6; then
/etc/init.d/iptables status > /dev/null 2>&1
if [ $? -eq 0 ]; then
iptables -L -n | grep -i ${shadowsocksport} > /dev/null 2>&1
if [ $? -ne 0 ]; then
iptables -I INPUT -m state --state NEW -m tcp -p tcp --dport ${shadowsocksport} -j ACCEPT
iptables -I INPUT -m state --state NEW -m udp -p udp --dport ${shadowsocksport} -j ACCEPT
/etc/init.d/iptables save
/etc/init.d/iptables restart
else
echo -e "[${green}Info${plain}] port ${green}${shadowsocksport}${plain} already be enabled."
fi
else
echo -e "[${yellow}Warning${plain}] iptables looks like not running or not installed, please enable port ${shadowsocksport} manually if necessary."
fi
elif centosversion 7; then
systemctl status firewalld > /dev/null 2>&1
if [ $? -eq 0 ]; then
default_zone=$(firewall-cmd --get-default-zone)
firewall-cmd --permanent --zone=${default_zone} --add-port=${shadowsocksport}/tcp
firewall-cmd --permanent --zone=${default_zone} --add-port=${shadowsocksport}/udp
firewall-cmd --reload
else
echo -e "[${yellow}Warning${plain}] firewalld looks like not running or not installed, please enable port ${shadowsocksport} manually if necessary."
fi
fi
}
config_shadowsocks(){
if check_kernel_version && check_kernel_headers; then
fast_open="true"
else
fast_open="false"
fi
if   [ "${selected}" == "1" ]; then
if [ ! -d "$(dirname ${shadowsocks_python_config})" ]; then
mkdir -p $(dirname ${shadowsocks_python_config})
fi
cat > ${shadowsocks_python_config}<<-EOF
{
"server":"0.0.0.0",
"server_port":${shadowsocksport},
"local_address":"127.0.0.1",
"local_port":1080,
"password":"${shadowsockspwd}",
"timeout":300,
"method":"${shadowsockscipher}",
"fast_open":${fast_open}
}
EOF
elif [ "${selected}" == "2" ]; then
if [ ! -d "$(dirname ${shadowsocks_r_config})" ]; then
mkdir -p $(dirname ${shadowsocks_r_config})
fi
cat > ${shadowsocks_r_config}<<-EOF
{
"server":"0.0.0.0",
"server_ipv6":"::",
"server_port":${shadowsocksport},
"local_address":"127.0.0.1",
"local_port":1080,
"password":"${shadowsockspwd}",
"timeout":120,
"method":"${shadowsockscipher}",
"protocol":"${shadowsockprotocol}",
"protocol_param":"",
"obfs":"${shadowsockobfs}",
"obfs_param":"",
"redirect":"",
"dns_ipv6":false,
"fast_open":${fast_open},
"workers":1
}
EOF
elif [ "${selected}" == "3" ]; then
if [ ! -d "$(dirname ${shadowsocks_go_config})" ]; then
mkdir -p $(dirname ${shadowsocks_go_config})
fi
cat > ${shadowsocks_go_config}<<-EOF
{
"server":"0.0.0.0",
"server_port":${shadowsocksport},
"local_port":1080,
"password":"${shadowsockspwd}",
"method":"${shadowsockscipher}",
"timeout":300
}
EOF
elif [ "${selected}" == "4" ]; then
local server_value="\"0.0.0.0\""
if get_ipv6; then
server_value="[\"[::0]\",\"0.0.0.0\"]"
fi
if [ ! -d "$(dirname ${shadowsocks_libev_config})" ]; then
mkdir -p $(dirname ${shadowsocks_libev_config})
fi
if [ "${libev_obfs}" == "y" ] || [ "${libev_obfs}" == "Y" ]; then
cat > ${shadowsocks_libev_config}<<-EOF
{
"server":${server_value},
"server_port":${shadowsocksport},
"password":"${shadowsockspwd}",
"timeout":300,
"user":"nobody",
"method":"${shadowsockscipher}",
"fast_open":${fast_open},
"nameserver":"8.8.8.8",
"mode":"tcp_and_udp",
"plugin":"obfs-server",
"plugin_opts":"obfs=${shadowsocklibev_obfs}"
}
EOF
else
cat > ${shadowsocks_libev_config}<<-EOF
{
"server":${server_value},
"server_port":${shadowsocksport},
"password":"${shadowsockspwd}",
"timeout":300,
"user":"nobody",
"method":"${shadowsockscipher}",
"fast_open":${fast_open},
"nameserver":"8.8.8.8",
"mode":"tcp_and_udp"
}
EOF
fi
fi
}
install_dependencies(){
if check_sys packageManager yum; then
echo -e "[${green}Info${plain}] Checking the EPEL repository..."
if [ ! -f /etc/yum.repos.d/epel.repo ]; then
yum install -y epel-release > /dev/null 2>&1
fi
[ ! -f /etc/yum.repos.d/epel.repo ] && echo -e "[${red}Error${plain}] Install EPEL repository failed, please check it." && exit 1
[ ! "$(command -v yum-config-manager)" ] && yum install -y yum-utils > /dev/null 2>&1
[ x"$(yum-config-manager epel | grep -w enabled | awk '{print $3}')" != x"True" ] && yum-config-manager --enable epel > /dev/null 2>&1
echo -e "[${green}Info${plain}] Checking the EPEL repository complete..."
yum_depends=(
unzip gzip openssl openssl-devel gcc python python-devel python-setuptools pcre pcre-devel libtool libevent
autoconf automake make curl curl-devel zlib-devel perl perl-devel cpio expat-devel gettext-devel
libev-devel c-ares-devel git qrencode
)
for depend in ${yum_depends[@]}; do
error_detect_depends "yum -y install ${depend}"
done
elif check_sys packageManager apt; then
apt_depends=(
gettext build-essential unzip gzip python python-dev python-setuptools curl openssl libssl-dev
autoconf automake libtool gcc make perl cpio libpcre3 libpcre3-dev zlib1g-dev libev-dev libc-ares-dev git qrencode
)
apt-get -y update
for depend in ${apt_depends[@]}; do
error_detect_depends "apt-get -y install ${depend}"
done
fi
}
install_check(){
if check_sys packageManager yum || check_sys packageManager apt; then
if centosversion 5; then
return 1
fi
return 0
else
return 1
fi
}
install_select(){
if ! install_check; then
echo -e "[${red}Error${plain}] Your OS is not supported to run it!"
echo "Please change to CentOS 6+/Debian 7+/Ubuntu 12+ and try again."
exit 1
fi
clear
while true
do
echo  "Cual servidor Shadowsocks quieres instalar (recomendado 4):"
for ((i=1;i<=${#software[@]};i++ )); do
hint="${software[$i-1]}"
echo -e "${green}${i}${plain}) ${hint}"
done
read -p "Escribe un numero (Default ${software[0]}):" selected
[ -z "${selected}" ] && selected="1"
case "${selected}" in
1|2|3|4)
echo
echo "Escogiste = ${software[${selected}-1]}"
echo
break
;;
*)
echo -e "[${red}Error${plain}] Por favor escribe un numero del [1-4]"
;;
esac
done
}
install_prepare_password(){
echo "Escribe una contraseña ${software[${selected}-1]}"
read -p "(Default password: DrowKid):" shadowsockspwd
[ -z "${shadowsockspwd}" ] && shadowsockspwd="DrowKid"
echo
echo "password = ${shadowsockspwd}"
echo
}
install_prepare_port() {
while true
do
dport=$(shuf -i 9000-19999 -n 1)
echo -e "Por favor escribe un puerto ${software[${selected}-1]} [1-65535]"
read -p "(Default port: ${dport}):" shadowsocksport
[ -z "${shadowsocksport}" ] && shadowsocksport=${dport}
expr ${shadowsocksport} + 1 &>/dev/null
if [ $? -eq 0 ]; then
if [ ${shadowsocksport} -ge 1 ] && [ ${shadowsocksport} -le 65535 ] && [ ${shadowsocksport:0:1} != 0 ]; then
echo
echo "port = ${shadowsocksport}"
echo
break
fi
fi
echo -e "[${red}Error${plain}] Por favor escribe un numero entre [1-65535]"
done
}
install_prepare_cipher(){
while true
do
echo -e "Escribe el tipo de encriptacion ${software[${selected}-1]}:"
if   [[ "${selected}" == "1" || "${selected}" == "4" ]]; then
for ((i=1;i<=${#common_ciphers[@]};i++ )); do
hint="${common_ciphers[$i-1]}"
echo -e "${green}${i}${plain}) ${hint}"
done
read -p "Encriptacion(Default: ${common_ciphers[0]}):" pick
[ -z "$pick" ] && pick=1
expr ${pick} + 1 &>/dev/null
if [ $? -ne 0 ]; then
echo -e "[${red}Error${plain}] Por favor escribe un numero"
continue
fi
if [[ "$pick" -lt 1 || "$pick" -gt ${#common_ciphers[@]} ]]; then
echo -e "[${red}Error${plain}] Por favor escribe un numero entre 1 y ${#common_ciphers[@]}"
continue
fi
shadowsockscipher=${common_ciphers[$pick-1]}
elif [ "${selected}" == "2" ]; then
for ((i=1;i<=${#r_ciphers[@]};i++ )); do
hint="${r_ciphers[$i-1]}"
echo -e "${green}${i}${plain}) ${hint}"
done
read -p "Encriptacion(Default: ${r_ciphers[1]}):" pick
[ -z "$pick" ] && pick=2
expr ${pick} + 1 &>/dev/null
if [ $? -ne 0 ]; then
echo -e "[${red}Error${plain}] Por favor escribe un numero"
continue
fi
if [[ "$pick" -lt 1 || "$pick" -gt ${#r_ciphers[@]} ]]; then
echo -e "[${red}Error${plain}] Por favor escribe un numero entre 1 y ${#r_ciphers[@]}"
continue
fi
shadowsockscipher=${r_ciphers[$pick-1]}
elif [ "${selected}" == "3" ]; then
for ((i=1;i<=${#go_ciphers[@]};i++ )); do
hint="${go_ciphers[$i-1]}"
echo -e "${green}${i}${plain}) ${hint}"
done
read -p "Encriptacion(Default: ${go_ciphers[0]}):" pick
[ -z "$pick" ] && pick=1
expr ${pick} + 1 &>/dev/null
if [ $? -ne 0 ]; then
echo -e "[${red}Error${plain}] Por favor escribe un numero"
continue
fi
if [[ "$pick" -lt 1 || "$pick" -gt ${#go_ciphers[@]} ]]; then
echo -e "[${red}Error${plain}] Por favor escribe un numero entre 1 y ${#go_ciphers[@]}"
continue
fi
shadowsockscipher=${go_ciphers[$pick-1]}
fi
echo
echo "cipher = ${shadowsockscipher}"
echo
break
done
}
install_prepare_protocol(){
while true
do
echo -e "Escoge un protocolo ${software[${selected}-1]}:"
for ((i=1;i<=${#protocols[@]};i++ )); do
hint="${protocols[$i-1]}"
echo -e "${green}${i}${plain}) ${hint}"
done
read -p "Protocolo(Default: ${protocols[0]}):" protocol
[ -z "$protocol" ] && protocol=1
expr ${protocol} + 1 &>/dev/null
if [ $? -ne 0 ]; then
echo -e "[${red}Error${plain}] Por favor escribe un numero"
continue
fi
if [[ "$protocol" -lt 1 || "$protocol" -gt ${#protocols[@]} ]]; then
echo -e "[${red}Error${plain}] Por favor escribe un numero entre 1 y ${#protocols[@]}"
continue
fi
shadowsockprotocol=${protocols[$protocol-1]}
echo
echo "protocol = ${shadowsockprotocol}"
echo
break
done
}
install_prepare_obfs(){
while true
do
echo -e "Please select obfs for ${software[${selected}-1]}:"
for ((i=1;i<=${#obfs[@]};i++ )); do
hint="${obfs[$i-1]}"
echo -e "${green}${i}${plain}) ${hint}"
done
read -p "Obfs(Default: ${obfs[0]}):" r_obfs
[ -z "$r_obfs" ] && r_obfs=1
expr ${r_obfs} + 1 &>/dev/null
if [ $? -ne 0 ]; then
echo -e "[${red}Error${plain}] Por favor escribe un numero"
continue
fi
if [[ "$r_obfs" -lt 1 || "$r_obfs" -gt ${#obfs[@]} ]]; then
echo -e "[${red}Error${plain}] Por favor escoge un numero entre 1 y ${#obfs[@]}"
continue
fi
shadowsockobfs=${obfs[$r_obfs-1]}
echo
echo "obfs = ${shadowsockobfs}"
echo
break
done
}
install_prepare_libev_obfs(){
if autoconf_version || centosversion 6; then
while true
do
echo -e "Quieres instalar simple-obfs para ${software[${selected}-1]}? [y/n]"
read -p "(default: n):" libev_obfs
[ -z "$libev_obfs" ] && libev_obfs=n
case "${libev_obfs}" in
y|Y|n|N)
echo
echo "Escogiste = ${libev_obfs}"
echo
break
;;
*)
echo -e "[${red}Error${plain}] Por favor solo escribe [y/n]"
;;
esac
done
if [ "${libev_obfs}" == "y" ] || [ "${libev_obfs}" == "Y" ]; then
while true
do
echo -e "Por favor selecciona el simple-obfs:"
for ((i=1;i<=${#obfs_libev[@]};i++ )); do
hint="${obfs_libev[$i-1]}"
echo -e "${green}${i}${plain}) ${hint}"
done
read -p "Obfs(Default: ${obfs_libev[0]}):" r_libev_obfs
[ -z "$r_libev_obfs" ] && r_libev_obfs=1
expr ${r_libev_obfs} + 1 &>/dev/null
if [ $? -ne 0 ]; then
echo -e "[${red}Error${plain}] Por favor escribe un numero"
continue
fi
if [[ "$r_libev_obfs" -lt 1 || "$r_libev_obfs" -gt ${#obfs_libev[@]} ]]; then
echo -e "[${red}Error${plain}] Por favor escribe un numero entre 1 y ${#obfs_libev[@]}"
continue
fi
shadowsocklibev_obfs=${obfs_libev[$r_libev_obfs-1]}
echo
echo "obfs = ${shadowsocklibev_obfs}"
echo
break
done
fi
else
echo -e "[${green}Info${plain}] autoconf version is less than 2.67, simple-obfs for ${software[${selected}-1]} installation has been skipped"
fi
}
install_prepare(){
if  [[ "${selected}" == "1" || "${selected}" == "3" || "${selected}" == "4" ]]; then
install_prepare_password
install_prepare_port
install_prepare_cipher
if [ "${selected}" == "4" ]; then
install_prepare_libev_obfs
fi
elif [ "${selected}" == "2" ]; then
install_prepare_password
install_prepare_port
install_prepare_cipher
install_prepare_protocol
install_prepare_obfs
fi
echo
echo "Presiona cualquier tecla para continuar...o Presiona Ctrl+C para cancelar"
char=`get_char`
}
install_libsodium(){
if [ ! -f /usr/lib/libsodium.a ]; then
cd ${cur_dir}
download "${libsodium_file}.tar.gz" "${libsodium_url}"
tar zxf ${libsodium_file}.tar.gz
cd ${libsodium_file}
./configure --prefix=/usr && make && make install
if [ $? -ne 0 ]; then
echo -e "[${red}Error${plain}] ${libsodium_file} install failed."
install_cleanup
exit 1
fi
else
echo -e "[${green}Info${plain}] ${libsodium_file} already installed."
fi
}
install_mbedtls(){
if [ ! -f /usr/lib/libmbedtls.a ]; then
cd ${cur_dir}
download "${mbedtls_file}-gpl.tgz" "${mbedtls_url}"
tar xf ${mbedtls_file}-gpl.tgz
cd ${mbedtls_file}
make SHARED=1 CFLAGS=-fPIC
make DESTDIR=/usr install
if [ $? -ne 0 ]; then
echo -e "[${red}Error${plain}] ${mbedtls_file} install failed."
install_cleanup
exit 1
fi
else
echo -e "[${green}Info${plain}] ${mbedtls_file} already installed."
fi
}
install_shadowsocks_python(){
cd ${cur_dir}
unzip -q ${shadowsocks_python_file}.zip
if [ $? -ne 0 ];then
echo -e "[${red}Error${plain}] unzip ${shadowsocks_python_file}.zip failed, please check unzip command."
install_cleanup
exit 1
fi
cd ${shadowsocks_python_file}
python setup.py install --record /usr/local/shadowsocks_python.log
if [ -f /usr/bin/ssserver ] || [ -f /usr/local/bin/ssserver ]; then
chmod +x ${shadowsocks_python_init}
local service_name=$(basename ${shadowsocks_python_init})
if check_sys packageManager yum; then
chkconfig --add ${service_name}
chkconfig ${service_name} on
elif check_sys packageManager apt; then
update-rc.d -f ${service_name} defaults
fi
else
echo
echo -e "[${red}Error${plain}] ${software[0]} install failed."
install_cleanup
exit 1
fi
}
install_shadowsocks_r(){
cd ${cur_dir}
tar zxf ${shadowsocks_r_file}.tar.gz
mv ${shadowsocks_r_file}/shadowsocks /usr/local/
if [ -f /usr/local/shadowsocks/server.py ]; then
chmod +x ${shadowsocks_r_init}
local service_name=$(basename ${shadowsocks_r_init})
if check_sys packageManager yum; then
chkconfig --add ${service_name}
chkconfig ${service_name} on
elif check_sys packageManager apt; then
update-rc.d -f ${service_name} defaults
fi
else
echo
echo -e "[${red}Error${plain}] ${software[1]} install failed."
install_cleanup
exit 1
fi
}
install_shadowsocks_go(){
cd ${cur_dir}
if is_64bit; then
gzip -d ${shadowsocks_go_file_64}.gz
if [ $? -ne 0 ];then
echo -e "[${red}Error${plain}] Decompress ${shadowsocks_go_file_64}.gz failed."
install_cleanup
exit 1
fi
mv -f ${shadowsocks_go_file_64} /usr/bin/shadowsocks-server
else
gzip -d ${shadowsocks_go_file_32}.gz
if [ $? -ne 0 ];then
echo -e "[${red}Error${plain}] Decompress ${shadowsocks_go_file_32}.gz failed."
install_cleanup
exit 1
fi
mv -f ${shadowsocks_go_file_32} /usr/bin/shadowsocks-server
fi
if [ -f /usr/bin/shadowsocks-server ]; then
chmod +x /usr/bin/shadowsocks-server
chmod +x ${shadowsocks_go_init}
local service_name=$(basename ${shadowsocks_go_init})
if check_sys packageManager yum; then
chkconfig --add ${service_name}
chkconfig ${service_name} on
elif check_sys packageManager apt; then
update-rc.d -f ${service_name} defaults
fi
else
echo
echo -e "[${red}Error${plain}] ${software[2]} install failed."
install_cleanup
exit 1
fi
}
install_shadowsocks_libev(){
cd ${cur_dir}
tar zxf ${shadowsocks_libev_file}.tar.gz
cd ${shadowsocks_libev_file}
./configure --disable-documentation && make && make install
if [ $? -eq 0 ]; then
chmod +x ${shadowsocks_libev_init}
local service_name=$(basename ${shadowsocks_libev_init})
if check_sys packageManager yum; then
chkconfig --add ${service_name}
chkconfig ${service_name} on
elif check_sys packageManager apt; then
update-rc.d -f ${service_name} defaults
fi
else
echo
echo -e "[${red}Error${plain}] ${software[3]} install failed."
install_cleanup
exit 1
fi
}
install_shadowsocks_libev_obfs(){
if [ "${libev_obfs}" == "y" ] || [ "${libev_obfs}" == "Y" ]; then
cd ${cur_dir}
git clone https://github.com/shadowsocks/simple-obfs.git
[ -d simple-obfs ] && cd simple-obfs || echo -e "[${red}Error:${plain}] Failed to git clone simple-obfs."
git submodule update --init --recursive
if centosversion 6; then
if [ ! "$(command -v autoconf268)" ]; then
echo -e "[${green}Info${plain}] Starting install autoconf268..."
yum install -y autoconf268 > /dev/null 2>&1 || echo -e "[${red}Error:${plain}] Failed to install autoconf268."
fi
sed -i 's/autoreconf/autoreconf268/' autogen.sh
sed -i 's@^#include <ev.h>@#include <libev/ev.h>@' src/local.h
sed -i 's@^#include <ev.h>@#include <libev/ev.h>@' src/server.h
fi
./autogen.sh
./configure --disable-documentation
make
make install
if [ ! "$(command -v obfs-server)" ]; then
echo -e "[${red}Error${plain}] simple-obfs for ${software[${selected}-1]} install failed."
install_cleanup
exit 1
fi
[ -f /usr/local/bin/obfs-server ] && ln -s /usr/local/bin/obfs-server /usr/bin
fi
}
install_completed_python(){
clear
${shadowsocks_python_init} start
echo
echo -e "Felicidades, ${green}${software[0]}${plain} server install completed!"
echo -e "IP        : ${red} $(get_ip) ${plain}"
echo -e "Port      : ${red} ${shadowsocksport} ${plain}"
echo -e "Password         : ${red} ${shadowsockspwd} ${plain}"
echo -e "Metodo de Encriptacion: ${red} ${shadowsockscipher} ${plain}"
}
install_completed_r(){
clear
${shadowsocks_r_init} start
echo
echo -e "Felicidades, ${green}${software[1]}${plain} server install completed!"
echo -e "IP        : ${red} $(get_ip) ${plain}"
echo -e "Port      : ${red} ${shadowsocksport} ${plain}"
echo -e "Password         : ${red} ${shadowsockspwd} ${plain}"
echo -e "Protocol         : ${red} ${shadowsockprotocol} ${plain}"
echo -e "Obfs             : ${red} ${shadowsockobfs} ${plain}"
echo -e "Metodo de Encriptacion: ${red} ${shadowsockscipher} ${plain}"
}
install_completed_go(){
clear
${shadowsocks_go_init} start
echo
echo -e "Felicidades, ${green}${software[2]}${plain} server install completed!"
echo -e "IP        : ${red} $(get_ip) ${plain}"
echo -e "Port      : ${red} ${shadowsocksport} ${plain}"
echo -e "Password         : ${red} ${shadowsockspwd} ${plain}"
echo -e "Metodo de Encriptacion: ${red} ${shadowsockscipher} ${plain}"
}
install_completed_libev(){
clear
ldconfig
${shadowsocks_libev_init} start
echo
echo -e "Felicidades, ${green}${software[3]}${plain} instalacion completada!"
echo -e "IP        : ${red} $(get_ip) ${plain}"
echo -e "Port      : ${red} ${shadowsocksport} ${plain}"
echo -e "Password         : ${red} ${shadowsockspwd} ${plain}"
if [ "$(command -v obfs-server)" ]; then
echo -e "Obfs             : ${red} ${shadowsocklibev_obfs} ${plain}"
fi
echo -e "Metodo de Encriptacion: ${red} ${shadowsockscipher} ${plain}"
}
qr_generate_python(){
if [ "$(command -v qrencode)" ]; then
local tmp=$(echo -n "${shadowsockscipher}:${shadowsockspwd}@$(get_ip):${shadowsocksport}" | base64 -w0)
local qr_code="ss://${tmp}"
echo
echo "Codigo QR: (Para Shadowsocks Windows, OSX, Android y iOS)"
echo -e "${green} ${qr_code} ${plain}"
echo -n "${qr_code}" | qrencode -s8 -o ${cur_dir}/shadowsocks_python_qr.png
echo "Tu codigo QR fue guardado en la siguiente direccion:"
echo -e "${green} ${cur_dir}/shadowsocks_python_qr.png ${plain}"
fi
}
qr_generate_r(){
if [ "$(command -v qrencode)" ]; then
local tmp1=$(echo -n "${shadowsockspwd}" | base64 -w0 | sed 's/=//g;s/\//_/g;s/+/-/g')
local tmp2=$(echo -n "$(get_ip):${shadowsocksport}:${shadowsockprotocol}:${shadowsockscipher}:${shadowsockobfs}:${tmp1}/?obfsparam=" | base64 -w0)
local qr_code="ssr://${tmp2}"
echo
echo "Codigo QR: (Para ShadowsocksR Windows, Android)"
echo -e "${green} ${qr_code} ${plain}"
echo -n "${qr_code}" | qrencode -s8 -o ${cur_dir}/shadowsocks_r_qr.png
echo "Tu codigo QR fue guardado en la siguiente direccion como PNG:"
echo -e "${green} ${cur_dir}/shadowsocks_r_qr.png ${plain}"
fi
}
qr_generate_go(){
if [ "$(command -v qrencode)" ]; then
local tmp=$(echo -n "${shadowsockscipher}:${shadowsockspwd}@$(get_ip):${shadowsocksport}" | base64 -w0)
local qr_code="ss://${tmp}"
echo
echo "Codigo QR: (Para Shadowsocks Windows, OSX, Android y iOS)"
echo -e "${green} ${qr_code} ${plain}"
echo -n "${qr_code}" | qrencode -s8 -o ${cur_dir}/shadowsocks_go_qr.png
echo "Tu codigo QR fue guardado en la siguiente direccion como PNG:"
echo -e "${green} ${cur_dir}/shadowsocks_go_qr.png ${plain}"
fi
}
qr_generate_libev(){
if [ "$(command -v qrencode)" ]; then
local tmp=$(echo -n "${shadowsockscipher}:${shadowsockspwd}@$(get_ip):${shadowsocksport}" | base64 -w0)
local qr_code="ss://${tmp}"
echo
echo "Codigo QR: (Para Shadowsocks Windows, OSX, Android y iOS)"
echo -e "${green} ${qr_code} ${plain}"
echo -n "${qr_code}" | qrencode -s8 -o ${cur_dir}/shadowsocks_libev_qr.png
echo "Tu codigo QR fue guardado en la siguiente direccion como PNG:"
echo -e "${green} ${cur_dir}/shadowsocks_libev_qr.png ${plain}"
fi
}
install_main(){
install_libsodium
if ! ldconfig -p | grep -wq "/usr/lib"; then
echo "/usr/lib" > /etc/ld.so.conf.d/lib.conf
fi
ldconfig
if   [ "${selected}" == "1" ]; then
install_shadowsocks_python
install_completed_python
qr_generate_python
elif [ "${selected}" == "2" ]; then
install_shadowsocks_r
install_completed_r
qr_generate_r
elif [ "${selected}" == "3" ]; then
install_shadowsocks_go
install_completed_go
qr_generate_go
elif [ "${selected}" == "4" ]; then
install_mbedtls
install_shadowsocks_libev
install_shadowsocks_libev_obfs
install_completed_libev
qr_generate_libev
fi
echo
echo "✧ | ᴅʀᴏᴡᴋɪᴅ | ✧ - Shadowsocks"
echo "t.me/drowkid01"
echo
}
install_cleanup(){
cd ${cur_dir}
rm -rf simple-obfs
rm -rf ${libsodium_file} ${libsodium_file}.tar.gz
rm -rf ${mbedtls_file} ${mbedtls_file}-gpl.tgz
rm -rf ${shadowsocks_python_file} ${shadowsocks_python_file}.zip
rm -rf ${shadowsocks_r_file} ${shadowsocks_r_file}.tar.gz
rm -rf ${shadowsocks_go_file_64}.gz ${shadowsocks_go_file_32}.gz
rm -rf ${shadowsocks_libev_file} ${shadowsocks_libev_file}.tar.gz
}
install_shadowsocks(){
disable_selinux
install_select
install_prepare
install_dependencies
download_files
config_shadowsocks
if check_sys packageManager yum; then
config_firewall
fi
install_main
install_cleanup
}
uninstall_shadowsocks_python(){
printf "Estas seguro que quieres desinstalar ${red}${software[0]}${plain}? [y/n]\n"
read -p "(default: n):" answer
[ -z ${answer} ] && answer="n"
if [ "${answer}" == "y" ] || [ "${answer}" == "Y" ]; then
${shadowsocks_python_init} status > /dev/null 2>&1
if [ $? -eq 0 ]; then
${shadowsocks_python_init} stop
fi
local service_name=$(basename ${shadowsocks_python_init})
if check_sys packageManager yum; then
chkconfig --del ${service_name}
elif check_sys packageManager apt; then
update-rc.d -f ${service_name} remove
fi
rm -fr $(dirname ${shadowsocks_python_config})
rm -f ${shadowsocks_python_init}
rm -f /var/log/shadowsocks.log
if [ -f /usr/local/shadowsocks_python.log ]; then
cat /usr/local/shadowsocks_python.log | xargs rm -rf
rm -f /usr/local/shadowsocks_python.log
fi
echo -e "[${green}Info${plain}] ${software[0]} desinstalacion exitosa"
else
echo
echo -e "[${green}Info${plain}] ${software[0]} desinstalacion cancelada..."
echo
fi
}
uninstall_shadowsocks_r(){
printf "Estas seguro que quieres desinstalar ${red}${software[1]}${plain}? [y/n]\n"
read -p "(default: n):" answer
[ -z ${answer} ] && answer="n"
if [ "${answer}" == "y" ] || [ "${answer}" == "Y" ]; then
${shadowsocks_r_init} status > /dev/null 2>&1
if [ $? -eq 0 ]; then
${shadowsocks_r_init} stop
fi
local service_name=$(basename ${shadowsocks_r_init})
if check_sys packageManager yum; then
chkconfig --del ${service_name}
elif check_sys packageManager apt; then
update-rc.d -f ${service_name} remove
fi
rm -fr $(dirname ${shadowsocks_r_config})
rm -f ${shadowsocks_r_init}
rm -f /var/log/shadowsocks.log
rm -fr /usr/local/shadowsocks
echo -e "[${green}Info${plain}] ${software[1]} Desinstalacion exitosa"
else
echo
echo -e "[${green}Info${plain}] ${software[1]} Desinstalacion cancelada..."
echo
fi
}
uninstall_shadowsocks_go(){
printf "Estas seguro que quieres desinstalar ${red}${software[2]}${plain}? [y/n]\n"
read -p "(default: n):" answer
[ -z ${answer} ] && answer="n"
if [ "${answer}" == "y" ] || [ "${answer}" == "Y" ]; then
${shadowsocks_go_init} status > /dev/null 2>&1
if [ $? -eq 0 ]; then
${shadowsocks_go_init} stop
fi
local service_name=$(basename ${shadowsocks_go_init})
if check_sys packageManager yum; then
chkconfig --del ${service_name}
elif check_sys packageManager apt; then
update-rc.d -f ${service_name} remove
fi
rm -fr $(dirname ${shadowsocks_go_config})
rm -f ${shadowsocks_go_init}
rm -f /usr/bin/shadowsocks-server
echo -e "[${green}Info${plain}] ${software[2]} desinstalacion exitosa"
else
echo
echo -e "[${green}Info${plain}] ${software[2]} desinstalacion cancelada..."
echo
fi
}
uninstall_shadowsocks_libev(){
printf "Estas seguro que quieres desinstalar ${red}${software[3]}${plain}? [y/n]\n"
read -p "(default: n):" answer
[ -z ${answer} ] && answer="n"
if [ "${answer}" == "y" ] || [ "${answer}" == "Y" ]; then
${shadowsocks_libev_init} status > /dev/null 2>&1
if [ $? -eq 0 ]; then
${shadowsocks_libev_init} stop
fi
local service_name=$(basename ${shadowsocks_libev_init})
if check_sys packageManager yum; then
chkconfig --del ${service_name}
elif check_sys packageManager apt; then
update-rc.d -f ${service_name} remove
fi
rm -fr $(dirname ${shadowsocks_libev_config})
rm -f /usr/local/bin/ss-local
rm -f /usr/local/bin/ss-tunnel
rm -f /usr/local/bin/ss-server
rm -f /usr/local/bin/ss-manager
rm -f /usr/local/bin/ss-redir
rm -f /usr/local/bin/ss-nat
rm -f /usr/local/bin/obfs-local
rm -f /usr/local/bin/obfs-server
rm -f /usr/local/lib/libshadowsocks-libev.a
rm -f /usr/local/lib/libshadowsocks-libev.la
rm -f /usr/local/include/shadowsocks.h
rm -f /usr/local/lib/pkgconfig/shadowsocks-libev.pc
rm -f /usr/local/share/man/man1/ss-local.1
rm -f /usr/local/share/man/man1/ss-tunnel.1
rm -f /usr/local/share/man/man1/ss-server.1
rm -f /usr/local/share/man/man1/ss-manager.1
rm -f /usr/local/share/man/man1/ss-redir.1
rm -f /usr/local/share/man/man1/ss-nat.1
rm -f /usr/local/share/man/man8/shadowsocks-libev.8
rm -fr /usr/local/share/doc/shadowsocks-libev
rm -f ${shadowsocks_libev_init}
echo -e "[${green}Info${plain}] ${software[3]} desinstalacion exitosa"
else
echo
echo -e "[${green}Info${plain}] ${software[3]} desinstalacion cancelada..."
echo
fi
}
uninstall_shadowsocks(){
while true
do
echo  "Cual servidor Shadowsocks quieres desinstalar?"
for ((i=1;i<=${#software[@]};i++ )); do
hint="${software[$i-1]}"
echo -e "${green}${i}${plain}) ${hint}"
done
read -p "Escoge un numero [1-4]:" un_select
case "${un_select}" in
1|2|3|4)
echo
echo "Escogiste = ${software[${un_select}-1]}"
echo
break
;;
*)
echo -e "[${red}Error${plain}] escoge un numero [1-4]"
;;
esac
done
if   [ "${un_select}" == "1" ]; then
if [ -f ${shadowsocks_python_init} ]; then
uninstall_shadowsocks_python
else
echo -e "[${red}Error${plain}] ${software[${un_select}-1]} no instalado, por favor verifica e intenta de nuevo."
echo
exit 1
fi
elif [ "${un_select}" == "2" ]; then
if [ -f ${shadowsocks_r_init} ]; then
uninstall_shadowsocks_r
else
echo -e "[${red}Error${plain}] ${software[${un_select}-1]} no instalado, por favor verifica e intenta de nuevo."
echo
exit 1
fi
elif [ "${un_select}" == "3" ]; then
if [ -f ${shadowsocks_go_init} ]; then
uninstall_shadowsocks_go
else
echo -e "[${red}Error${plain}] ${software[${un_select}-1]} no instalado, por favor verifica e intenta de nuevo."
echo
exit 1
fi
elif [ "${un_select}" == "4" ]; then
if [ -f ${shadowsocks_libev_init} ]; then
uninstall_shadowsocks_libev
else
echo -e "[${red}Error${plain}] ${software[${un_select}-1]} no instalado, por favor verifica e intenta de nuevo."
echo
exit 1
fi
fi
}
action=$1
[ -z $1 ] && action=install
case "${action}" in
install|uninstall)
${action}_shadowsocks
;;
*)
echo "Arguments error! [${action}]"
echo "Usage: $(basename $0) [install|uninstall]"
;;
esac
}

function select_extra(){
case $1 in
 --firewall) firewall;;
 --openvpn) openvpn;;
 --clash-beta) clash_beta;;
 --socks) socks5;;
 --h-beta) h_beta;;
 --shadowsocks)shadowsocks ;;
 *)echo -e "\e[1;31m[x] error [x]";exit 1 ;;
esac
}

[[ -z $1 ]] && exit 1

case $1 in
 "$@") select_extra "$@";;
esac
